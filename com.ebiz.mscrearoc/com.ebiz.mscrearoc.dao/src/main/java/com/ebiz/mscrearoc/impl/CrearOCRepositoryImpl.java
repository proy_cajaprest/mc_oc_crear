package com.ebiz.mscrearoc.impl;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParsePosition;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ebiz.mscrearoc.OrdenCompra;
import com.ebiz.mscrearoc.Organiza;
import com.ebiz.mscrearoc.Organizacion;
import com.ebiz.mscrearoc.POUPLOADMQ;
import com.ebiz.mscrearoc.ProductoxOC;
import com.ebiz.mscrearoc.SubItemxProductoxOC;
import com.ebiz.mscrearoc.Usuario;
import com.ebiz.mscrearoc.UsuarioEquivalencia;

import com.fasterxml.jackson.databind.util.ISO8601Utils;
import com.ebiz.mscrearoc.ArchivoxOC;
import com.ebiz.mscrearoc.AtributoxOC;
import com.ebiz.mscrearoc.AtributoxProductoxOC;
import com.ebiz.mscrearoc.AtributoxSubItemxProductoxOC;
import com.ebiz.mscrearoc.CEOrdenCompras;
import com.ebiz.mscrearoc.CrearOCRepository;
import com.ebiz.mscrearoc.Maestra;

import java.util.UUID;

@Repository
public class CrearOCRepositoryImpl implements CrearOCRepository{
	private static final Logger LOGGER = LoggerFactory.getLogger(CrearOCRepositoryImpl.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Value("${app.constantes.habilitado}")
	private int habilitado;					// 2 o 4
	
	@Value("${app.constantes.estadoanuladoOC}")
	private String estadoanuladoOC;		
	
	@Value("${app.constantes.estadocompradorOC}")
	private String estadocompradorOC;	
	
	@Value("${app.constantes.estadovendedorOC}")
	private String estadovendedorOC;	
		
	@Transactional(rollbackFor = Exception.class)
	public List<OrdenCompra> insertOC(CEOrdenCompras cEOrdenCompras,Organiza org,String token,Organiza proveedor,Usuario usuario_prov, UsuarioEquivalencia usuario_comp, UsuarioEquivalencia usuario_creador, UUID idusuario_auditoria)  throws Exception {
		LOGGER.info("Inicio de transacción creación de OC ");
		List<OrdenCompra> lista_idoc = new ArrayList<OrdenCompra>();	
		OrdenCompra oc_mensaje = new OrdenCompra();
		POUPLOADMQ pouploadmq = cEOrdenCompras.getPouploadmq();
		List<OrdenCompra> list_oc = pouploadmq.getLista_oc();
	
		LOGGER.info(list_oc.size()+"tama�o lista oc");		
		
		for(OrdenCompra ordenCompra : list_oc) {
		
			OrdenCompra actualizarOC = new OrdenCompra();
			
			actualizarOC=selectOrdenCompra(ordenCompra);
			UUID in_idoc= UUID.randomUUID();
			if (actualizarOC==null) {
				LOGGER.info("Se creará una nueva orden de compra");
				ordenCompra.setIn_version(1);
				oc_mensaje=insertarOrdenCompra(in_idoc,ordenCompra,pouploadmq,org,token,proveedor, usuario_prov, usuario_comp, usuario_creador, idusuario_auditoria);
				oc_mensaje.setIn_idoc(in_idoc);
				lista_idoc.add(oc_mensaje);
			} else {
				LOGGER.info("Existe orden de compra duplicada se versiona");
				if (actualizarOC!=null) { 
				//actualiza la oc a anulada y crea una con la version incrementada en 1 solo se debe setear la orden de compra con una nueva version todo lo
				//dem�s se realiza de nuevo
					LOGGER.info(actualizarOC.getIn_idoc()+" idoc para actualizar");
					updateOrdenCompra(actualizarOC, idusuario_auditoria);
					incrementaVersion(actualizarOC,ordenCompra);
				}
				LOGGER.info(ordenCompra.getIn_version()+" in_version para actualizar");
				oc_mensaje=insertarOrdenCompra(in_idoc,ordenCompra,pouploadmq,org,token,proveedor,usuario_prov, usuario_comp, usuario_creador, idusuario_auditoria);
				oc_mensaje.setIn_idoc(in_idoc);
				lista_idoc.add(oc_mensaje);
			}
			
			LOGGER.info(ordenCompra.getVc_enviarfacturaa()+" orden Compra");
							
		}
		LOGGER.info("Fin de transacción creación de OC ");
		return lista_idoc;

	}
	
	public OrdenCompra insertarOrdenCompra(UUID in_idoc,OrdenCompra ordenCompra,POUPLOADMQ pouploadmq,Organiza org,String token,Organiza proveedor, Usuario usuario_adm_prov, UsuarioEquivalencia usuario_comp, UsuarioEquivalencia usuario_creador, UUID idusuario_auditoria)  throws Exception {
		LOGGER.info("Inicio de creación de Orden de Compra");
		OrdenCompra oc = new OrdenCompra(); 
		String SQL = "insert into oc.t_oc (vc_nombreusuariocreadopor,in_idusuariocreadopor,vc_condicionesgenerales,vc_codinternoprov,vc_ruccomprador,IN_IDOC,IN_IDUSUARIOCOMPRADOR,IN_IDUSUARIOproveedor,VC_NOMBREUSUARIOproveedor,VC_NOMBREUSUARIOCOMPRADOR,ts_fechaentrega,CH_NUMEROSEGUIMIENTO,IN_VERSION,CH_PRIORIDAD,TS_FECHAREGISTRO,VC_TERMINOSENTREGA,VC_CONSIGNATARIO,TS_FECHAENVIO,VC_PAISEMBARQUE,VC_REGIONEMBARQUE,VC_CONDICIONEMBARQUE,VC_EMBARCADOR,IN_EMBARQUEPARCIAL,VC_PUERTODESEMBARQUE,VC_POLIZASEGURO,VC_ADUANA,VC_COMPANIAINSPECTORA,CH_NUMEROINSPECCION,VC_AUTORIZADOPOR,VC_CARGO,FL_VALORTOTAL,FL_OTROSCOSTOS,FL_VALORVENTA,FL_DESCUENTO,FL_SUBTOTAL,TS_FECHAAUTORIZACION,VC_MONEDA,CH_CODIGOALMACENENVIO,VC_LAB,IN_CODIGOUSUARIOCREACION,TS_FECHAHORACREACION,TS_FECHACREACION,VC_TIPOTRANSPORTE,VC_CONDICIONPAGO,IN_IDORGANIZACIONCOMPRADORA,IN_IDORGANIZACIONPROVEEDORA,FL_IMPUESTOS,VC_DIRECCIONFACTURA,CH_NUMEROQT,VC_FACTURARA,VC_RUCPROVEEDOR,VC_RAZONSOCIALPROVEEDOR,VC_ATENCIONA,VC_EMAILCONTACTO,VC_COMENTARIOCOMPRADOR,FL_DESCUENTOPORCENTAJE,FL_IMPUESTOSPORCENTAJE,VC_LOGO,VC_FIRMA,IN_FLAGOCDIRECTA,IN_REQUIEREINSPECCION,VC_TIPOOC,IN_IDORGPROPIETARIA,CH_NUMERORFQ,VC_ESTADOCOMPRADOR,VC_ESTADOPROVEEDOR,FL_TIPOCAMBIO,TS_FECHANOTIFICACION,TS_FECHACOMPROMISOPPTO,VC_NUMEROEXPEDIENTESIAF,VC_TIPOCONTRATACION,VC_OBJETOCONTRATACION,VC_DESCRIPCIONCONTRATACION,VC_UNIDADESSOLICITANTES,VC_UNIDADESINFORMANTES,vc_nombrerfq,in_versionqt,vc_terminos,ts_fechainicontrato,ts_fechafincontrato,vc_grupocompra,vc_nombrecontactovendedor,vc_emailcontactovendedor,vc_otrosdatosvendedor,vc_nombrecontactocomprador,vc_emailcontactocomprador,vc_otrosdatoscomprador,vc_msgmail,in_idoperadorlog,vc_numeroap,vc_forwarder,fl_isc,vc_enviarfacturaa,VC_UNIDADCONDPAGO,VC_NARRATIVA,vc_tipoprecio,in_otrosimpuestos,ts_fechamodoc,in_flagmodoc,VC_NUMQTPROV,vc_precioqt,in_tiemporecordatorio,fl_preciofob,in_flagorigen,fl_tasacambio,de_iva,de_utilidades,de_gastosgen ) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				
		java.util.Date today = new java.util.Date();
		Timestamp fechahoracreacion =new java.sql.Timestamp(today.getTime());
		try{

			LOGGER.info(ordenCompra.getVc_prioridad()+"prioridad");
			//UUID codigousuariocreacion= UUID.randomUUID();
			UUID in_idorganizacionproveedora = null;
			LOGGER.info(token + " token");
			if (proveedor!=null) {
				in_idorganizacionproveedora = proveedor.getIn_idorganizacion();
				LOGGER.info(proveedor.getIn_idorganizacion() + " proveedor UUID");
			}else {
				LOGGER.info("No existe organizacion proveedora");
				throw new Exception("No existe organizacion proveedora");
			}
			
			ordenCompra.setIn_idusuarioproveedor(usuario_adm_prov.getId_usuario());
			
			LOGGER.info(usuario_adm_prov.getId_usuario() + " usuario proveedor ");
			
			if (ordenCompra.getContactos()!=null) {
			//para otras empresas distintas de centenario
				jdbcTemplate.update(SQL,new Object[] {StringUtils.trimToEmpty(usuario_creador.getNombre())+ " " + StringUtils.trimToEmpty(usuario_creador.getAp_paterno()) + " "+ StringUtils.trimToEmpty(usuario_creador.getAp_materno()), usuario_creador.getIdusuario(), ordenCompra.getCondicionesGenerales(),ordenCompra.getVc_codinternoprov(),pouploadmq.getCompradorOrgID().substring(2),in_idoc,usuario_comp.getIdusuario(),(ordenCompra.getIn_idusuarioproveedor())!=null?ordenCompra.getIn_idusuarioproveedor():null,(ordenCompra.getVc_razonsocialproveedor())!=null?ordenCompra.getVc_razonsocialproveedor():null,usuario_comp.getNombre()!=null?usuario_comp.getNombre():"" + usuario_comp.getAp_paterno()!=null?usuario_comp.getAp_paterno():"" + usuario_comp.getAp_materno()!=null?usuario_comp.getAp_materno():"" ,(ordenCompra.getTs_fechaentrega()!=null)?convertirFecha(ordenCompra.getTs_fechaentrega()):null,(ordenCompra.getVc_numeroseguimiento())!=null?ordenCompra.getVc_numeroseguimiento():null,ordenCompra.getIn_version(),(ordenCompra.getVc_prioridad())!=null?ordenCompra.getVc_prioridad():null,(ordenCompra.getTs_fechacreacion())!=null?convertirFecha(ordenCompra.getTs_fechacreacion()):null,(ordenCompra.getVc_terminosentrega())!=null?ordenCompra.getVc_terminosentrega():null,(ordenCompra.getVc_consignatario())!=null?ordenCompra.getVc_consignatario():null,(ordenCompra.getTs_fechaenvio())!=null?convertirFecha(ordenCompra.getTs_fechaenvio()):null,(ordenCompra.getVc_paisembarque())!=null?ordenCompra.getVc_paisembarque():null,(ordenCompra.getVc_regionembarque())!=null?ordenCompra.getVc_regionembarque():null,(ordenCompra.getVc_condicionembarque())!=null?ordenCompra.getVc_condicionembarque():null,(ordenCompra.getVc_embarcador())!=null?ordenCompra.getVc_embarcador():null,(ordenCompra.getIn_embarqueparcial())!=null?ordenCompra.getIn_embarqueparcial():null,(ordenCompra.getVc_puertodesembarque())!=null?ordenCompra.getVc_puertodesembarque():null,(ordenCompra.getVc_polizaseguro())!=null?ordenCompra.getVc_polizaseguro():null,(ordenCompra.getVc_aduana())!=null?ordenCompra.getVc_aduana():null,(ordenCompra.getVc_companiainspectora())!=null?ordenCompra.getVc_companiainspectora():null,(ordenCompra.getVc_numeroinspeccion())!=null?ordenCompra.getVc_numeroinspeccion():null,(ordenCompra.getVc_autorizadopor())!=null?ordenCompra.getVc_autorizadopor():null,(ordenCompra.getVc_cargo())!=null?ordenCompra.getVc_cargo():null,(ordenCompra.getFl_valortotal())!=null?ordenCompra.getFl_valortotal():null,(ordenCompra.getFl_otroscostos())!=null?ordenCompra.getFl_otroscostos():null,(ordenCompra.getFl_valorventa())!=null?ordenCompra.getFl_valorventa():null,(ordenCompra.getFl_descuentoporcentaje())!=null?ordenCompra.getFl_descuentoporcentaje():null,(ordenCompra.getFl_subtotal())!=null?ordenCompra.getFl_subtotal():null,(ordenCompra.getTs_fechaautorizacion())!=null?convertirFecha(ordenCompra.getTs_fechaautorizacion()):null,(ordenCompra.getVc_moneda())!=null?ordenCompra.getVc_moneda():null,(ordenCompra.getVc_codigoalmacenenvio())!=null?ordenCompra.getVc_codigoalmacenenvio():null,(ordenCompra.getVc_lab())!=null?ordenCompra.getVc_lab():null,idusuario_auditoria,fechahoracreacion,(ordenCompra.getTs_fechacreacion())!=null?convertirFecha(ordenCompra.getTs_fechacreacion()):null,(ordenCompra.getVc_tipotransporte())!=null?ordenCompra.getVc_tipotransporte():null,(ordenCompra.getVc_condicionpago())!=null?ordenCompra.getVc_condicionpago():null,org.getIn_idorganizacion(),in_idorganizacionproveedora,(ordenCompra.getFl_impuestos())!=null?ordenCompra.getFl_impuestos():null,(ordenCompra.getVc_direccionfactura())!=null?ordenCompra.getVc_direccionfactura():null,(ordenCompra.getCotizacion())!=null?ordenCompra.getCotizacion().getNumeroCotizacion():null,(ordenCompra.getVc_facturara())!=null?ordenCompra.getVc_facturara():null,ordenCompra.getVc_rucproveedor().substring(2),ordenCompra.getVc_razonsocialproveedor(),ordenCompra.getVc_atenciona(),ordenCompra.getVc_emailcontacto(),ordenCompra.getVc_comentariocomprador(),ordenCompra.getFl_descuentoporcentaje(),ordenCompra.getFl_impuestosporcentaje(),ordenCompra.getVc_logo(),ordenCompra.getVc_firma(),ordenCompra.getIn_flagocdirecta(),ordenCompra.getIn_requiereinspeccion(),ordenCompra.getVc_tipooc(),org.getIn_idorganizacion(),(ordenCompra.getCotizacion())!=null?ordenCompra.getCotizacion().getNumeroRfq():null,estadocompradorOC,estadovendedorOC,ordenCompra.getTipocambio(),(ordenCompra.getTs_fechanotificacion())!=null?convertirFecha(ordenCompra.getTs_fechanotificacion()):null,(ordenCompra.getTs_fechacompromisoppto())!=null?convertirFecha(ordenCompra.getTs_fechacompromisoppto()):null,ordenCompra.getVc_numeroexpedientesiaf(),ordenCompra.getVc_tipocontratacion(),ordenCompra.getVc_objetocontratacion(),ordenCompra.getVc_descripcioncontratacion(),ordenCompra.getVc_unidadessolicitantes(),ordenCompra.getVc_unidadesinformantes(),ordenCompra.getCotizacion().getNombreRfq(),(ordenCompra.getCotizacion())!=null?ordenCompra.getCotizacion().getVersionCotizacion():null,ordenCompra.getVc_terminos(),(ordenCompra.getTs_fechainicontrato())!=null?convertirFecha(ordenCompra.getTs_fechainicontrato()):null,(ordenCompra.getTs_fechafincontrato())!=null?convertirFecha(ordenCompra.getTs_fechafincontrato()):null,ordenCompra.getVc_grupocompra(),ordenCompra.getContactos().getNombreContactoVendedor(),ordenCompra.getContactos().getEmailContactoVendedor(),ordenCompra.getContactos().getOtrosDatosVendedor(),ordenCompra.getContactos().getNombreContactoComprador(),ordenCompra.getContactos().getEmailContactoComprador(),ordenCompra.getContactos().getOtrosDatosComprador(),ordenCompra.getServicioCorreo()!=null?ordenCompra.getServicioCorreo().getMsgmail():null,(ordenCompra.getOperadorLogistico())!=null?ordenCompra.getOperadorLogistico().getOrgIDOperador():null,ordenCompra.getVc_numeroap(),ordenCompra.getVc_forwarder(),ordenCompra.getFl_isc(),ordenCompra.getVc_enviarfacturaa(),ordenCompra.getVc_unidadcondpago(),ordenCompra.getVc_narrativa(),ordenCompra.getVc_tipoprecio(),ordenCompra.getIn_otrosimpuestos(),convertirFecha(ordenCompra.getTs_fechamodoc()),ordenCompra.getIn_flagmodoc(),ordenCompra.getVc_numqtprov(),ordenCompra.getVc_precioqt(),ordenCompra.getIn_tiemporecordatorio(),ordenCompra.getFl_preciofob(),ordenCompra.getIn_flagorigen(),ordenCompra.getFl_tasacambio(),ordenCompra.getDe_iva(),ordenCompra.getDe_utilidades(),ordenCompra.getDe_gastosgen() });
			}else {
				jdbcTemplate.update(SQL,new Object[] {StringUtils.trimToEmpty(usuario_creador.getNombre())+ " " + StringUtils.trimToEmpty(usuario_creador.getAp_paterno()) + " "+ StringUtils.trimToEmpty(usuario_creador.getAp_materno()), usuario_creador.getIdusuario(), ordenCompra.getCondicionesGenerales(),ordenCompra.getVc_codinternoprov(),pouploadmq.getCompradorOrgID().substring(2),in_idoc,usuario_comp.getIdusuario(),(ordenCompra.getIn_idusuarioproveedor())!=null?ordenCompra.getIn_idusuarioproveedor():null,(ordenCompra.getVc_razonsocialproveedor())!=null?ordenCompra.getVc_razonsocialproveedor():null,usuario_comp.getNombre()!=null?usuario_comp.getNombre():"" + usuario_comp.getAp_paterno()!=null?usuario_comp.getAp_paterno():"" + usuario_comp.getAp_materno()!=null?usuario_comp.getAp_materno():"" ,(ordenCompra.getTs_fechaentrega()!=null)?convertirFecha(ordenCompra.getTs_fechaentrega()):null,(ordenCompra.getVc_numeroseguimiento())!=null?ordenCompra.getVc_numeroseguimiento():null,ordenCompra.getIn_version(),(ordenCompra.getVc_prioridad())!=null?ordenCompra.getVc_prioridad():null,(ordenCompra.getTs_fechacreacion())!=null?convertirFecha(ordenCompra.getTs_fechacreacion()):null,(ordenCompra.getVc_terminosentrega())!=null?ordenCompra.getVc_terminosentrega():null,(ordenCompra.getVc_consignatario())!=null?ordenCompra.getVc_consignatario():null,(ordenCompra.getTs_fechaenvio())!=null?convertirFecha(ordenCompra.getTs_fechaenvio()):null,(ordenCompra.getVc_paisembarque())!=null?ordenCompra.getVc_paisembarque():null,(ordenCompra.getVc_regionembarque())!=null?ordenCompra.getVc_regionembarque():null,(ordenCompra.getVc_condicionembarque())!=null?ordenCompra.getVc_condicionembarque():null,(ordenCompra.getVc_embarcador())!=null?ordenCompra.getVc_embarcador():null,(ordenCompra.getIn_embarqueparcial())!=null?ordenCompra.getIn_embarqueparcial():null,(ordenCompra.getVc_puertodesembarque())!=null?ordenCompra.getVc_puertodesembarque():null,(ordenCompra.getVc_polizaseguro())!=null?ordenCompra.getVc_polizaseguro():null,(ordenCompra.getVc_aduana())!=null?ordenCompra.getVc_aduana():null,(ordenCompra.getVc_companiainspectora())!=null?ordenCompra.getVc_companiainspectora():null,(ordenCompra.getVc_numeroinspeccion())!=null?ordenCompra.getVc_numeroinspeccion():null,(ordenCompra.getVc_autorizadopor())!=null?ordenCompra.getVc_autorizadopor():null,(ordenCompra.getVc_cargo())!=null?ordenCompra.getVc_cargo():null,(ordenCompra.getFl_valortotal())!=null?ordenCompra.getFl_valortotal():null,(ordenCompra.getFl_otroscostos())!=null?ordenCompra.getFl_otroscostos():null,(ordenCompra.getFl_valorventa())!=null?ordenCompra.getFl_valorventa():null,(ordenCompra.getFl_descuentoporcentaje())!=null?ordenCompra.getFl_descuentoporcentaje():null,(ordenCompra.getFl_subtotal())!=null?ordenCompra.getFl_subtotal():null,(ordenCompra.getTs_fechaautorizacion())!=null?convertirFecha(ordenCompra.getTs_fechaautorizacion()):null,(ordenCompra.getVc_moneda())!=null?ordenCompra.getVc_moneda():null,(ordenCompra.getVc_codigoalmacenenvio())!=null?ordenCompra.getVc_codigoalmacenenvio():null,(ordenCompra.getVc_lab())!=null?ordenCompra.getVc_lab():null,idusuario_auditoria,fechahoracreacion,(ordenCompra.getTs_fechacreacion())!=null?convertirFecha(ordenCompra.getTs_fechacreacion()):null,(ordenCompra.getVc_tipotransporte())!=null?ordenCompra.getVc_tipotransporte():null,(ordenCompra.getVc_condicionpago())!=null?ordenCompra.getVc_condicionpago():null,org.getIn_idorganizacion(),in_idorganizacionproveedora,(ordenCompra.getFl_impuestos())!=null?ordenCompra.getFl_impuestos():null,(ordenCompra.getVc_direccionfactura())!=null?ordenCompra.getVc_direccionfactura():null,(ordenCompra.getCotizacion())!=null?ordenCompra.getCotizacion().getNumeroCotizacion():null,(ordenCompra.getVc_facturara())!=null?ordenCompra.getVc_facturara():null,ordenCompra.getVc_rucproveedor().substring(2),ordenCompra.getVc_razonsocialproveedor(),ordenCompra.getVc_atenciona(),ordenCompra.getVc_emailcontacto(),ordenCompra.getVc_comentariocomprador(),ordenCompra.getFl_descuentoporcentaje(),ordenCompra.getFl_impuestosporcentaje(),ordenCompra.getVc_logo(),ordenCompra.getVc_firma(),ordenCompra.getIn_flagocdirecta(),ordenCompra.getIn_requiereinspeccion(),ordenCompra.getVc_tipooc(),org.getIn_idorganizacion(),ordenCompra.getCotizacion().getNumeroRfq(),estadocompradorOC,estadovendedorOC,ordenCompra.getTipocambio(),(ordenCompra.getTs_fechanotificacion())!=null?convertirFecha(ordenCompra.getTs_fechanotificacion()):null,(ordenCompra.getTs_fechacompromisoppto())!=null?convertirFecha(ordenCompra.getTs_fechacompromisoppto()):null,ordenCompra.getVc_numeroexpedientesiaf(),ordenCompra.getVc_tipocontratacion(),ordenCompra.getVc_objetocontratacion(),ordenCompra.getVc_descripcioncontratacion(),ordenCompra.getVc_unidadessolicitantes(),ordenCompra.getVc_unidadesinformantes(),ordenCompra.getCotizacion().getNombreRfq(),(ordenCompra.getCotizacion())!=null?ordenCompra.getCotizacion().getVersionCotizacion():null,ordenCompra.getVc_terminos(),(ordenCompra.getTs_fechainicontrato())!=null?convertirFecha(ordenCompra.getTs_fechainicontrato()):null,(ordenCompra.getTs_fechafincontrato())!=null?convertirFecha(ordenCompra.getTs_fechafincontrato()):null,ordenCompra.getVc_grupocompra(),null,null,null,null,null,null,ordenCompra.getServicioCorreo()!=null?ordenCompra.getServicioCorreo().getMsgmail():null,(ordenCompra.getOperadorLogistico())!=null?ordenCompra.getOperadorLogistico().getOrgIDOperador():null,ordenCompra.getVc_numeroap(),ordenCompra.getVc_forwarder(),ordenCompra.getFl_isc(),ordenCompra.getVc_enviarfacturaa(),ordenCompra.getVc_unidadcondpago(),ordenCompra.getVc_narrativa(),ordenCompra.getVc_tipoprecio(),ordenCompra.getIn_otrosimpuestos(),convertirFecha(ordenCompra.getTs_fechamodoc()),ordenCompra.getIn_flagmodoc(),ordenCompra.getVc_numqtprov(),ordenCompra.getVc_precioqt(),ordenCompra.getIn_tiemporecordatorio(),ordenCompra.getFl_preciofob(),ordenCompra.getIn_flagorigen(),ordenCompra.getFl_tasacambio(),ordenCompra.getDe_iva(),ordenCompra.getDe_utilidades(),ordenCompra.getDe_gastosgen() });
			}
			/// Insertar en T_ATRIBUTOXOC
			insertarOCAtributos(in_idoc,ordenCompra.getLista_atributoxoc(),idusuario_auditoria);
			insertarOCArhivos(in_idoc,ordenCompra.getLista_archivoxoc(),idusuario_auditoria);
			insertarOCProductos(in_idoc,ordenCompra.getCotizacion().getLista_prodxoc(),ordenCompra.getVc_tipooc(),org,idusuario_auditoria);
			LOGGER.info("Se ha insertado en la tabla t_oc");
			oc.setIn_idusuariocomprador(usuario_comp.getIdusuario());
			oc.setIn_idusuarioproveedor(usuario_adm_prov.getId_usuario());
			oc.setIn_idorgcompra(org.getIn_idorganizacion());
			oc.setIn_idorganizacionproveedora(in_idorganizacionproveedora);
			oc.setId_usuariocreadopor(usuario_creador.getIdusuario());
			oc.setNombre_usuariocreadopor(StringUtils.trimToEmpty(usuario_creador.getNombre())+ " " + StringUtils.trimToEmpty(usuario_creador.getAp_paterno()) + " "+ StringUtils.trimToEmpty(usuario_creador.getAp_materno()));
			return oc;
		}catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}	
	}
	
	private void insertarOCAtributos(UUID in_idoc,List<AtributoxOC> lista_atributoxoc, UUID idusuario_auditoria) {
		LOGGER.info("Inicio de creación de Atributos de OC");
		String SQL ="INSERT INTO oc.t_atributoxoc (IN_IDOC,VC_NOMBREATRIBUTO,IN_IDATRIBUTO,VC_VALORENVIADO,CH_MANDATORIO,CH_MODIFICABLE,IN_IDUNIDAD,IN_CODIGOUSUARIOCREACION,TS_FECHAHORACREACION,IN_HABILITADO) VALUES(?,?,?,?,?,?,?,?,?,?)";
		
		int size = lista_atributoxoc.size();
		for (int i=0; i<size; i++) {
			AtributoxOC elemento = lista_atributoxoc.get(i);
			java.util.Date today = new java.util.Date();
			Timestamp fechahoracreacion =new java.sql.Timestamp(today.getTime());
			try{				
				jdbcTemplate.update(SQL,new Object[] {in_idoc,elemento.getVc_nombreatributo(),elemento.getIn_idatributo(),elemento.getVc_valorenviado(),elemento.getVc_mandatorio(),elemento.getVc_modificable(),elemento.getIn_idunidad(),idusuario_auditoria,fechahoracreacion,habilitado});				
				LOGGER.info("Se ha insertado en la tabla atributosxoc");
			}catch (EmptyResultDataAccessException e) {
				e.printStackTrace();
				LOGGER.error(e.getMessage());
				throw e;
			}	
		}
	}
	
	private void insertarOCArhivos(UUID in_idoc,List<ArchivoxOC> lista_archivoxoc, UUID idusuario_auditoria) {
		//String SQL ="INSERT INTO oc.t_archivoxoc (IN_IDOC,IN_IDARCHIVOOC,VC_NOMBRE,VC_RUTA,VC_DESCRIPCION,IN_CODIGOUSUARIOCREACION,TS_FECHAHORACREACION,IN_HABILITADO) VALUES(?,?,?,?,?,?,?,?)";
		LOGGER.info("Inicio de creación de Archivos de OC");
		String SQL ="INSERT INTO oc.t_archivoxoc (IN_IDOC,IN_IDARCHIVOOC,VC_NOMBRE,VC_RUTA,VC_DESCRIPCION,IN_CODIGOUSUARIOCREACION,TS_FECHAHORACREACION,IN_HABILITADO) VALUES(?,?,?,?,?,?,?)";
		
		int size = lista_archivoxoc.size();
		for (int i=0; i<size; i++) {
			ArchivoxOC elemento = lista_archivoxoc.get(i);
			java.util.Date today = new java.util.Date();
			Timestamp fechahoracreacion =new java.sql.Timestamp(today.getTime());
			UUID in_idarchivooc= UUID.randomUUID();
			try{				
				
				//jdbcTemplate.update(SQL,new Object[] {in_idoc,in_idarchivooc,elemento.getVc_nombre(),elemento.getVc_ruta(),elemento.getVc_descripcion(),elemento.getIn_codigousuariocreacion(),fechahoracreacion,elemento.getIn_habilitado()});				
				jdbcTemplate.update(SQL,new Object[] {in_idoc,in_idarchivooc,elemento.getVc_nombre(),elemento.getVc_ruta(),elemento.getVc_descripcion(),idusuario_auditoria,fechahoracreacion,habilitado});
				LOGGER.info("Se ha insertado en la tabla archivoxoc");
				
			}catch (EmptyResultDataAccessException e) {
				e.printStackTrace();
				LOGGER.error(e.getMessage());
				throw e;
			}	
		}
	}
	
	private void insertarOCProductos(UUID in_idoc,List<ProductoxOC> lista_productoxoc,String vc_tipooc,Organiza org, UUID idusuario_auditoria) throws Exception{
		LOGGER.info("Inicio de creación de Productos por OC");
		String SQL ="INSERT INTO oc.t_productoxoc (vc_unidadproducto,vc_idtablaunidad, vc_idregistrounidad, de_impuestos,IN_IDOC,IN_IDPRODUCTOXOC,IN_IDPRODUCTO,VC_CODIGOPRODUCTOORG,VC_TIPOPRODUCTO,VC_DESCORTAPRO,IN_IDUSUARIOCREACION,TS_FECHAHORACREACION,IN_HABILITADO,CH_ESTADOPROD,IN_IDORGANIZACION,IN_POSICION,TS_FECHAENTREGAPRODUCTO,FL_PRECIOPRODUCTO,FL_CANTIDADPRODUCTO,FL_PRECIOTOTALPRODUCTO,IN_IDUNIDADPRODUCTO,vc_codigoproductomercado,vc_deslargapro,ch_commodity,in_idproductocat,in_idcategoria,in_itemrequisicion,fl_cantidadproductopend,in_idprodb2m,in_idrequerimiento,in_posicionreq,fl_cantidadrecepcionada) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		ProductoxOC producto = null;
		Maestra maestra_unidad = new Maestra();
		
		int size = lista_productoxoc.size();		
		for (int i=0; i<size; i++) {
			java.util.Date today = new java.util.Date();
			Timestamp fechahoracreacion =new java.sql.Timestamp(today.getTime());
			ProductoxOC elemento = lista_productoxoc.get(i);
			UUID in_idproducto = null;
			producto = null;
			UUID in_idproductoxoc = UUID.randomUUID();

			// Si es un nuevo producto
			if (elemento.getVc_codigoproductoorg()!=null) {
				producto = buscar_producto(elemento.getVc_codigoproductoorg(),org.getIn_idorganizacion());
			}
			if ( producto == null) {
				LOGGER.info("Hay un nuevo producto");
				in_idproducto = UUID.randomUUID();
				// insertar en T_PRODUCTO
				//hacer el select en tabla de equivalencias
				elemento.setIn_idorganizacion(org.getIn_idorganizacion());
				insertarProducto(in_idproducto,elemento, idusuario_auditoria);
				LOGGER.info("insert� nuevo producto");
			}			 
			
			try{
				
				LOGGER.info("Antes de insertar en tabla productoxoc");

				//elemento.setIn_idunidadproducto(in_idunidadproducto);
				if (elemento.getVc_unidadproducto()!=null && !elemento.getVc_unidadproducto().equals("-")) {
					maestra_unidad = buscar_maestra(elemento.getVc_unidadproducto());
					//in_idunidad = 2;//obtener de la tabla de tablas query
						if (maestra_unidad!=null && maestra_unidad.getVc_idregistro()!=null && maestra_unidad.getVc_idtabla()!=null) {
							elemento.setVc_idregistrounidad(maestra_unidad.getVc_idregistro());
							elemento.setVc_idtablaunidad(maestra_unidad.getVc_idtabla());
						}else {
							throw new Exception("No existe unidad de producto");
						}
				}else {
					elemento.setVc_idregistrounidad(null);
					elemento.setVc_idtablaunidad(null);
					//throw new Exception("No existe unidad de producto");
				}
				jdbcTemplate.update(SQL,new Object[] {maestra_unidad.getVc_iso(),elemento.getVc_idtablaunidad(), elemento.getVc_idregistrounidad(),elemento.getFl_impuestos(), in_idoc,in_idproductoxoc,in_idproducto,elemento.getVc_codigoproductoorg(),elemento.getVc_tipoproducto(),elemento.getVc_descortapro(),idusuario_auditoria,fechahoracreacion,habilitado,elemento.getVc_estadoprod(),elemento.getIn_idorganizacion(),elemento.getIn_posicion(),convertirFecha(elemento.getTs_fechaentregaproducto()),elemento.getFl_precioproducto(),elemento.getFl_cantidadproducto(),elemento.getFl_preciototalproducto(),elemento.getIn_idunidadproducto(),elemento.getVc_codigoproductomercado(),elemento.getVc_deslargapro(),elemento.getCh_commodity(),elemento.getIn_idproductocat(),elemento.getIn_idcategoria(),elemento.getIn_itemrequisicion(),elemento.getFl_cantidadproductopend(),elemento.getIn_idprodb2m(),elemento.getIn_idrequerimiento(),elemento.getIn_posicionreq(),elemento.getFl_cantidadrecepcionada()});
				LOGGER.info("Se ha insertado en la tabla productoxoc");
			}catch (EmptyResultDataAccessException e) {
				e.printStackTrace();
				LOGGER.error(e.getMessage());
				throw e;
			}
			
			if (vc_tipooc!=null && vc_tipooc.equals("S")) {
				LOGGER.info("Es orden de compra de servicios no se inserta atributos por producto");
			}
			else
				insertarOCAtributosxProducto(in_idoc,in_idproductoxoc,elemento.getLista_atributoxprod(),idusuario_auditoria);
			//if es tipo servicio
			if (vc_tipooc!=null && vc_tipooc.equals("S"))
				insertarOCSubItem(in_idoc,in_idproductoxoc,elemento.getLista_subitemxprod(),idusuario_auditoria);//Evelin Ribbeck
		}
	}
	
	private void insertarProducto(UUID in_idproducto,ProductoxOC producto, UUID idusuario_auditoria) {
		LOGGER.info("Inicio de creación de Productos");
		String SQL ="INSERT INTO producto.t_producto(in_idproducto,in_idorganizacion,vc_nombre,vc_codigoproductoorg,in_codigousuariocreacion,in_habilitado) VALUES(?,?,?,?,?,?)";

		try{
			jdbcTemplate.update(SQL,new Object[] {in_idproducto,producto.getIn_idorganizacion(),producto.getVc_descortapro(),producto.getVc_codigoproductoorg(),idusuario_auditoria,habilitado});
			LOGGER.info("Se ha insertado en la tabla T_PRODUCTO");
		}catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
	}
	
	private void updateOrdenCompra(OrdenCompra oc, UUID idusuario_auditoria) {		
		LOGGER.info("Inicio de actualización de orden de compra");
		//String SQL ="INSERT INTO producto.t_producto(in_idproducto,in_idorganizacion,vc_nombre,vc_codigoproductoorg,in_codigousuariocreacion,in_habilitado) VALUES(?,?,?,?,?,?)";
		String SQL = "UPDATE oc.t_oc SET vc_estadocomprador = ?, vc_estadoproveedor = ?, in_codigousuariomodificacion = ? WHERE in_idoc = ? ";
		try{
			jdbcTemplate.update(SQL,new Object[] {estadoanuladoOC,estadoanuladoOC, idusuario_auditoria, oc.getIn_idoc()});
			LOGGER.info("Se ha versionado en la tabla T_OC "+oc.getIn_idoc());
		}catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
	}
	
	private void insertarOCAtributosxProducto(UUID in_idoc,UUID in_idproductoxoc,List<AtributoxProductoxOC> lista_atributoxprod, UUID idusuario_auditoria) throws Exception {
		LOGGER.info("Inicio de atributos de producto por OC");
		UUID in_idatributo = UUID.randomUUID();
		String SQL ="INSERT INTO oc.t_atributoxproductoxoc (vc_idtablaoperador,vc_idregistrooperador,vc_idtablaunidad,vc_idregistrounidad,vc_idoperador,vc_nombreunidad,IN_IDPRODUCTOXOC,IN_IDATRIBUTO,IN_IDOC,VC_VALORENVIADO,IN_IDUNIDAD,VC_NOMBREATRIBUTO,IN_CODIGOUSUARIOCREACION,TS_FECHAHORACREACION,IN_HABILITADO,ch_mandatorio,ch_modificable,vc_valorrealorg) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		Maestra maestra_operador = new Maestra(); 
		Maestra maestra_unidad = new Maestra();
		
		int size = lista_atributoxprod.size();
		for (int i=0; i<size; i++) {
			AtributoxProductoxOC elemento = lista_atributoxprod.get(i);
			java.util.Date today = new java.util.Date();
			Timestamp fechahoracreacion =new java.sql.Timestamp(today.getTime());
			try{				
				//elemento.getIn_idatributo() buscar en la tabla de tablas harcode con el campo de nombre de atributo
				in_idatributo = UUID.randomUUID();
				//int in_idoperador = 5;//obtener de la tabla de tablas con el query de vc_operador elemento.getvc_operador
				//int in_idunidad;
				if (elemento.getVc_nombreunidad()!=null && !elemento.getVc_nombreunidad().equals("-")) {
					maestra_unidad = buscar_maestra(elemento.getVc_nombreunidad());
					//in_idunidad = 2;//obtener de la tabla de tablas query
					if (maestra_unidad!=null && maestra_unidad.getVc_idregistro()!=null && maestra_unidad.getVc_idtabla()!=null) {
						elemento.setVc_idregistrounidad(maestra_unidad.getVc_idregistro());
						elemento.setVc_idtablaunidad(maestra_unidad.getVc_idtabla());
					}else {
						throw new Exception("No existe unidad de atributo");
					}
					//elemento.setIn_idunidad(in_idunidad);
				}else {
					//throw new Exception("No existe unidad de producto");
				}
				
				if (elemento.getVc_operador()!=null && !elemento.getVc_operador().equals("-")) {
					maestra_operador = buscar_maestra(elemento.getVc_operador());
					//in_idunidad = 2;//obtener de la tabla de tablas query
					if (maestra_operador!=null && maestra_operador.getVc_idregistro()!=null && maestra_operador.getVc_idtabla()!=null) {
						elemento.setVc_idregistrooperador(maestra_operador.getVc_idregistro());
						elemento.setVc_idtablaoperador(maestra_operador.getVc_idtabla());
					}else {
						elemento.setVc_idregistrooperador(null);
						elemento.setVc_idtablaoperador(null);
					}
					//elemento.setIn_idunidad(in_idunidad);
				}else {
					elemento.setVc_idregistrooperador(null);
					elemento.setVc_idtablaoperador(null);
				}
				jdbcTemplate.update(SQL,new Object[] {elemento.getVc_idtablaoperador(),elemento.getVc_idregistrooperador(),elemento.getVc_idtablaunidad(),elemento.getVc_idregistrounidad(),elemento.getVc_operador(),maestra_unidad.getVc_iso(),in_idproductoxoc,in_idatributo,in_idoc,elemento.getVc_valorenviado(),elemento.getIn_idunidad(),elemento.getVc_nombreatributo(),idusuario_auditoria,fechahoracreacion,habilitado,elemento.getCh_mandatorio(),elemento.getVc_modificable(),elemento.getVc_valorrealorg()});				
				LOGGER.info("Se ha insertado en la tabla atributoxproductoxoc");
			}catch (EmptyResultDataAccessException e) {
				e.printStackTrace();
				LOGGER.error(e.getMessage());
				throw e;
			}	
		}
	}
	
	private void insertarOCSubItem(UUID in_idoc,UUID in_idproductoxoc,List<SubItemxProductoxOC> lista_subitemxproductoxoc, UUID idusuario_auditoria) throws Exception {
		LOGGER.info("Inicio de subitem de producto por OC");
		String SQLinsert = "";
        String columnas = " INSERT INTO oc.t_subitemxprodxoc  (in_idoc, in_idproductoxoc ";
        String valores = "VALUES (?, ? ";
        List<Object> parametros = new ArrayList<>();
		Maestra maestra_unidad = new Maestra();
        
		int size = lista_subitemxproductoxoc.size();
		for (int i=0; i<size; i++) {
			//inicializar variables
			SQLinsert = "";
			columnas = " INSERT INTO oc.t_subitemxprodxoc  (in_idoc, in_idproductoxoc ";
			valores = "VALUES (?, ? ";
			parametros = new ArrayList<>();
			parametros.add(in_idoc);
			parametros.add(in_idproductoxoc);
			//generar UID todos son inserts
			UUID in_idsubitemxprodxoc = UUID.randomUUID();
			SubItemxProductoxOC elemento = lista_subitemxproductoxoc.get(i);
			java.util.Date today = new java.util.Date();
			Timestamp fechahoracreacion =new java.sql.Timestamp(today.getTime());
			try{				
				//if (elemento.getIn_idsubitemxprodxoc() != null) {
					parametros.add(in_idsubitemxprodxoc);
					columnas=columnas+", in_idsubitemxprodxoc";
		            valores=valores+", ?";
		        
		    	if (elemento.getVc_codigosubitemorg() != null) {
		    		parametros.add(elemento.getVc_codigosubitemorg());
		    		columnas=columnas+", vc_codigosubitemorg";
		            valores=valores+",? ";
		        }
				if (elemento.getIn_tiposubitem() != null) {
					parametros.add(elemento.getIn_tiposubitem());
					columnas=columnas+", in_tiposubitem";
		            valores=valores+", ?";
		        }
		    	if (elemento.getVc_descortasubitem() != null) {
		    		parametros.add(elemento.getVc_descortasubitem());
		    		columnas=columnas+", vc_descortasubitem";
		            valores=valores+",? ";
		        }
		    	if (elemento.getVc_deslargasubitem() != null) {
		    		parametros.add(elemento.getVc_deslargasubitem());
		    		columnas=columnas+", vc_deslargasubitem";
		            valores=valores+",? ";
		        }
		    	if (elemento.getVc_campo1() != null) {
		    		parametros.add(elemento.getVc_campo1());
		    		columnas=columnas+", vc_campo1";
		            valores=valores+",? ";
		        }
		    	if (elemento.getVc_campo2() != null) {
		    		parametros.add(elemento.getVc_campo2());
		    		columnas=columnas+", vc_campo2";
		            valores=valores+",? ";
		        }
		    	if (elemento.getVc_campo3() != null) {
		    		parametros.add(elemento.getVc_campo3());
		    		columnas=columnas+", vc_campo3";
		            valores=valores+",? ";
		        }
		    	if (elemento.getVc_campo4() != null) {
		    		parametros.add(elemento.getVc_campo4());
		    		columnas=columnas+", vc_campo4";
		            valores=valores+",? ";
		        }
		    	if (elemento.getVc_campo5() != null) {
		    		parametros.add(elemento.getVc_campo5());
		    		columnas=columnas+", vc_campo5";
		            valores=valores+",? ";
		        }
		    	if (elemento.getIn_idusuariocreacion() != null) {
		    		parametros.add(elemento.getIn_idusuariocreacion());
		    		columnas=columnas+", in_idusuariocreacion";
		            valores=valores+",? ";
		        }
		    	if (elemento.getIn_idusuariomodificacion() != null) {	
		    		parametros.add(elemento.getIn_idusuariomodificacion());
		    		columnas=columnas+", in_idusuariomodificacion";
		            valores=valores+",? ";
		        }
		    	/*if (elemento.getTs_fechahoracreacion() != null) {*/
		    		parametros.add(fechahoracreacion);
		    		columnas=columnas+", ts_fechahoracreacion";
		            valores=valores+",? ";
		        /*}*/
		    	/*if (elemento.getTs_fechahoramodificacion() != null) {*/
		            parametros.add(fechahoracreacion);
		    		columnas=columnas+", ts_fechahoramodificacion";
		            valores=valores+",? ";
		        /*}*/		    	
		    	if (elemento.getIn_habilitado() != null) {
		    		parametros.add(habilitado);
		    		columnas=columnas+", in_habilitado";
		            valores=valores+",? ";
		        }
		    	if (elemento.getVc_estadosubitem() != null) {
		    		parametros.add(elemento.getVc_estadosubitem());
		    		columnas=columnas+", vc_estadosubitem";
		            valores=valores+",? ";
		        }
		    	if (elemento.getIn_idorganizacion() != null) {
		    		parametros.add(elemento.getIn_idorganizacion());
		    		columnas=columnas+", in_idorganizacion";
		            valores=valores+",? ";
		        }
		    	if (elemento.getIn_subitemposicion() != null) {
		    		parametros.add(elemento.getIn_subitemposicion());
		    		columnas=columnas+", in_subitemposicion";
		            valores=valores+",? ";
		        }
		    	if (elemento.getTs_fechaentregasubitem()!= null) {
		    		parametros.add(convertirFecha(elemento.getTs_fechaentregasubitem()));
		    		columnas=columnas+", ts_fechaentregasubitem";
		            valores=valores+",? ";
		        }
		    	if (elemento.getFl_preciounitariosubitem()!= null) {
		    		parametros.add(elemento.getFl_preciounitariosubitem());
		    		columnas=columnas+", fl_preciosubitem";
		            valores=valores+",? ";
		        }
		    	if (elemento.getFl_cantidadsubitem()!= null) {
		    		parametros.add(elemento.getFl_cantidadsubitem());
		    		columnas=columnas+", fl_cantidadsubitem";
		            valores=valores+",? ";
		        }
		    	if (elemento.getFl_preciosubitem()!= null) {
		    		parametros.add(elemento.getFl_preciosubitem());
		    		columnas=columnas+", fl_preciototalsubitem";
		            valores=valores+",? ";
		        }
				if (elemento.getVc_unidadsubitem()!=null && !elemento.getVc_unidadsubitem().equals("-")) {
					maestra_unidad = buscar_maestra(elemento.getVc_unidadsubitem());
					//in_idunidad = 2;//obtener de la tabla de tablas query
					if (maestra_unidad!=null && maestra_unidad.getVc_idregistro()!=null && maestra_unidad.getVc_idtabla()!=null) {
						elemento.setVc_idregistrounidad(maestra_unidad.getVc_idregistro());
						elemento.setVc_idtablaunidad(maestra_unidad.getVc_idtabla());
					}else {
						throw new Exception("No existe unidad de subproducto");
					}
					//elemento.setIn_idunidad(in_idunidad);
				}else {
					elemento.setVc_idregistrounidad(null);
					elemento.setVc_idtablaunidad(null);
				}
				
				if (elemento.getVc_idregistrounidad() != null) {
		    		parametros.add(elemento.getVc_idregistrounidad());
		    		columnas=columnas+", vc_idregistrounidad";
		            valores=valores+",? ";
		        }
		    	
		    	if (elemento.getVc_idtablaunidad() != null) {
		    		parametros.add(elemento.getVc_idtablaunidad());
		    		columnas=columnas+", vc_idtablaunidad";
		            valores=valores+",? ";
		        }		 
		    	
		    	if (elemento.getIn_idunidadsubitem()!= null) {
		    		parametros.add(elemento.getIn_idunidadsubitem());
		    		columnas=columnas+", in_idunidadsubitem";
		            valores=valores+",? ";
		        }
		    	
		    	if (maestra_unidad!= null) {
		    		parametros.add(maestra_unidad.getVc_iso());
		    		columnas=columnas+", vc_nombreunidad";
		            valores=valores+",? ";
		        }
		    	if (elemento.getFl_cantidadsubitempend()!= null) {
		    		parametros.add(elemento.getFl_cantidadsubitempend());
		    		columnas=columnas+", fl_cantidadsubitempend";
		            valores=valores+",? ";
		        }
		    	if (elemento.getVc_numeroparte()!= null) {
		    		parametros.add(elemento.getVc_numeroparte());
		    		columnas=columnas+", vc_numeroparte";
		            valores=valores+",? ";
		        }
		    	if (elemento.getVc_subitemposicion()!= null) {
		    		parametros.add(elemento.getVc_subitemposicion());
		    		columnas=columnas+", vc_subitemposicion";
		            valores=valores+",? ";
		        }
		    	if (elemento.getIn_idrequerimiento()!= null) {
		    		parametros.add(elemento.getIn_idrequerimiento());
		    		columnas=columnas+", in_idrequerimiento";
		            valores=valores+",? ";
		        }
		    	if (elemento.getIn_posicionreq()!= null) {
		    		parametros.add(elemento.getIn_posicionreq());
		    		columnas=columnas+", in_posicionreq";
		            valores=valores+",? ";
		        }
		    	columnas=columnas+" )";
	            valores=valores+" )";
	            SQLinsert=columnas+valores;
		    	/*if (estado != null) {									// debe recibir la descripción corta del estado
		        	if (!estado.equalsIgnoreCase("NONE")) {
		        		condiciones.add("vc_estadocomprador = ? ");
		                parametros.add(estado);
		        	}
		        }*/
				//jdbcTemplate.update(SQL,new Object[] {in_idoc,in_idproductoxoc,elemento.getIn_idsubitemxprodxoc(),elemento.getVc_codigosubitemorg(),elemento.getIn_tiposubitem(),elemento.getVc_descortasubitem(),elemento.getVc_deslargasubitem(),elemento.getVc_campo1(),elemento.getVc_campo2(),elemento.getVc_campo3(),elemento.getVc_campo4(),elemento.getVc_campo5(),elemento.getIn_idusuariocreacion(),elemento.getIn_idusuariomodificacion(),fechahoracreacion,fechahoracreacion,elemento.getIn_habilitado(),elemento.getVc_estadosubitem(),elemento.getIn_idorganizacion(),elemento.getIn_subitemposicion(),elemento.getTs_fechaentregasubitem(),elemento.getFl_preciosubitem(),elemento.getFl_cantidadsubitem(),elemento.getFl_preciototalsubitem(),elemento.getIn_idunidadsubitem(),elemento.getFl_cantidadsubitempend(),elemento.getVc_numeroparte(),elemento.getVc_subitemposicion(),elemento.getIn_idrequerimiento(),elemento.getIn_posicionreq()});
	            LOGGER.info(SQLinsert);
	            LOGGER.info(parametros.toArray()+" parametros");
	            for (int t=0; t<parametros.size();t++) {
	            	LOGGER.info(parametros.get(t)+ " elemento " + t);
	            }
	            jdbcTemplate.update(SQLinsert,parametros.toArray());
	            LOGGER.info("Se ha insertado en la tabla t_subitemxprodxoc");
				insertarOCAtributoxSubItem(in_idsubitemxprodxoc,elemento.getLista_atributoxsubitemxprod());		

			}catch (EmptyResultDataAccessException e) {
				e.printStackTrace();
				LOGGER.error(e.getMessage());
				throw e;
			}	
		}
	}

	private void insertarOCAtributoxSubItem(UUID in_idsubitemxprodxoc,List<AtributoxSubItemxProductoxOC> lista_atributoxsubitemxprod) throws Exception  {
		LOGGER.info("Inicio de atributo de subitem de producto por OC");
		String SQLinsert = "";
        String columnas = " INSERT INTO oc.t_atributoxsubitemxprodxoc  (in_idsubitemxprodxoc ";
        String valores = "VALUES (? ";
        List<Object> parametros = new ArrayList<>();
		Maestra maestra_operador = new Maestra(); 
		Maestra maestra_unidad = new Maestra();
        
		int size = lista_atributoxsubitemxprod.size();
		for (int i=0; i<size; i++) {
			//inicializar variables
			SQLinsert = "";
			columnas = " INSERT INTO oc.t_atributoxsubitemxprodxoc  (in_idsubitemxprodxoc ";
			valores = "VALUES (? ";
			parametros = new ArrayList<>();
			parametros.add(in_idsubitemxprodxoc);
			//generar UID todos son inserts
			UUID in_idatributoxsubitemxprodxoc = UUID.randomUUID();
			AtributoxSubItemxProductoxOC elemento = lista_atributoxsubitemxprod.get(i);
			java.util.Date today = new java.util.Date();
			Timestamp fechahoracreacion =new java.sql.Timestamp(today.getTime());
			try{				
				//if (elemento.getIn_idsubitemxprodxoc() != null) {
				parametros.add(in_idatributoxsubitemxprodxoc);
				columnas=columnas+", in_idatributoxsubitemxprodxoc";
	            valores=valores+", ?";
	            if (elemento.getVc_nombreunidad()!=null && !elemento.getVc_nombreunidad().equals("-")) {
					maestra_unidad = buscar_maestra(elemento.getVc_nombreunidad());
					//in_idunidad = 2;//obtener de la tabla de tablas query
					if (maestra_unidad!=null && maestra_unidad.getVc_idregistro()!=null && maestra_unidad.getVc_idtabla()!=null) {
						elemento.setVc_idregistrounidad(maestra_unidad.getVc_idregistro());
						elemento.setVc_idtablaunidad(maestra_unidad.getVc_idtabla());
					}else {
						throw new Exception("No existe unidad de atributo subproducto");
					}
					//elemento.setIn_idunidad(in_idunidad);
				}else {
					elemento.setVc_idregistrounidad(null);
					elemento.setVc_idtablaunidad(null);
				}
				
				if (elemento.getVc_operador()!=null && !elemento.getVc_operador().equals("-")) {
					maestra_operador = buscar_maestra(elemento.getVc_operador());
					//in_idunidad = 2;//obtener de la tabla de tablas query
					if (maestra_operador!=null && maestra_operador.getVc_idregistro()!=null && maestra_operador.getVc_idtabla()!=null) {
						elemento.setVc_idregistrooperador(maestra_operador.getVc_idregistro());
						elemento.setVc_idtablaoperador(maestra_operador.getVc_idtabla());
					}else {
						elemento.setVc_idregistrooperador(null);
						elemento.setVc_idtablaoperador(null);
					}
					//elemento.setIn_idunidad(in_idunidad);
				}else {
					elemento.setVc_idregistrooperador(null);
					elemento.setVc_idtablaoperador(null);
				}
				
		    	if (elemento.getVc_idregistrounidad() != null) {
		    		parametros.add(elemento.getVc_idregistrounidad());
		    		columnas=columnas+", vc_idregistrounidad";
		            valores=valores+",? ";
		        }
		    	
		    	if (elemento.getVc_idtablaunidad() != null) {
		    		parametros.add(elemento.getVc_idtablaunidad());
		    		columnas=columnas+", vc_idtablaunidad";
		            valores=valores+",? ";
		        }		    	
		    	
		    	if (elemento.getVc_idregistrooperador() != null) {
		    		parametros.add(elemento.getVc_idregistrooperador());
		    		columnas=columnas+", vc_idregistrooperador";
		            valores=valores+",? ";
		        }
		    	
		    	if (elemento.getVc_idtablaoperador() != null) {
		    		parametros.add(elemento.getVc_idtablaoperador());
		    		columnas=columnas+", vc_idtablaoperador";
		            valores=valores+",? ";
		        }
				
		    	if (elemento.getVc_valorenviado() != null) {
		    		parametros.add(elemento.getVc_valorenviado());
		    		columnas=columnas+", vc_valorenviado";
		            valores=valores+",? ";
		        }
				if (elemento.getVc_mandatorio() != null) {
					parametros.add(elemento.getVc_mandatorio());
					columnas=columnas+", vc_mandatorio";
		            valores=valores+", ?";
		        }
		    	if (elemento.getVc_modificable() != null) {
		    		parametros.add(elemento.getVc_modificable());
		    		columnas=columnas+", vc_modificable";
		            valores=valores+",? ";
		        }
		    	/*if (In_idunidad() != null) {
		    		parametros.add(elemento.getIn_idunidad());
		    		columnas=columnas+", in_idunidad";
		            valores=valores+",? ";
		        }*/
		    	if (maestra_unidad != null) {
		    		parametros.add(maestra_unidad.getVc_iso());
		    		columnas=columnas+", vc_nombreunidad";
		            valores=valores+",? ";
		        }
		    	if (elemento.getVc_unidaderror() != null) {
		    		parametros.add(elemento.getVc_unidaderror());
		    		columnas=columnas+", vc_unidaderror";
		            valores=valores+",? ";
		        }
		    	if (elemento.getVc_operador() != null) {
		    		parametros.add(elemento.getVc_operador());
		    		columnas=columnas+", vc_idoperador";
		            valores=valores+",? ";
		        }
		    	if (elemento.getIn_flagcodificado() != null) {
		    		parametros.add(elemento.getIn_flagcodificado());
		    		columnas=columnas+", in_flagcodificado";
		            valores=valores+",? ";
		        }
		    	if (elemento.getVc_valorrealorg() != null) {
		    		parametros.add(elemento.getVc_valorrealorg());
		    		columnas=columnas+", vc_valorrealorg";
		            valores=valores+",? ";
		        }
		    	if (elemento.getVc_nombreatributo() != null) {
		    		parametros.add(elemento.getVc_nombreatributo());
		    		columnas=columnas+", vc_nombreatributo";
		            valores=valores+",? ";
		        }
		    	if (elemento.getVc_campo1() != null) {
		    		parametros.add(elemento.getVc_campo1());
		    		columnas=columnas+", vc_campo1";
		            valores=valores+",? ";
		        }
		    	if (elemento.getVc_campo2() != null) {
		    		parametros.add(elemento.getVc_campo2());
		    		columnas=columnas+", vc_campo2";
		            valores=valores+",? ";
		        }
		    	if (elemento.getVc_campo3() != null) {
		    		parametros.add(elemento.getVc_campo3());
		    		columnas=columnas+", vc_campo3";
		            valores=valores+",? ";
		        }
		    	if (elemento.getVc_campo4() != null) {
		    		parametros.add(elemento.getVc_campo4());
		    		columnas=columnas+", vc_campo4";
		            valores=valores+",? ";
		        }
		    	if (elemento.getVc_campo5() != null) {
		    		parametros.add(elemento.getVc_campo5());
		    		columnas=columnas+", vc_campo5";
		            valores=valores+",? ";
		        }
		    	if (elemento.getIn_codigousuariocreacion() != null) {
		    		parametros.add(elemento.getIn_codigousuariocreacion());
		    		columnas=columnas+", in_codigousuariocreacion";
		            valores=valores+",? ";
		        }
		    	if (elemento.getIn_codigousuariomodificacion() != null) {	
		    		parametros.add(elemento.getIn_codigousuariomodificacion());
		    		columnas=columnas+", in_codigousuariomodificacion";
		            valores=valores+",? ";
		        }
		    	/*if (elemento.getTs_fechahoracreacion() != null) {*/
		    		parametros.add(fechahoracreacion);
		    		columnas=columnas+", ts_fechahoracreacion";
		            valores=valores+",? ";
		        /*}*/
		    	/*if (elemento.getTs_fechahoramodificacion() != null) {*/
		            parametros.add(fechahoracreacion);
		    		columnas=columnas+", ts_fechahoramodificacion";
		            valores=valores+",? ";
		        /*}*/		    	
		    	if (elemento.getIn_habilitado() != null) {
		    		parametros.add(habilitado);
		    		columnas=columnas+", in_habilitado";
		            valores=valores+",? ";
		        }
		    	columnas=columnas+" )";
	            valores=valores+" )";
	            SQLinsert=columnas+valores;
		    	/*if (estado != null) {									// debe recibir la descripción corta del estado
		        	if (!estado.equalsIgnoreCase("NONE")) {
		        		condiciones.add("vc_estadocomprador = ? ");
		                parametros.add(estado);
		        	}
		        }*/
				//jdbcTemplate.update(SQL,new Object[] {in_idoc,in_idproductoxoc,elemento.getIn_idsubitemxprodxoc(),elemento.getVc_codigosubitemorg(),elemento.getIn_tiposubitem(),elemento.getVc_descortasubitem(),elemento.getVc_deslargasubitem(),elemento.getVc_campo1(),elemento.getVc_campo2(),elemento.getVc_campo3(),elemento.getVc_campo4(),elemento.getVc_campo5(),elemento.getIn_idusuariocreacion(),elemento.getIn_idusuariomodificacion(),fechahoracreacion,fechahoracreacion,elemento.getIn_habilitado(),elemento.getVc_estadosubitem(),elemento.getIn_idorganizacion(),elemento.getIn_subitemposicion(),elemento.getTs_fechaentregasubitem(),elemento.getFl_preciosubitem(),elemento.getFl_cantidadsubitem(),elemento.getFl_preciototalsubitem(),elemento.getIn_idunidadsubitem(),elemento.getFl_cantidadsubitempend(),elemento.getVc_numeroparte(),elemento.getVc_subitemposicion(),elemento.getIn_idrequerimiento(),elemento.getIn_posicionreq()});
	            LOGGER.info(SQLinsert);
	            jdbcTemplate.update(SQLinsert,parametros.toArray());
				//jdbcTemplate.update(SQL,new Object[] {in_idproductoxoc,elemento.getIn_idatributo(),in_idoc,elemento.getVc_valorenviado(),elemento.getIn_idunidad(),elemento.getIn_idoperador(),elemento.getVc_nombreatributo(),elemento.getIn_codigousuariocreacion(),fechahoracreacion,elemento.getIn_habilitado()});				
	            LOGGER.info("Se ha insertado en la tabla atributoxproductoxoc");
			}catch (EmptyResultDataAccessException e) {
				e.printStackTrace();
				LOGGER.error(e.getMessage());
				throw e;
			}	
		}
	}
	
	/*@Transactional(rollbackFor = Exception.class)
	public List<UUID> updateOC(CEOrdenCompras cEOrdenCompras,Organizacion org) {
		LOGGER.info("Inicio de update de OC");
		List<UUID> lista_idoc = new ArrayList<UUID>();	
		Boolean flag = false; 

		POUPLOADMQ pouploadmq = cEOrdenCompras.getPouploadmq();
		List<OrdenCompra> list_oc = pouploadmq.getLista_oc();
		
		for(OrdenCompra ordenCompra : list_oc) {
			
			//OrdenCompra oc= new OrdenCompra();
			LOGGER.info(ordenCompra+" ordenCompra");
			UUID in_idoc= UUID.randomUUID();
			lista_idoc.add(in_idoc);
			//select en bd de la oc
			OrdenCompra actualizarOC = new OrdenCompra(); 
			actualizarOC=selectOrdenCompra(ordenCompra);
			if (actualizarOC!=null) { 
			//actualiza la oc a anulada y crea una con la version incrementada en 1 solo se debe setear la orden de compra con una nueva version todo lo
			//dem�s se realiza de nuevo
				LOGGER.info(actualizarOC.getIn_idoc()+" idoc para actualizar");
				updateOrdenCompra(actualizarOC);
				incrementaVersion(actualizarOC,ordenCompra);
			}
			LOGGER.info(ordenCompra.getIn_version()+" in_version para actualizar");
			insertarOrdenCompra(in_idoc,ordenCompra,pouploadmq,org);
			
			/// Insertar en T_ATRIBUTOXOC
			//insertarOCAtributos(in_idoc,ordenCompra.getLista_atributoxoc());
				
			/// Insertar en ACHIVOSXOC
			//insertarOCArhivos(in_idoc,ordenCompra.getLista_archivoxoc());
			
			/// Insertar en T_PRODUCTOXOC
			/// Insertar en T_ATRIBUTOXPRODUCTOXOC
			//insertarOCProductos(in_idoc,ordenCompra.getLista_prodxoc(),ordenCompra.getVc_tipooc());
			
			/// Insertar en T_SUBITEMXPRODUCTOXOC Evelin Ribbeck R
			//insertarOCSubItem(in_idoc,ordenCompra.getLista_prodxoc());		
							
		}
		return lista_idoc;

	}*/
		
	private Timestamp convertirFecha(String fecha) {
		if (fecha==null)
			return null;
		if (fecha.equals(""))
			return null;
		
		//SimpleDateFormat sdf;
		/*
		if (fecha.length() > 11) {
	        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    } else {
	        sdf = new SimpleDateFormat("yyyy-MM-dd");
	    }
	    sdf.setLenient(false);
	    java.util.Date utilTime;
		try {
			utilTime = sdf.parse(fecha);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	    Timestamp ts = new Timestamp(utilTime.getTime());
	    return ts;
	    */
		try {
			Date utiltime = null;
			utiltime = ISO8601Utils.parse(fecha, new ParsePosition(0));
			//return new Timestamp(utiltime.getTime());
			return Timestamp.valueOf(utiltime.toInstant().atZone(ZoneId.of("-05:00")).toLocalDateTime());
			//return Timestamp.valueOf(LocalDateTime.now(ZoneId.of("-05:00")));
		}
		catch(Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}
	
	private OrdenCompra selectOrdenCompra(OrdenCompra oc)  {
		LOGGER.info("Inicio de búsqueda de OC");
		OrdenCompra ordenCompra = new OrdenCompra();
		//List<OrdenCompra> lista = new ArrayList<>();
		String ch_numeroseguimiento = oc.getVc_numeroseguimiento();
		LOGGER.info("ch_numeroseguimiento "+ch_numeroseguimiento);
		String SQL = "SELECT  MAX(ts_fechahoracreacion) fecha, ch_numeroseguimiento,in_idoc, in_version  " + 
				"		FROM oc.t_oc " + 
				"       WHERE ch_numeroseguimiento = ? "	+
				"		GROUP BY ch_numeroseguimiento, in_idoc " + 
				"		ORDER BY fecha DESC";//and que no esta anulada, *buscar la que tiene la maxima version
		
		List<OrdenCompra> l_ordencompra = null;
	
		try{
			l_ordencompra = jdbcTemplate.query(SQL,new Object[] {ch_numeroseguimiento}, this::mapParam);
		}catch (Exception e) { //EmptyResultDataAccessException
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
	
		//System.out.println(l_ordencompra.get(0) + "primer elemento fecha maxima ");
		if (l_ordencompra != null && l_ordencompra.size()>0)
			ordenCompra = l_ordencompra.get(0);
		else return null;

		return ordenCompra;
	}
	
	public Maestra buscar_maestra(String estado)  {
		
		Maestra maestra = new Maestra();
		
		String SQL = "SELECT * FROM MASTER.T_MAESTRA WHERE "	+
					" vc_equivalencia = ? ";
		List<Maestra> l_maestra = null;
		
		try{
			l_maestra = jdbcTemplate.query(SQL,new Object[] {estado}, this::mapParamMaestra);
		}catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
	
		if (l_maestra != null && l_maestra.size()>0)
			maestra = l_maestra.get(0);
		else return null;

		return maestra;
	}
	
	public OrdenCompra mapParam(ResultSet rs, int i) throws SQLException {

		OrdenCompra ordencompra = new OrdenCompra();

		ordencompra.setVc_numeroseguimiento(StringUtils.trimToEmpty(rs.getString("ch_numeroseguimiento")));

        ordencompra.setIn_idoc(UUID.fromString((StringUtils.trimToEmpty(rs.getString("in_idoc")))));
        
        ordencompra.setIn_version(rs.getInt("in_version"));
	
        return ordencompra;
	}
	
	public Maestra mapParamMaestra(ResultSet rs, int i) throws SQLException {

		Maestra maestra = new Maestra();

		maestra.setVc_idtabla(StringUtils.trimToEmpty(rs.getString("vc_idtabla")));

		maestra.setVc_desc_corta(StringUtils.trimToEmpty(rs.getString("vc_desc_corta")));
		
		maestra.setVc_idregistro(StringUtils.trimToEmpty(rs.getString("vc_idregistro")));
		
		maestra.setVc_iso(StringUtils.trimToEmpty(rs.getString("vc_iso")));
	
        return maestra;
	}
	
	public void incrementaVersion(OrdenCompra ocantigua,OrdenCompra oc){

		Integer version_antigua=ocantigua.getIn_version();
		LOGGER.info(version_antigua+" versi�n antigua");
		if (version_antigua!=null) {
			oc.setIn_version(++version_antigua);
		}
	}

	public Organizacion mapParamOrg(ResultSet rs, int i) throws SQLException {

		Organizacion org = new Organizacion();

		org.setVc_ruc(StringUtils.trimToEmpty(rs.getString("vc_ruc")));

		org.setIn_idorganizacion(UUID.fromString((rs.getString("in_idorganizacion"))));
	
        return org;
	}
	
	public ProductoxOC buscar_producto(String vc_codigoproductoorg,UUID idorganizacion)  {
		
		ProductoxOC producto = new ProductoxOC();
		
		String SQL = "SELECT * FROM producto.t_producto WHERE "	+
					" vc_codigoproductoorg = ? AND in_idorganizacion = ? ";
		List<ProductoxOC> l_producto = null;
		
		try{
			l_producto = jdbcTemplate.query(SQL,new Object[] {vc_codigoproductoorg,idorganizacion}, this::mapParamProducto);
		}catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
	
		if (l_producto != null && l_producto.size()>0)
			producto = l_producto.get(0);
		else return null;

		return producto;
	}
	
	public ProductoxOC mapParamProducto(ResultSet rs, int i) throws SQLException {

		ProductoxOC org = new ProductoxOC();

		org.setIn_idproducto(UUID.fromString((rs.getString("in_idproducto"))));

		org.setIn_idorganizacion(UUID.fromString((rs.getString("in_idorganizacion"))));
	
        return org;
	}
	
	public OrdenCompra buscar_OC(UUID in_idoc )  {
		
		OrdenCompra oc = new OrdenCompra();
		
		String SQL = "SELECT * FROM oc.t_oc  WHERE "	+
					" in_idoc = ? ";
		List<OrdenCompra> l_oc = null;
		
		try{
			l_oc = jdbcTemplate.query(SQL,new Object[] {in_idoc}, this::mapParamOC);
		}catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
	
		if (l_oc != null && l_oc.size()>0)
			oc = l_oc.get(0);
		else return null;

		return oc;
	}
	
	public OrdenCompra buscar_OCxnumseg(UUID in_idorgcompra, String numseg)  {
		
		OrdenCompra oc = new OrdenCompra();
		
		String SQL = "SELECT * FROM oc.t_oc T1 WHERE in_version = (SELECT MAX(T2.IN_VERSION) FROM oc.t_oc T2 WHERE T1.ch_numeroseguimiento = T2.ch_numeroseguimiento) and "	+
					" T1.in_idorganizacioncompradora = ? and T1.ch_numeroseguimiento= ?";
		List<OrdenCompra> l_oc = null;
		
		try{
			l_oc = jdbcTemplate.query(SQL,new Object[] {in_idorgcompra,numseg}, this::mapParamOC);
		}catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
	
		if (l_oc != null && l_oc.size()>0)
			oc = l_oc.get(0);
		else return null;

		return oc;
	}

	public OrdenCompra mapParamOC(ResultSet rs, int i) throws SQLException {

		OrdenCompra oc = new OrdenCompra();
		
		oc.setIn_idusuariocomprador(UUID.fromString(StringUtils.trimToEmpty(rs.getString("in_idusuariocomprador"))));

		oc.setIn_idusuarioproveedor(UUID.fromString(StringUtils.trimToEmpty(rs.getString("in_idusuarioproveedor"))));
		
		oc.setIn_idorgcompra(UUID.fromString(StringUtils.trimToEmpty(rs.getString("in_idorganizacioncompradora"))));
		
		oc.setIn_idorganizacionproveedora(UUID.fromString(StringUtils.trimToEmpty(rs.getString("in_idorganizacionproveedora"))));
		
		oc.setNombre_usuariocreadopor(StringUtils.trimToEmpty(rs.getString("vc_nombreusuariocreadopor")));
	
		oc.setId_usuariocreadopor(UUID.fromString(StringUtils.trimToEmpty(rs.getString("in_idusuariocreadopor"))));
		
        return oc;
	}


}
