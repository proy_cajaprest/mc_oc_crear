package com.ebiz.mscrearoc;

import java.util.List;
import java.util.UUID;

public interface CrearOCRepository {

	List<OrdenCompra> insertOC(CEOrdenCompras cEOrdenCompras, Organiza org, String token, Organiza proveedor, Usuario usuario_prov, UsuarioEquivalencia usuario_comp, UsuarioEquivalencia usuario_equiv, UUID idusuario_auditoria) throws Exception;

	//List<UUID> updateOC(CEOrdenCompras cEOrdenCompras, Organizacion org);
	
	OrdenCompra buscar_OC(UUID in_idoc); 
	
	OrdenCompra buscar_OCxnumseg(UUID in_idorgcompra, String numseg);

	//Organiza buscar_organizacion(String organizationId, String tokenBearer,String RUC, String pais);
	
}
