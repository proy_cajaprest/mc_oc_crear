package com.ebiz.mscrearoc.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ebiz.mscrearoc.ComentarioRepository;
import com.ebiz.mscrearoc.Comentario;

//import org.springframework.jdbc.support.KeyHolder;
//import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class ComentarioRepositoryImpl implements ComentarioRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Transactional(rollbackFor = Exception.class)
	public int guardarcomentariocomprador(Comentario comentario) {

            String sql = "";
            int i = 0;
            try {
            	
            	if (comentario.getVc_num()!=null && comentario.getVc_num()!="") {
            		sql = "UPDATE oc.t_oc T1 set vc_comentariocomprador = ? where in_version = (SELECT MAX(T2.IN_VERSION) FROM oc.t_oc T2 WHERE T1.ch_numeroseguimiento = T2.ch_numeroseguimiento) and T1.ch_numeroseguimiento = ? and T1.in_idorganizacioncompradora = ?"; 
            		i = jdbcTemplate.update(sql, new Object[] { comentario.getVc_comentario(), comentario.getVc_num() ,comentario.getIn_idorg()});
            	}else {
            		sql = "update oc.t_oc set vc_comentariocomprador = ? where in_idoc = ? and in_idorganizacioncompradora = ?";
            		i = jdbcTemplate.update(sql, new Object[] { comentario.getVc_comentario(), comentario.getIn_id() ,comentario.getIn_idorg()});
            	}
            	System.out.println("SQL COMENTARIO COMPRADOR: " + sql);
                System.out.println("FILAS AFECTADAS POR EL UPDATE : " + i);
            }
            catch(EmptyResultDataAccessException e) {
                e.printStackTrace();
                throw e;
            }
            catch(Exception e) {
                e.printStackTrace();
                System.out.println("OCURRIO UN ERROR EN DAO : " + e.getMessage().toString());
                i = 0;
            }
            return i;
    }

	@Transactional(rollbackFor = Exception.class)
	public int guardarcomentarioproveedor(Comentario comentario) {


            String sql = "";
            int i = 0;
            try {
                
            	
            	if (comentario.getVc_num()!=null && comentario.getVc_num()!="") {
            		sql = "UPDATE oc.t_oc T1 set vc_comentarioproveedor = ? where in_version = (SELECT MAX(T2.IN_VERSION) FROM oc.t_oc T2 WHERE T1.ch_numeroseguimiento = T2.ch_numeroseguimiento) and T1.ch_numeroseguimiento = ? and T1.in_idorganizacionproveedora = ?"; 
            		i = jdbcTemplate.update(sql, new Object[] { comentario.getVc_comentario(), comentario.getVc_num() ,comentario.getIn_idorg()});
            	}else {
            		sql = "update oc.t_oc set vc_comentarioproveedor = ? where in_idoc = ? and in_idorganizacionproveedora = ?";
            		i = jdbcTemplate.update(sql, new Object[] { comentario.getVc_comentario(), comentario.getIn_id() ,comentario.getIn_idorg()});
            	}
            	
            	System.out.println("SQL COMENTARIO PROVEEDOR : " + sql);
                System.out.println("FILAS AFECTADAS POR EL UPDATE : " + i);
            }
            catch(EmptyResultDataAccessException e) {
                e.printStackTrace();
                throw e;
            }
            catch(Exception e) {
                e.printStackTrace();
                System.out.println("OCURRIO UN ERROR EN DAO : " + e.getMessage().toString());
                i = 0;
            }
            return i;

	}
}