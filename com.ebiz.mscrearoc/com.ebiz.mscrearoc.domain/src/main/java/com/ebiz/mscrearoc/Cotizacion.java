package com.ebiz.mscrearoc;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)

public class Cotizacion {

    @JsonProperty("NumeroRfq")
    private String numeroRfq;
    @JsonProperty("NombreRfq")
    private String nombreRfq;
    @JsonProperty("NumeroCotizacion")
    private String numeroCotizacion;
    @JsonProperty("VersionCotizacion")
    private Integer versionCotizacion;
	@JsonProperty("Producto")
	private List<ProductoxOC> lista_prodxoc = new ArrayList<ProductoxOC>();

    @JsonProperty("NumeroRfq")
    public String getNumeroRfq() {
        return numeroRfq;
    }

    @JsonProperty("NumeroRfq")
    public void setNumeroRfq(String numeroRfq) {
        this.numeroRfq = numeroRfq;
    }

    @JsonProperty("NombreRfq")
    public String getNombreRfq() {
        return nombreRfq;
    }

    @JsonProperty("NombreRfq")
    public void setNombreRfq(String nombreRfq) {
        this.nombreRfq = nombreRfq;
    }

    @JsonProperty("NumeroCotizacion")
    public String getNumeroCotizacion() {
        return numeroCotizacion;
    }

    @JsonProperty("NumeroCotizacion")
    public void setNumeroCotizacion(String numeroCotizacion) {
        this.numeroCotizacion = numeroCotizacion;
    }

    @JsonProperty("VersionCotizacion")
    public Integer getVersionCotizacion() {
        return versionCotizacion;
    }

    @JsonProperty("VersionCotizacion")
    public void setVersionCotizacion(Integer versionCotizacion) {
        this.versionCotizacion = versionCotizacion;
    }

	public List<ProductoxOC> getLista_prodxoc() {
		return lista_prodxoc;
	}

	public void setLista_prodxoc(List<ProductoxOC> lista_prodxoc) {
		this.lista_prodxoc = lista_prodxoc;
	}
    
    
}
