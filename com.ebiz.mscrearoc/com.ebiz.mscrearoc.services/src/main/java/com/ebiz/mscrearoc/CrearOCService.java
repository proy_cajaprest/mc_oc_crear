package com.ebiz.mscrearoc;

import java.util.List;
import java.util.UUID;

public interface CrearOCService {
	
	List<OrdenCompra> insertOC(CEOrdenCompras cEOrdenCompras,Organiza org, String token, Organiza proveedor, Usuario usuario, UsuarioEquivalencia usu_compra, UsuarioEquivalencia usuario_creador, UUID idusuario_auditoria) throws Exception;
	
	String verificacionTotal(CEOrdenCompras cEOrdenCompras,String origen_datos);

	//List<UUID> updateOC(CEOrdenCompras cEOrdenCompras, Organizacion org);
	
	OrdenCompra buscar_OC(UUID in_idoc); 

	OrdenCompra buscar_OCxnumseg(UUID in_idorgcompra, String numseg); //nuevo xc

	//Organiza buscar_organizacion(String organizationId, String tokenBearer,String RUC,String pais);
	
}
