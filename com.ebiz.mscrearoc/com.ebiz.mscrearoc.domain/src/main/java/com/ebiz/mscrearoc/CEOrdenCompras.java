package com.ebiz.mscrearoc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CEOrdenCompras {

	/*@JsonProperty("lista_oc")
	private List<OrdenCompra> lista_oc = new ArrayList<OrdenCompra>();*/
	
	@JsonProperty("POUPLOADMQ")
	private POUPLOADMQ pouploadmq;
	
	/*@JsonProperty("cuadro_evaluacion")
	private CuadroEvaluacion cuadroEvaluacion;*/
	
	@JsonProperty("origen_datos")
	private String origen_datos;
	
	////////////////////////////////////////////
	
	public String getOrigen_datos() {
		return origen_datos;
	}

	public void setOrigen_datos(String origen_datos) {
		this.origen_datos = origen_datos;
	}
	
	/*public CuadroEvaluacion getCuadroEvaluacion() {
		return cuadroEvaluacion;
	}

	public void setCuadroEvaluacion(CuadroEvaluacion cuadroEvaluacion) {
		this.cuadroEvaluacion = cuadroEvaluacion;
	}*/

	public POUPLOADMQ getPouploadmq() {
		return pouploadmq;
	}

	public void setPouploadmq(POUPLOADMQ pouploadmq) {
		this.pouploadmq = pouploadmq;
	}
	
	
	
}
