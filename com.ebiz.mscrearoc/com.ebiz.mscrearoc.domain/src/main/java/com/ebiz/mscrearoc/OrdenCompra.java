package com.ebiz.mscrearoc;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class OrdenCompra {
	
	@JsonProperty("AdjuntoOrden")
	private List<ArchivoxOC> lista_archivoxoc = new ArrayList<ArchivoxOC>();
	
	@JsonProperty("Cotizacion")
	private Cotizacion cotizacion;

	@JsonProperty("lista_atributoxoc")
	private List<AtributoxOC> lista_atributoxoc = new ArrayList<AtributoxOC>();
	
	/*@JsonProperty("lista_prodxoc")
	private List<ProductoxOC> lista_prodxoc = new ArrayList<ProductoxOC>();*/
	
	private UUID in_idoc ;
	
	private UUID in_idorganizacionproveedora;
	
	private UUID in_idorgcompra;
	
	@JsonProperty("Contactos")
	private Contacto contactos;//Contacto
	
	//@JsonProperty("Contacto")
	private String contacto;//Contacto
	
	@JsonProperty("EnviarComprobanteA")
	private String vc_enviarfacturaa;//Contacto
	
	@JsonProperty("Expeditor")
	private String vc_forwarder;//vc_forwarder
	
	@JsonProperty("OperadorLogistico")
	private  OperadorLogistico operadorLogistico;//OperadorLogistico
	
	@JsonProperty("ServicioCorreo")
	private  ServicioCorreo servicioCorreo;//ServicioCorreo
	
	private UUID in_idusuariocomprador;//CompradorUsuarioID
	
	@JsonProperty("IDVendedor")
	private UUID in_idusuarioproveedor;//IDVendedor
	
	//@JsonProperty("vc_nombreusuariocomprador")
	@JsonProperty("CreadoPor")
	private String vc_creadopor;//Usuario
	
	//@JsonProperty("vc_numeroseguimiento")
	@JsonProperty("NumeroOrden")
	private String vc_numeroseguimiento;//NumeroOrden
	
	@JsonProperty("Version")
	private int in_version;//Version
	
	//@JsonProperty("vc_prioridad")
	@JsonProperty("PrioridadOrden")
	private String vc_prioridad;//PrioridadOrden
	
	//@JsonProperty("vc_terminosentrega")
	@JsonProperty("TerminosEntrega")
	private String vc_terminosentrega;//TerminosEntrega
	
	//@JsonProperty("vc_consignatario")
	@JsonProperty("ConsignarA")
	private String vc_consignatario;//ConsignarA
	
	//@JsonProperty("ts_fechaenvio")
	@JsonProperty("FechaEnvio")
	private String ts_fechaenvio;//FechaEnvio
	
	@JsonProperty("FechaEntrega")
	private String ts_fechaentrega;//FechaEntrega
	
	//@JsonProperty("vc_paisembarque")
	@JsonProperty("PaisEmbarque")
	private String vc_paisembarque;//PaisEmbarque
	
	@JsonProperty("PaisVendedor")
	private String vc_paisvendedor;//PaisVendedor
	
	//@JsonProperty("vc_regionembarque")
	@JsonProperty("RegionEmbarque")
	private String vc_regionembarque;//RegionEmbarque
	
	@JsonProperty("NumeroCotizacionProveedor")
	private String vc_numqtprov;//NumeroCotizacionProveedor
	
	@JsonProperty("CondicionesEnvio")
	private String vc_condicionembarque;//CondicionesEnvio
	
	@JsonProperty("CondicionesGenerales")
	private String condicionesGenerales;//CondicionesGenerales
	
	@JsonProperty("Personalizado")
	private String personalizado;//Personalizado
	
	@JsonProperty("FechaIniContrato")
	private String ts_fechainicontrato;//FechaIniContrato
	
	@JsonProperty("FechaFinContrato")
	private String ts_fechafincontrato;//FechaFinContrato
	
	@JsonProperty("GrupoCompra")
	private String vc_grupocompra;//GrupoCompra
	
	@JsonProperty("OtroImpuesto")
	private Integer in_otrosimpuestos;//OtroImpuesto
	
	@JsonProperty("Terminos")
	private String vc_terminos;//Terminos
	
	@JsonProperty("Embarcador")
	private String vc_embarcador;//falta
	
	@JsonProperty("CobrarA")
	private String vc_cobrara;//falta
	
	@JsonProperty("PrecioCotizacion")
	private String vc_precioqt;//PrecioCotizacion
	
	@JsonProperty("IndicadorCambioOrden")
	private Integer in_flagmodoc;//IndicadorCambioOrden
	
	@JsonProperty("FechaCambioOrden")
	private String ts_fechamodoc;//FechaCambioOrden
	
	@JsonProperty("ISC")
	private BigDecimal fl_isc;//ISC
	
	@JsonProperty("EmbarqueParcial")
	private Integer in_embarqueparcial;//EnviosParciales
	
	//@JsonProperty("vc_puertodesembarque")
	@JsonProperty("PuertoDesembarque")
	private String vc_puertodesembarque;//PuertoDesembarque
	
	//@JsonProperty("vc_polizaseguro")
	@JsonProperty("Seguro")
	private String vc_polizaseguro;//Seguro
	
	@JsonProperty("Aduana")
	private String vc_aduana;//
	
	@JsonProperty("TipoPrecio")
	private String vc_tipoprecio;//TipoPrecio
		
	@JsonProperty("AgenteInspeccion")
	private String vc_companiainspectora;//AgenteInspeccion
	
	//@JsonProperty("vc_numeroinspeccion")
	@JsonProperty("NumeroInspeccion")
	private String vc_numeroinspeccion;
	
	@JsonProperty("AprobadoPor")
	private String vc_autorizadopor;//AprobadoPor
	
	@JsonProperty("Cargo")
	private String vc_cargo;//
	
	//@JsonProperty("ValorTotal")
	//private BigDecimal fl_valortotal;//
	
	//@JsonProperty("fl_otroscostos")
	@JsonProperty("OtrosCostos")
	private BigDecimal fl_otroscostos = null;//OtrosCostos
	
	//@JsonProperty("fl_valorventa")
	/*@JsonProperty("ValorVentaNeto")
	private Double fl_valorventaNeto;//ValorVentaNeto*/
	
	@JsonProperty("ValorVenta")
	private BigDecimal fl_valorventa;//ValorVenta
	
	@JsonProperty("ValorVentaNeto")
	private BigDecimal fl_subtotal;
	
	//@JsonProperty("ts_fechaautorizacion")
	@JsonProperty("FechaAutorizacion")
	private String ts_fechaautorizacion;//FechaAutorizacion
	
	//@JsonProperty("vc_moneda")
	@JsonProperty("MonedaOrden")
	private String vc_moneda;//MonedaOrden

	/*@JsonProperty("vc_codigoalmacenenvio")
	private String vc_codigoalmacenenvio;/*/
	
	@JsonProperty("CodigoAlmacenEntrega")
	private String vc_codigoalmacenenvio;//CodigoAlmacenEntrega
	
	@JsonProperty("Narrativa")
	private String vc_narrativa;
	
	@JsonProperty("Lab")
	private String vc_lab;
	
	@JsonProperty("UnidadCondicionesPago")
	private String vc_unidadcondpago;//VC_UNIDADCONDPAGO
	
	//@JsonProperty("in_codigousuariocreacion")
	//@JsonProperty("CreadoPor")
	private String in_codigousuariocreacion;//CreadoPor
	
	@JsonProperty("Fecha")
	private String ts_fechacreacion;//Fecha
	
	/*@JsonProperty("Msgmail")
	private Timestamp vc_msgmail;//Msgmail*/
	
	//@JsonProperty("vc_tipotransporte")
	@JsonProperty("TipoTransporte")
	private String vc_tipotransporte;//TipoTransporte
	
	//@JsonProperty("vc_condicionpago")
	/*@JsonProperty("ValorCondicionesPago")
	private String vc_condicionpago;//ValorCondicionesPago*/
	
	//@JsonProperty("vc_condicionpago")
	@JsonProperty("CondicionesPago")
	private String vc_condicionpago;//ValorCondicionesPago
	//private String vc_condicionespago;//CondicionesPago
	
	//@JsonProperty("in_idorganizacioncompradora")
	@JsonProperty("CompradorOrgID")
	private String in_idorganizacioncompradora;//CompradorOrgID*/
	
	//@JsonProperty("in_idorganizacionproveedora")

	//private UUID in_idorganizacionproveedora;//OrgIDVendedor
	@JsonProperty("OrgIDVendedor")
	private String vc_codinternoprov;//OrgIDVendedor
	
	//@JsonProperty("fl_impuestos")
	@JsonProperty("Impuestos")
	private BigDecimal fl_impuestos;//Impuestos
	
	//@JsonProperty("vc_direccionfactura")
	@JsonProperty("DireccionFactura")
	private String vc_direccionfactura;//EmitirA
	
	//@JsonProperty("vc_numeroqt")
	/*@JsonProperty("NumeroCotizacion")
	private String vc_numeroqt;//NumeroCotizacion*/
	
	//@JsonProperty("vc_facturara")
	@JsonProperty("EmitirA")
	private String vc_facturara;//EmitirA
	
	/*@JsonProperty("VersionCotizacion")
	private String in_versionqt;//VersionCotizacion*/
	
	//@JsonProperty("vc_rucproveedor")
	@JsonProperty("NITVendedor")
	private String vc_rucproveedor;//NITVendedor
	
	//@JsonProperty("vc_razonsocialproveedor")
	@JsonProperty("NombreVendedor")
	private String vc_razonsocialproveedor;//NombreVendedor
	
	//@JsonProperty("vc_atenciona")
	@JsonProperty("AtencionA")
	private String vc_atenciona;//AtencionA
	
	//@JsonProperty("EmailContacto")
	@JsonProperty("Contacto")
	private String vc_emailcontacto;//
	
	@JsonProperty("vc_comentariocomprador")
	private String vc_comentariocomprador;//no hay
	
	//@JsonProperty("fl_descuentoporcentaje")
	@JsonProperty("Descuentos")
	private BigDecimal fl_descuentoporcentaje;//Descuentos confirmado
	
	//@JsonProperty("fl_impuestosporcentaje")
	@JsonProperty("PorcentajeImpuestos")
	private BigDecimal fl_impuestosporcentaje;//PorcentajeImpuestos no hay porcentaje
	
	@JsonProperty("Logo")
	private String vc_logo;//no hay
	
	@JsonProperty("MontoAPagar")
	private BigDecimal fl_valortotal;//
	//private String vc_pagaraproveedor;//no hay verificar con Raul 27/10 
	
	@JsonProperty("Firma")
	private String vc_firma;//no hay
	
	@JsonProperty("FlagOcDirecta")
	private int in_flagocdirecta;//no hay
	
	//@JsonProperty("in_requiereinspeccion")
	@JsonProperty("InspeccionRequerida")
	private int in_requiereinspeccion;//InspeccionRequerida
	
	//@JsonProperty("vc_tipooc")
	@JsonProperty("TipoOrden")
	private String vc_tipooc;//TipoOrden
	
	//@JsonProperty("vc_tipooc")
	@JsonProperty("Observacion")
	private String observacion;//Observacion
	
	//@JsonProperty("vc_numerorfq")
	/*@JsonProperty("NumeroRfq")
	private String vc_numerorfq;//Cotizacion.NumeroRfq*/
	
	@JsonProperty("NumeroAP")
	private String vc_numeroap;//Cotizacion.NumeroRfq
	
	//@JsonProperty("tipocambio")
	@JsonProperty("TipoCambio")
	private BigDecimal tipocambio;//TipoCambio
	
	/*@JsonProperty("NombreRfq")
	private Double vc_nombrerfq;//NombreRfq*/
	
	@JsonProperty("EstadoComprador")
	private String vc_estadocomprador;//no hay
	
	@JsonProperty("EstadoProveedor")
	private String vc_estadoproveedor;//"ONVIS";//no hay
	
	@JsonProperty("ts_fechanotificacion")
	private String ts_fechanotificacion;//no hay
	
	@JsonProperty("ts_fechacompromisoppto")
	private String ts_fechacompromisoppto;//no hay
	
	@JsonProperty("vc_numeroexpedientesiaf")
	private String vc_numeroexpedientesiaf;//no hay
	
	@JsonProperty("vc_tipocontratacion")
	private String vc_tipocontratacion;//no hay
	
	@JsonProperty("vc_objetocontratacion")
	private String vc_objetocontratacion;//no hay
	
	@JsonProperty("vc_descripcioncontratacion")
	private String vc_descripcioncontratacion;//no hay
	
	@JsonProperty("vc_unidadessolicitantes")
	private String vc_unidadessolicitantes;//no hay
	
	@JsonProperty("vc_unidadesinformantes")
	private String vc_unidadesinformantes;//no hay
	
	@JsonProperty("TiempoRecordatorio")
	private Integer in_tiemporecordatorio;//no hay
	
	@JsonProperty("PrecioFOB")
	private BigDecimal fl_preciofob;//no hay
	
	@JsonProperty("FlagOrigen")
	private Integer in_flagorigen;//no hay
	
	@JsonProperty("TasaCambio")
	private BigDecimal fl_tasacambio;//no hay
	
	@JsonProperty("IdOrgPropietaria")
	private Integer in_idorgpropietaria;//no hay
	
	@JsonProperty("Iva")
	private BigDecimal de_iva;//no hay
	
	@JsonProperty("Utilidades")
	private BigDecimal de_utilidades;//no hay
	
	@JsonProperty("GastosGen")
	private BigDecimal de_gastosgen;//no hay
	
	private String nombre_usuariocreadopor;
	private UUID id_usuariocreadopor;
		
	/////////////////////////////////////////////////
	
	public List<ArchivoxOC> getLista_archivoxoc() {
		return lista_archivoxoc;
	}

	public String getVc_estadocomprador() {
		return vc_estadocomprador;
	}

	public void setVc_estadocomprador(String vc_estadocomprador) {
		this.vc_estadocomprador = vc_estadocomprador;
	}

	public String getVc_estadoproveedor() {
		return vc_estadoproveedor;
	}

	public void setVc_estadoproveedor(String vc_estadoproveedor) {
		this.vc_estadoproveedor = vc_estadoproveedor;
	}

	public void setLista_archivoxoc(List<ArchivoxOC> lista_archivoxoc) {
		this.lista_archivoxoc = lista_archivoxoc;
	}
	
	public List<AtributoxOC> getLista_atributoxoc() {
		return lista_atributoxoc;
	}

	public void setLista_atributoxoc(List<AtributoxOC> lista_atributoxoc) {
		this.lista_atributoxoc = lista_atributoxoc;
	}

	public UUID getIn_idusuariocomprador() {
		return in_idusuariocomprador;
	}

	public void setIn_idusuariocomprador(UUID in_idusuariocomprador) {
		this.in_idusuariocomprador = in_idusuariocomprador;
	}

	public String getVc_creadopor() {
		return vc_creadopor;
	}

	public void setVc_creadopor(String vc_creadopor) {
		this.vc_creadopor = vc_creadopor;
	}

	public String getVc_numeroseguimiento() {
		return vc_numeroseguimiento;
	}

	public void setVc_numeroseguimiento(String vc_numeroseguimiento) {
		this.vc_numeroseguimiento = vc_numeroseguimiento;
	}

	public int getIn_version() {
		return in_version;
	}

	public void setIn_version(int in_version) {
		this.in_version = in_version;
	}

	public String getVc_prioridad() {
		return vc_prioridad;
	}

	public void setVc_prioridad(String vc_prioridad) {
		this.vc_prioridad = vc_prioridad;
	}

	public String getVc_terminosentrega() {
		return vc_terminosentrega;
	}

	public void setVc_terminosentrega(String vc_terminosentrega) {
		this.vc_terminosentrega = vc_terminosentrega;
	}

	public String getVc_consignatario() {
		return vc_consignatario;
	}

	public void setVc_consignatario(String vc_consignatario) {
		this.vc_consignatario = vc_consignatario;
	}

	public String getTs_fechaenvio() {
		return ts_fechaenvio;
	}

	public void setTs_fechaenvio(String ts_fechaenvio) {
		this.ts_fechaenvio = ts_fechaenvio;
	}

	public String getVc_paisembarque() {
		return vc_paisembarque;
	}

	public void setVc_paisembarque(String vc_paisembarque) {
		this.vc_paisembarque = vc_paisembarque;
	}

	public String getVc_regionembarque() {
		return vc_regionembarque;
	}

	public void setVc_regionembarque(String vc_regionembarque) {
		this.vc_regionembarque = vc_regionembarque;
	}

	public String getVc_condicionembarque() {
		return vc_condicionembarque;
	}

	public void setVc_condicionembarque(String vc_condicionembarque) {
		this.vc_condicionembarque = vc_condicionembarque;
	}

	public String getVc_embarcador() {
		return vc_embarcador;
	}

	public void setVc_embarcador(String vc_embarcador) {
		this.vc_embarcador = vc_embarcador;
	}

	public Integer getIn_embarqueparcial() {
		return in_embarqueparcial;
	}

	public void setIn_embarqueparcial(Integer in_embarqueparcial) {
		this.in_embarqueparcial = in_embarqueparcial;
	}

	public String getVc_puertodesembarque() {
		return vc_puertodesembarque;
	}

	public void setVc_puertodesembarque(String vc_puertodesembarque) {
		this.vc_puertodesembarque = vc_puertodesembarque;
	}

	public String getVc_polizaseguro() {
		return vc_polizaseguro;
	}

	public void setVc_polizaseguro(String vc_polizaseguro) {
		this.vc_polizaseguro = vc_polizaseguro;
	}

	public String getVc_aduana() {
		return vc_aduana;
	}

	public void setVc_aduana(String vc_aduana) {
		this.vc_aduana = vc_aduana;
	}

	public String getVc_companiainspectora() {
		return vc_companiainspectora;
	}

	public void setVc_companiainspectora(String vc_companiainspectora) {
		this.vc_companiainspectora = vc_companiainspectora;
	}

	public String getVc_numeroinspeccion() {
		return vc_numeroinspeccion;
	}

	public void setVc_numeroinspeccion(String vc_numeroinspeccion) {
		this.vc_numeroinspeccion = vc_numeroinspeccion;
	}

	public String getVc_autorizadopor() {
		return vc_autorizadopor;
	}

	public void setVc_autorizadopor(String vc_autorizadopor) {
		this.vc_autorizadopor = vc_autorizadopor;
	}

	public String getVc_cargo() {
		return vc_cargo;
	}

	public void setVc_cargo(String vc_cargo) {
		this.vc_cargo = vc_cargo;
	}

	public BigDecimal getFl_valortotal() {
		return fl_valortotal;
	}

	public void setFl_valortotal(BigDecimal fl_valortotal) {
		this.fl_valortotal = fl_valortotal;
	}

	public BigDecimal getFl_otroscostos() {
		return fl_otroscostos;
	}

	public void setFl_otroscostos(BigDecimal fl_otroscostos) {
		this.fl_otroscostos = fl_otroscostos;
	}

	public BigDecimal getFl_valorventa() {
		return fl_valorventa;
	}

	public void setFl_valorventa(BigDecimal fl_valorventa) {
		this.fl_valorventa = fl_valorventa;
	}

	public BigDecimal getFl_subtotal() {
		return fl_subtotal;
	}

	public void setFl_subtotal(BigDecimal fl_subtotal) {
		this.fl_subtotal = fl_subtotal;
	}

	public String getTs_fechaautorizacion() {
		return ts_fechaautorizacion;
	}

	public void setTs_fechaautorizacion(String ts_fechaautorizacion) {
		this.ts_fechaautorizacion = ts_fechaautorizacion;
	}

	public String getVc_moneda() {
		return vc_moneda;
	}

	public void setVc_moneda(String vc_moneda) {
		this.vc_moneda = vc_moneda;
	}

	public String getVc_codigoalmacenenvio() {
		return vc_codigoalmacenenvio;
	}

	public void setVc_codigoalmacenenvio(String vc_codigoalmacenenvio) {
		this.vc_codigoalmacenenvio = vc_codigoalmacenenvio;
	}

	public String getVc_lab() {
		return vc_lab;
	}

	public void setVc_lab(String vc_lab) {
		this.vc_lab = vc_lab;
	}

	public String getIn_codigousuariocreacion() {
		return in_codigousuariocreacion;
	}

	public void setIn_codigousuariocreacion(String in_codigousuariocreacion) {
		this.in_codigousuariocreacion = in_codigousuariocreacion;
	}

	public String getTs_fechacreacion() {
		return ts_fechacreacion;
	}

	public void setTs_fechacreacion(String ts_fechacreacion) {
		this.ts_fechacreacion = ts_fechacreacion;
	}

	public String getVc_tipotransporte() {
		return vc_tipotransporte;
	}

	public void setVc_tipotransporte(String vc_tipotransporte) {
		this.vc_tipotransporte = vc_tipotransporte;
	}

	public String getVc_condicionpago() {
		return vc_condicionpago;
	}

	public void setVc_condicionpago(String vc_condicionpago) {
		this.vc_condicionpago = vc_condicionpago;
	}

	/*public UUID getIn_idorganizacionproveedora() {
		return in_idorganizacionproveedora;
	}

	public void setIn_idorganizacionproveedora(UUID in_idorganizacionproveedora) {
		this.in_idorganizacionproveedora = in_idorganizacionproveedora;
	}*/

	public BigDecimal getFl_impuestos() {
		return fl_impuestos;
	}

	public void setFl_impuestos(BigDecimal fl_impuestos) {
		this.fl_impuestos = fl_impuestos;
	}

	public String getVc_direccionfactura() {
		return vc_direccionfactura;
	}

	public void setVc_direccionfactura(String vc_direccionfactura) {
		this.vc_direccionfactura = vc_direccionfactura;
	}

	public String getVc_facturara() {
		return vc_facturara;
	}

	public void setVc_facturara(String vc_facturara) {
		this.vc_facturara = vc_facturara;
	}

	public String getVc_rucproveedor() {
		return vc_rucproveedor;
	}

	public void setVc_rucproveedor(String vc_rucproveedor) {
		this.vc_rucproveedor = vc_rucproveedor;
	}

	public String getVc_razonsocialproveedor() {
		return vc_razonsocialproveedor;
	}

	public void setVc_razonsocialproveedor(String vc_razonsocialproveedor) {
		this.vc_razonsocialproveedor = vc_razonsocialproveedor;
	}

	public String getVc_atenciona() {
		return vc_atenciona;
	}

	public void setVc_atenciona(String vc_atenciona) {
		this.vc_atenciona = vc_atenciona;
	}

	public String getVc_comentariocomprador() {
		return vc_comentariocomprador;
	}

	public void setVc_comentariocomprador(String vc_comentariocomprador) {
		this.vc_comentariocomprador = vc_comentariocomprador;
	}

	public BigDecimal getFl_descuentoporcentaje() {
		return fl_descuentoporcentaje;
	}

	public void setFl_descuentoporcentaje(BigDecimal fl_descuentoporcentaje) {
		this.fl_descuentoporcentaje = fl_descuentoporcentaje;
	}

	public BigDecimal getFl_impuestosporcentaje() {
		return fl_impuestosporcentaje;
	}

	public void setFl_impuestosporcentaje(BigDecimal fl_impuestosporcentaje) {
		this.fl_impuestosporcentaje = fl_impuestosporcentaje;
	}

	public String getVc_logo() {
		return vc_logo;
	}

	public void setVc_logo(String vc_logo) {
		this.vc_logo = vc_logo;
	}

	public String getVc_firma() {
		return vc_firma;
	}

	public void setVc_firma(String vc_firma) {
		this.vc_firma = vc_firma;
	}

	public int getIn_flagocdirecta() {
		return in_flagocdirecta;
	}

	public void setIn_flagocdirecta(int in_flagocdirecta) {
		this.in_flagocdirecta = in_flagocdirecta;
	}

	public int getIn_requiereinspeccion() {
		return in_requiereinspeccion;
	}

	public void setIn_requiereinspeccion(int in_requiereinspeccion) {
		this.in_requiereinspeccion = in_requiereinspeccion;
	}

	public String getVc_tipooc() {
		return vc_tipooc;
	}

	public void setVc_tipooc(String vc_tipooc) {
		this.vc_tipooc = vc_tipooc;
	}

	public BigDecimal getTipocambio() {
		return tipocambio;
	}

	public void setTipocambio(BigDecimal tipocambio) {
		this.tipocambio = tipocambio;
	}

	public String getTs_fechanotificacion() {
		return ts_fechanotificacion;
	}

	public void setTs_fechanotificacion(String ts_fechanotificacion) {
		this.ts_fechanotificacion = ts_fechanotificacion;
	}

	public String getTs_fechacompromisoppto() {
		return ts_fechacompromisoppto;
	}

	public void setTs_fechacompromisoppto(String ts_fechacompromisoppto) {
		this.ts_fechacompromisoppto = ts_fechacompromisoppto;
	}

	public String getVc_numeroexpedientesiaf() {
		return vc_numeroexpedientesiaf;
	}

	public void setVc_numeroexpedientesiaf(String vc_numeroexpedientesiaf) {
		this.vc_numeroexpedientesiaf = vc_numeroexpedientesiaf;
	}

	public String getVc_tipocontratacion() {
		return vc_tipocontratacion;
	}

	public void setVc_tipocontratacion(String vc_tipocontratacion) {
		this.vc_tipocontratacion = vc_tipocontratacion;
	}

	public String getVc_objetocontratacion() {
		return vc_objetocontratacion;
	}

	public void setVc_objetocontratacion(String vc_objetocontratacion) {
		this.vc_objetocontratacion = vc_objetocontratacion;
	}

	public String getVc_descripcioncontratacion() {
		return vc_descripcioncontratacion;
	}

	public void setVc_descripcioncontratacion(String vc_descripcioncontratacion) {
		this.vc_descripcioncontratacion = vc_descripcioncontratacion;
	}

	public String getVc_unidadessolicitantes() {
		return vc_unidadessolicitantes;
	}

	public void setVc_unidadessolicitantes(String vc_unidadessolicitantes) {
		this.vc_unidadessolicitantes = vc_unidadessolicitantes;
	}

	public String getVc_unidadesinformantes() {
		return vc_unidadesinformantes;
	}

	public void setVc_unidadesinformantes(String vc_unidadesinformantes) {
		this.vc_unidadesinformantes = vc_unidadesinformantes;
	}

	public Cotizacion getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(Cotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}

	public UUID getIn_idusuarioproveedor() {
		return in_idusuarioproveedor;
	}

	public void setIn_idusuarioproveedor(UUID in_idusuarioproveedor) {
		this.in_idusuarioproveedor = in_idusuarioproveedor;
	}

	public String getTs_fechaentrega() {
		return ts_fechaentrega;
	}

	public void setTs_fechaentrega(String ts_fechaentrega) {
		this.ts_fechaentrega = ts_fechaentrega;
	}

	public String getVc_numqtprov() {
		return vc_numqtprov;
	}

	public void setVc_numqtprov(String vc_numqtprov) {
		this.vc_numqtprov = vc_numqtprov;
	}

	public String getTs_fechainicontrato() {
		return ts_fechainicontrato;
	}

	public void setTs_fechainicontrato(String ts_fechainicontrato) {
		this.ts_fechainicontrato = ts_fechainicontrato;
	}

	public String getTs_fechafincontrato() {
		return ts_fechafincontrato;
	}

	public void setTs_fechafincontrato(String ts_fechafincontrato) {
		this.ts_fechafincontrato = ts_fechafincontrato;
	}

	public String getVc_grupocompra() {
		return vc_grupocompra;
	}

	public void setVc_grupocompra(String vc_grupocompra) {
		this.vc_grupocompra = vc_grupocompra;
	}

	public String getVc_forwarder() {
		return vc_forwarder;
	}

	public void setVc_forwarder(String vc_forwarder) {
		this.vc_forwarder = vc_forwarder;
	}

	public Integer getIn_otrosimpuestos() {
		return in_otrosimpuestos;
	}

	public void setIn_otrosimpuestos(Integer in_otrosimpuestos) {
		this.in_otrosimpuestos = in_otrosimpuestos;
	}

	public String getVc_terminos() {
		return vc_terminos;
	}

	public void setVc_terminos(String vc_terminos) {
		this.vc_terminos = vc_terminos;
	}

	public String getVc_precioqt() {
		return vc_precioqt;
	}

	public void setVc_precioqt(String vc_precioqt) {
		this.vc_precioqt = vc_precioqt;
	}

	public Integer getIn_flagmodoc() {
		return in_flagmodoc;
	}

	public void setIn_flagmodoc(Integer in_flagmodoc) {
		this.in_flagmodoc = in_flagmodoc;
	}

	public String getTs_fechamodoc() {
		return ts_fechamodoc;
	}

	public void setTs_fechamodoc(String ts_fechamodoc) {
		this.ts_fechamodoc = ts_fechamodoc;
	}

	public BigDecimal getFl_isc() {
		return fl_isc;
	}

	public void setFl_isc(BigDecimal fl_isc) {
		this.fl_isc = fl_isc;
	}

	public String getVc_tipoprecio() {
		return vc_tipoprecio;
	}

	public void setVc_tipoprecio(String vc_tipoprecio) {
		this.vc_tipoprecio = vc_tipoprecio;
	}

	public String getVc_narrativa() {
		return vc_narrativa;
	}

	public void setVc_narrativa(String vc_narrativa) {
		this.vc_narrativa = vc_narrativa;
	}

	public String getVc_enviarfacturaa() {
		return vc_enviarfacturaa;
	}

	public void setVc_enviarfacturaa(String vc_enviarfacturaa) {
		this.vc_enviarfacturaa = vc_enviarfacturaa;
	}


	/*public String getVc_pagaraproveedor() {
		return vc_pagaraproveedor;
	}

	public void setVc_pagaraproveedor(String vc_pagaraproveedor) {
		this.vc_pagaraproveedor = vc_pagaraproveedor;
	}*/

	public String getVc_numeroap() {
		return vc_numeroap;
	}

	public void setVc_numeroap(String vc_numeroap) {
		this.vc_numeroap = vc_numeroap;
	}

	public Contacto getContactos() {
		return contactos;
	}

	public void setContactos(Contacto contactos) {
		this.contactos = contactos;
	}

	public ServicioCorreo getServicioCorreo() {
		return servicioCorreo;
	}

	public void setServicioCorreo(ServicioCorreo servicioCorreo) {
		this.servicioCorreo = servicioCorreo;
	}

	public OperadorLogistico getOperadorLogistico() {
		return operadorLogistico;
	}

	public void setOperadorLogistico(OperadorLogistico operadorLogistico) {
		this.operadorLogistico = operadorLogistico;
	}

	public String getVc_paisvendedor() {
		return vc_paisvendedor;
	}

	public void setVc_paisvendedor(String vc_paisvendedor) {
		this.vc_paisvendedor = vc_paisvendedor;
	}

	public String getVc_cobrara() {
		return vc_cobrara;
	}

	public void setVc_cobrara(String vc_cobrara) {
		this.vc_cobrara = vc_cobrara;
	}

	public String getCondicionesGenerales() {
		return condicionesGenerales;
	}

	public void setCondicionesGenerales(String condicionesGenerales) {
		this.condicionesGenerales = condicionesGenerales;
	}

	public String getPersonalizado() {
		return personalizado;
	}

	public void setPersonalizado(String personalizado) {
		this.personalizado = personalizado;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getVc_unidadcondpago() {
		return vc_unidadcondpago;
	}

	public void setVc_unidadcondpago(String vc_unidadcondpago) {
		this.vc_unidadcondpago = vc_unidadcondpago;
	}

	public Integer getIn_tiemporecordatorio() {
		return in_tiemporecordatorio;
	}

	public void setIn_tiemporecordatorio(Integer in_tiemporecordatorio) {
		this.in_tiemporecordatorio = in_tiemporecordatorio;
	}

	public BigDecimal getFl_preciofob() {
		return fl_preciofob;
	}

	public void setFl_preciofob(BigDecimal fl_preciofob) {
		this.fl_preciofob = fl_preciofob;
	}

	public Integer getIn_flagorigen() {
		return in_flagorigen;
	}

	public void setIn_flagorigen(Integer in_flagorigen) {
		this.in_flagorigen = in_flagorigen;
	}

	public BigDecimal getFl_tasacambio() {
		return fl_tasacambio;
	}

	public void setFl_tasacambio(BigDecimal fl_tasacambio) {
		this.fl_tasacambio = fl_tasacambio;
	}

	public String getVc_emailcontacto() {
		return vc_emailcontacto;
	}

	public void setVc_emailcontacto(String vc_emailcontacto) {
		this.vc_emailcontacto = vc_emailcontacto;
	}

	public Integer getIn_idorgpropietaria() {
		return in_idorgpropietaria;
	}

	public void setIn_idorgpropietaria(Integer in_idorgpropietaria) {
		this.in_idorgpropietaria = in_idorgpropietaria;
	}

	public BigDecimal getDe_iva() {
		return de_iva;
	}

	public void setDe_iva(BigDecimal de_iva) {
		this.de_iva = de_iva;
	}

	public BigDecimal getDe_utilidades() {
		return de_utilidades;
	}

	public void setDe_utilidades(BigDecimal de_utilidades) {
		this.de_utilidades = de_utilidades;
	}

	public BigDecimal getDe_gastosgen() {
		return de_gastosgen;
	}

	public void setDe_gastosgen(BigDecimal de_gastosgen) {
		this.de_gastosgen = de_gastosgen;
	}

	public UUID getIn_idoc() {
		return in_idoc;
	}

	public void setIn_idoc(UUID in_idoc) {
		this.in_idoc = in_idoc;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public String getContacto() {
		return contacto;
	}

	public String getVc_codinternoprov() {
		return vc_codinternoprov;
	}

	public void setVc_codinternoprov(String vc_codinternoprov) {
		this.vc_codinternoprov = vc_codinternoprov;
	}

	public UUID getIn_idorganizacionproveedora() {
		return in_idorganizacionproveedora;
	}

	public void setIn_idorganizacionproveedora(UUID in_idorganizacionproveedora) {
		this.in_idorganizacionproveedora = in_idorganizacionproveedora;
	}

	public UUID getIn_idorgcompra() {
		return in_idorgcompra;
	}

	public void setIn_idorgcompra(UUID in_idorgcompra) {
		this.in_idorgcompra = in_idorgcompra;
	}

	public String getNombre_usuariocreadopor() {
		return nombre_usuariocreadopor;
	}

	public void setNombre_usuariocreadopor(String nombre_usuariocreadopor) {
		this.nombre_usuariocreadopor = nombre_usuariocreadopor;
	}

	public UUID getId_usuariocreadopor() {
		return id_usuariocreadopor;
	}

	public void setId_usuariocreadopor(UUID id_usuariocreadopor) {
		this.id_usuariocreadopor = id_usuariocreadopor;
	}

	/*public String getOrganizacion_proveedora() {
		return organizacion_proveedora;
	}

	public void setOrganizacion_proveedora(String organizacion_proveedora) {
		this.organizacion_proveedora = organizacion_proveedora;
	}
	*/
	
	
	
	///////////////////////////////////
	
}
