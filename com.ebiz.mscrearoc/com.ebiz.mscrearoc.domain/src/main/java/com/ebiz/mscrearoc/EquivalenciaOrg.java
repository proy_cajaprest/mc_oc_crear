package com.ebiz.mscrearoc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EquivalenciaOrg {
	@JsonProperty("CodigoInterno")
    private String CodigoInterno;
    
    @JsonProperty("Habilitado")
    private int Habilitado;
    
    @JsonProperty("IdOrganizacionOrigen")
    private String IdOrganizacionOrigen;
    
    @JsonProperty("IdOrganizacionDestino")
    private String IdOrganizacionDestino;

	public String getCodigoInterno() {
		return CodigoInterno;
	}

	public void setCodigoInterno(String codigoInterno) {
		CodigoInterno = codigoInterno;
	}

	public int getHabilitado() {
		return Habilitado;
	}

	public void setHabilitado(int habilitado) {
		Habilitado = habilitado;
	}

	public String getIdOrganizacionOrigen() {
		return IdOrganizacionOrigen;
	}

	public void setIdOrganizacionOrigen(String idOrganizacionOrigen) {
		IdOrganizacionOrigen = idOrganizacionOrigen;
	}

	public String getIdOrganizacionDestino() {
		return IdOrganizacionDestino;
	}

	public void setIdOrganizacionDestino(String idOrganizacionDestino) {
		IdOrganizacionDestino = idOrganizacionDestino;
	}
    
}
