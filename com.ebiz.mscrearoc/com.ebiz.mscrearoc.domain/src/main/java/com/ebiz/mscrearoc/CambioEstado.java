package com.ebiz.mscrearoc;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CambioEstado {

    @JsonProperty("in_id")
    private UUID id;

    @JsonProperty("estadoactual")
    private String estadoactual;

    @JsonProperty("accion")
    private String accion;
    
    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }
    public String getEstadoactual() {
        return estadoactual;
    }
    public void setEstadoactual(String estadoactual) {
        this.estadoactual = estadoactual;
    }
    public String getAccion() {
        return accion;
    }
    public void setAccion(String accion) {
        this.accion = accion;
    }

}