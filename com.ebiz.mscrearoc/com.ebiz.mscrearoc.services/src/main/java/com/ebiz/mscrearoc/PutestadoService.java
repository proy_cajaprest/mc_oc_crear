package com.ebiz.mscrearoc;

import com.ebiz.mscrearoc.Putestado;

public interface PutestadoService {
	
	int actualizaroccomprador(Putestado putestado);
	int actualizarocproveedor(Putestado putestado);
	int obtenerEstadoActual(Putestado putestado);
}
