package com.ebiz.mscrearoc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OperadorLogistico {

    @JsonProperty("NombreOperador")
    private String nombreOperador;
    
    @JsonProperty("MailOperador")
    private String mailOperador;
    
    @JsonProperty("OrgIDOperador")
    //private String orgIDOperador;
    private Integer orgIDOperador;

    @JsonProperty("NombreOperador")
    public String getNombreOperador() {
        return nombreOperador;
    }

    @JsonProperty("NombreOperador")
    public void setNombreOperador(String nombreOperador) {
        this.nombreOperador = nombreOperador;
    }

    @JsonProperty("MailOperador")
    public String getMailOperador() {
        return mailOperador;
    }

    @JsonProperty("MailOperador")
    public void setMailOperador(String mailOperador) {
        this.mailOperador = mailOperador;
    }

	public Integer getOrgIDOperador() {
		return orgIDOperador;
	}

	public void setOrgIDOperador(Integer orgIDOperador) {
		this.orgIDOperador = orgIDOperador;
	}

    /*@JsonProperty("OrgIDOperador")
    public String getOrgIDOperador() {
        return orgIDOperador;
    }

    @JsonProperty("OrgIDOperador")
    public void setOrgIDOperador(String orgIDOperador) {
        this.orgIDOperador = orgIDOperador;
    }*/
    
}
