package com.ebiz.mscrearoc;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public class POUPLOADMQ {

    @JsonProperty("Usuario")
    private String usuario;
    @JsonProperty("Clave")
    private String clave;
    @JsonProperty("CompradorOrgID")
    private String compradorOrgID;
    //private String compradorOrgID;
    /*@JsonProperty("CompradorUsuarioID")
    private String compradorUsuarioID;
    //private String compradorUsuarioID;*/
    
	@JsonProperty("CompradorUsuarioID")
	private String equivalencia_comprador;
	
    @JsonProperty("Orden")
	private List<OrdenCompra> lista_oc = new ArrayList<OrdenCompra>();

    @JsonProperty("Usuario")
    public String getUsuario() {
        return usuario;
    }

    @JsonProperty("Usuario")
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

 	@JsonProperty("Clave")
    public String getClave() {
        return clave;
    }

    @JsonProperty("Clave")
    public void setClave(String clave) {
        this.clave = clave;
    }

    /*@JsonProperty("CompradorOrgID")
    public String getCompradorOrgID() {
        return compradorOrgID;
    }

    @JsonProperty("CompradorOrgID")
    public void setCompradorOrgID(String compradorOrgID) {
        this.compradorOrgID = compradorOrgID;
    }*/

    /*@JsonProperty("CompradorUsuarioID")
    public String getCompradorUsuarioID() {
        return compradorUsuarioID;
    }

    @JsonProperty("CompradorUsuarioID")
    public void setCompradorUsuarioID(String compradorUsuarioID) {
        this.compradorUsuarioID = compradorUsuarioID;
    }*/

	public List<OrdenCompra> getLista_oc() {
		return lista_oc;
	}

	public void setLista_oc(List<OrdenCompra> lista_oc) {
		this.lista_oc = lista_oc;
	}

	/*public String getCompradorUsuarioID() {
		return compradorUsuarioID;
	}

	public void setCompradorUsuarioID(String compradorUsuarioID) {
		this.compradorUsuarioID = compradorUsuarioID;
	}*/

	public String getCompradorOrgID() {
		return compradorOrgID;
	}

	public void setCompradorOrgID(String compradorOrgID) {
		this.compradorOrgID = compradorOrgID;
	}

	public String getEquivalencia_comprador() {
		return equivalencia_comprador;
	}

	public void setEquivalencia_comprador(String equivalencia_comprador) {
		this.equivalencia_comprador = equivalencia_comprador;
	}
    
    
}
