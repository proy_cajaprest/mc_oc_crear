package com.ebiz.mscrearoc.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ebiz.mscrearoc.CrearOCService;
import com.ebiz.mscrearoc.CEOrdenCompras;
import com.ebiz.mscrearoc.CrearOCRepository;
import com.ebiz.mscrearoc.OrdenCompra;
import com.ebiz.mscrearoc.Organiza;
import com.ebiz.mscrearoc.Usuario;
import com.ebiz.mscrearoc.UsuarioEquivalencia;

@Service
public class CrearOCServiceImpl implements CrearOCService{
	
	@Value("${app.constantes.cant_decimales}")
	private int cant_decimales;					// 2 o 4
	
	/*@Value("#{'${app.constantes.origen1}'.split(',')}")
	private List<String> lista_origen1;   // PEESEACE,PEB2M
	
	@Value("#{'${app.constantes.origen2}'.split(',')}")
	private List<String> lista_origen2;
	
	@Value("#{'${app.constantes.origen3}'.split(',')}")
	private List<String> lista_origen3;
	
	@Value("#{'${app.constantes.mensajes}'.split(',')}")
	private List<String> lista_mensajes;*/
	
	@Autowired
	private CrearOCRepository crearOCRepository;
	
	// retorna 1 cuando es valida, 0 cuando los valores no coinciden
/*	private int verificarSubtotal(BigDecimal cantidad, BigDecimal precio, BigDecimal total) {
		BigDecimal aux_total = cantidad.multiply(precio);
		int cmp = total.compareTo(aux_total);
		if (cmp != 0) { // en caso sean diferentes se retorna 0
			return 0;
		}
		return 1;
	}*/
	
	/*private boolean verificaDecimal(BigDecimal numero) {
		String text = numero.abs().toString();
		int integerPlaces = text.indexOf('.');
		return (text.length() - integerPlaces - 1) <= this.cant_decimales;
	}*/
	
	/*private boolean verificarDecimales(List<OrdenCompra> lista_oc) { 
		BigDecimal otros_costos, descuento, impuestos;
		for (int i=0;i<lista_oc.size();i++) {
			List<BigDecimal> lista_double = new ArrayList<BigDecimal>();
			otros_costos =lista_oc.get(i).getFl_otroscostos();
			descuento = lista_oc.get(i).getFl_descuentoporcentaje();
			impuestos = lista_oc.get(i).getFl_impuestos();
			if ( otros_costos != null)
				lista_double.add(otros_costos);
			if ( descuento !=null)
				lista_double.add(descuento);
			if ( impuestos != null)
				lista_double.add(impuestos);
			for (int j=0;j<lista_double.size();j++) {
				if (verificaDecimal(lista_double.get(j)) == false)
					return false;
			}
			if ((verificaDecimal(lista_oc.get(i).getFl_subtotal()) && verificaDecimal(lista_oc.get(i).getFl_valortotal()) && verificaDecimal(lista_oc.get(i).getFl_valorventa())) == false) {
				return false;
			}
				
			List<ProductoxOC> lista_prodxoc = lista_oc.get(i).getCotizacion().getLista_prodxoc();
			System.out.println("Obtiene lista de productos"+lista_prodxoc);
			for (int j=0;j<lista_prodxoc.size();j++) {
				ProductoxOC elemento = lista_prodxoc.get(j);
				if (verificaDecimal(elemento.getFl_cantidadproducto()) && verificaDecimal(elemento.getFl_precioproducto()) && verificaDecimal(elemento.getFl_preciototalproducto()) == false)
					return false;
			}
		}
		System.out.println("Termino de verificar decimales");
		return true;
	}*/
	
	public List<OrdenCompra> insertOC(CEOrdenCompras cEOrdenCompras,Organiza org,String token,Organiza proveedor,Usuario usuario_prov, UsuarioEquivalencia usuario_comp, UsuarioEquivalencia usuario_creador, UUID in_codigousuariocreacion)  throws Exception {
		return crearOCRepository.insertOC(cEOrdenCompras,org,token, proveedor,usuario_prov,usuario_comp,usuario_creador,  in_codigousuariocreacion );
	}
	
	/*public List<UUID> updateOC(CEOrdenCompras cEOrdenCompras,Organizacion org) {
		return crearOCRepository.updateOC(cEOrdenCompras,org);
	}*/
	
	public OrdenCompra buscar_OC(UUID in_idoc) {

		OrdenCompra oc = null;
		try{
			oc =  crearOCRepository.buscar_OC(in_idoc);
		}catch(Exception e){
			System.out.println("Error al obtener la oc");
		}
		return oc;
	}

	public OrdenCompra buscar_OCxnumseg(UUID in_idorgcompra, String numseg) {
		OrdenCompra oc = null;
		try{
			oc =  crearOCRepository.buscar_OCxnumseg(in_idorgcompra, numseg);
		}catch(Exception e){
			System.out.println("Error al obtener la oc");
		}
		return oc;
	}

	/*private boolean verificarNulos(List<OrdenCompra> lista_oc) {
		for (int i=0;i<lista_oc.size();i++) {
			if((lista_oc.get(i).getFl_valortotal() == null) && (lista_oc.get(i).getFl_valorventa() == null) && (lista_oc.get(i).getFl_subtotal() == null))
				return false;
		}
		return true; // como no habia nulos en esos campos se devuelve true
	}*/
	
	public String verificacionTotal(CEOrdenCompras cEOrdenCompras,String origen_datos) {
		/*//String origen = cEOrdenCompras.getOrigen_datos();
		//System.out.println(origen+ "origen ");
		if (origen_datos==null) {
			return lista_mensajes.get(3);
		}
		POUPLOADMQ pouploadmq = cEOrdenCompras.getPouploadmq();
			List<OrdenCompra> lista_oc = pouploadmq.getLista_oc();
			for (int i=0;i<lista_origen1.size();i++) {
				if (origen_datos.equalsIgnoreCase(lista_origen1.get(i))) {	// filtro de origen	portal		
					if(verificarNulos(lista_oc)) {
						System.out.println("PERTENECE A LA LISTA ORIGEN 1");
						if (verificarDecimales(lista_oc)) {											// filtro de decimales
							BigDecimal sub_total_oc = new BigDecimal(0.0);
							BigDecimal valor_venta_aux;
							BigDecimal valor_total_aux;
							for (OrdenCompra ordenCompra : lista_oc) {
								valor_venta_aux = new BigDecimal(0.0);;
								valor_total_aux = new BigDecimal(0.0);;
								sub_total_oc = new BigDecimal(0.0);;
								List<ProductoxOC> lista = ordenCompra.getCotizacion().getLista_prodxoc();
								ProductoxOC elemento;
								for (int j=0;j<lista.size();j++) {
									elemento = lista.get(j);
									if (verificarSubtotal(elemento.getFl_cantidadproducto(), elemento.getFl_precioproducto(), elemento.getFl_preciototalproducto()) == 0) {
										System.out.println(lista_mensajes.get(2)+ "Primero" );
										return lista_mensajes.get(2);
									}
									else
										//sub_total_oc+=elemento.getFl_preciototalproducto();
										sub_total_oc =sub_total_oc.add(elemento.getFl_preciototalproducto());
									System.out.println(elemento.getFl_preciototalproducto()+"preciototalproduc");
								}
								System.out.println(sub_total_oc+"subtotalsumaprecioproducto");
								System.out.println(ordenCompra.getFl_subtotal()+"subtotaloc");
								// verifica subtotal 
								if (ordenCompra.getFl_subtotal().compareTo(sub_total_oc) != 0) {
									System.out.println(lista_mensajes.get(2)+ "Segundo");
									return lista_mensajes.get(2);
								}							
								// verifica valor de venta
								valor_venta_aux = ordenCompra.getFl_subtotal();
								if (ordenCompra.getFl_descuentoporcentaje()!=null)
									//valor_venta_aux-=ordenCompra.getFl_descuentoporcentaje();//preguntar a Arturo
									valor_venta_aux=valor_venta_aux.subtract(ordenCompra.getFl_descuentoporcentaje());
								if (valor_venta_aux.compareTo(ordenCompra.getFl_valorventa()) != 0) {
									System.out.println(lista_mensajes.get(2)+ "Tercero");
									return lista_mensajes.get(2);
								}
								// verifica valor total
								valor_total_aux = ordenCompra.getFl_valorventa();
								if (ordenCompra.getFl_otroscostos()!=null)
									//valor_total_aux+=ordenCompra.getFl_otroscostos();
									valor_total_aux=valor_total_aux.add(ordenCompra.getFl_otroscostos());
								if (ordenCompra.getFl_impuestos()!=null)
									//valor_total_aux+=ordenCompra.getFl_impuestos();
									valor_total_aux=valor_total_aux.add(ordenCompra.getFl_impuestos());
								if (valor_total_aux.compareTo(ordenCompra.getFl_valortotal()) != 0) {
									System.out.println(lista_mensajes.get(2)+ "Cuarto");
									return lista_mensajes.get(2);
								}
							}
							System.out.println(lista_mensajes.get(0) + "Quinto");
							return lista_mensajes.get(0);
						}else
							return lista_mensajes.get(1);
					}
				}
			}
			System.out.println(lista_mensajes.get(0) + "Sexto");
		return lista_mensajes.get(0);*/
		return "Ok";
	}

	/*@Override
	public Organiza buscar_organizacion(String organizationId, String tokenBearer, String RUC, String pais) {
		// TODO Auto-generated method stub
		return crearOCRepository. buscar_organizacion(organizationId, tokenBearer, RUC, pais);
	}*/
}
