package com.ebiz.mscrearoc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ArchivoxOC {
	
	/*@JsonProperty("in_idarchivooc")
	private UUID in_idarchivooc;*/

	@JsonProperty("NombreArchivo")
	private String vc_nombre;

	@JsonProperty("ContenidoArchivo")
	private String vc_ruta;
	
	@JsonProperty("DescripcionArchivo")
	private String vc_descripcion;
	
	/*@JsonProperty("in_codigousuariocreacion")
	private int in_codigousuariocreacion;
	
	@JsonProperty("in_habilitado")
	private int in_habilitado;*/

	/*public UUID getIn_idarchivooc() {
		return in_idarchivooc;
	}

	public void setIn_idarchivooc(UUID in_idarchivooc) {
		this.in_idarchivooc = in_idarchivooc;
	}*/
	
	public String getVc_nombre() {
		return vc_nombre;
	}

	public void setVc_nombre(String vc_nombre) {
		this.vc_nombre = vc_nombre;
	}

	public String getVc_ruta() {
		return vc_ruta;
	}

	public void setVc_ruta(String vc_ruta) {
		this.vc_ruta = vc_ruta;
	}

	public String getVc_descripcion() {
		return vc_descripcion;
	}

	public void setVc_descripcion(String vc_descripcion) {
		this.vc_descripcion = vc_descripcion;
	}

	/*public int getIn_codigousuariocreacion() {
		return in_codigousuariocreacion;
	}

	public void setIn_codigousuariocreacion(int in_codigousuariocreacion) {
		this.in_codigousuariocreacion = in_codigousuariocreacion;
	}

	public int getIn_habilitado() {
		return in_habilitado;
	}

	public void setIn_habilitado(int in_habilitado) {
		this.in_habilitado = in_habilitado;
	}*/
}
