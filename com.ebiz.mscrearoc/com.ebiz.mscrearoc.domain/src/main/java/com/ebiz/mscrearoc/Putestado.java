package com.ebiz.mscrearoc;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Putestado {
	
	@JsonProperty("iddoc")
	private UUID iddoc;
	
	@JsonProperty("numeroseguimiento")
	private String numeroseguimiento;
	
	@JsonProperty("perfil")
	private String perfil;
	
	@JsonProperty("idorg")
	private UUID idorg;
	
	@JsonProperty("estadoactual")
	private String estadoactual;

	@JsonProperty("accion")
	private String accion;

	@JsonProperty("comentario")
	private String comentario;
	
    @JsonProperty("id_orgproveedor")
    private UUID id_orgproveedor; 
    
    @JsonProperty("id_orgcomprador")
    private UUID id_orgcomprador;
    
    @JsonProperty("id_usuariocompra")
    private UUID id_usuariocompra;
    
    @JsonProperty("id_usuariovende")
    private UUID id_usuariovende;

	
	public UUID getIddoc() {
		return iddoc;
	}

	public void setIddoc(UUID iddoc) {
		this.iddoc = iddoc;
	}

	public String getNumeroseguimiento() {
		return numeroseguimiento;
	}

	public void setNumeroseguimiento(String numeroseguimiento) {
		this.numeroseguimiento = numeroseguimiento;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public String getEstadoactual() {
		return estadoactual;
	}

	public void setEstadoactual(String estadoactual) {
		this.estadoactual = estadoactual;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public UUID getIdorg() {
		return idorg;
	}

	public void setIdorg(UUID idorg) {
		this.idorg = idorg;
	}

	public String getComentario() {
		return comentario;
	}
	
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public UUID getId_orgproveedor() {
		return id_orgproveedor;
	}

	public void setId_orgproveedor(UUID id_orgproveedor) {
		this.id_orgproveedor = id_orgproveedor;
	}

	public UUID getId_orgcomprador() {
		return id_orgcomprador;
	}

	public void setId_orgcomprador(UUID id_orgcomprador) {
		this.id_orgcomprador = id_orgcomprador;
	}

	public UUID getId_usuariocompra() {
		return id_usuariocompra;
	}

	public void setId_usuariocompra(UUID id_usuariocompra) {
		this.id_usuariocompra = id_usuariocompra;
	}

	public UUID getId_usuariovende() {
		return id_usuariovende;
	}

	public void setId_usuariovende(UUID id_usuariovende) {
		this.id_usuariovende = id_usuariovende;
	}	
	
	
	
}
