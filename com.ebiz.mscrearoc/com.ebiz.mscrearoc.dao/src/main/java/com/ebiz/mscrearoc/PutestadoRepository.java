package com.ebiz.mscrearoc;

import com.ebiz.mscrearoc.Putestado;

public interface PutestadoRepository {
	

	int actualizaroccomprador(Putestado putestado);
	int actualizarocproveedor(Putestado putestado);
	int obtenerEstadoActual(Putestado putestado);

}