package com.ebiz.mscrearoc.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ebiz.mscrearoc.ComentarioRepository;
import com.ebiz.mscrearoc.ComentarioService;
import com.ebiz.mscrearoc.Comentario;

@Service
public class ComentarioServiceImpl implements ComentarioService {
	
	@Autowired
	private ComentarioRepository comentarioRepository;


	public int guardarcomentariocomprador(Comentario comentario) {
		return comentarioRepository.guardarcomentariocomprador(comentario);
	}

	public int guardarcomentarioproveedor(Comentario comentario) {
		return comentarioRepository.guardarcomentarioproveedor(comentario);
	}

}