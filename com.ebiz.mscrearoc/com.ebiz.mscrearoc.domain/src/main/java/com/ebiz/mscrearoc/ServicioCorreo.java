package com.ebiz.mscrearoc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ServicioCorreo {

    @JsonProperty("StateIni")
    private String stateIni;
    
    @JsonProperty("StateFin")
    private String stateFin;
    
    @JsonProperty("Frecuency")
    private String frecuency;
    
    @JsonProperty("Msgmail")
    private String msgmail;

    @JsonProperty("StateIni")
    public String getStateIni() {
        return stateIni;
    }

    @JsonProperty("StateIni")
    public void setStateIni(String stateIni) {
        this.stateIni = stateIni;
    }

    @JsonProperty("StateFin")
    public String getStateFin() {
        return stateFin;
    }

    @JsonProperty("StateFin")
    public void setStateFin(String stateFin) {
        this.stateFin = stateFin;
    }

    @JsonProperty("Frecuency")
    public String getFrecuency() {
        return frecuency;
    }

    @JsonProperty("Frecuency")
    public void setFrecuency(String frecuency) {
        this.frecuency = frecuency;
    }

    @JsonProperty("Msgmail")
    public String getMsgmail() {
        return msgmail;
    }

    @JsonProperty("Msgmail")
    public void setMsgmail(String msgmail) {
        this.msgmail = msgmail;
    }
}
