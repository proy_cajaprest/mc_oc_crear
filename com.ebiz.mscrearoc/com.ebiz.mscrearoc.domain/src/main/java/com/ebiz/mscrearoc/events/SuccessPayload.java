package com.ebiz.mscrearoc.events;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.UUID;

public class SuccessPayload {

    @JsonProperty("id_doc")
    private UUID id;

    @JsonProperty("num_doc")
    private String num;
    
    @JsonProperty("accion")
    private String accion;
    
    @JsonProperty("id_orgproveedor")
    private UUID id_orgproveedor; 
    
    @JsonProperty("id_orgcomprador")
    private UUID id_orgcomprador;
    
    @JsonProperty("id_usuariocompra")
    private UUID id_usuariocompra;
    
    @JsonProperty("id_usuariovende")
    private UUID id_usuariovende;

	@JsonProperty("motivo")
	private String motivo;
	
	@JsonProperty("nombre_usuariocreadopor")
	private String nombre_usuariocreadopor;
	
    @JsonProperty("id_usuariocreadopor")
    private UUID id_usuariocreadopor;

    public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}

    public UUID getId_doc() {
        return id;
    }
    public void setId_doc(UUID id_doc) {
        this.id = id_doc;
    }
    public String getNum_doc() {
        return num;
    }
    public void setNum_doc(String num_doc) {
        this.num = num_doc;
    }
	public UUID getId_orgproveedor() {
		return id_orgproveedor;
	}
	public void setId_orgproveedor(UUID id_orgproveedor) {
		this.id_orgproveedor = id_orgproveedor;
	}
	public UUID getId_orgcomprador() {
		return id_orgcomprador;
	}
	public void setId_orgcomprador(UUID id_orgcomprador) {
		this.id_orgcomprador = id_orgcomprador;
	}
	public UUID getId_usuariocompra() {
		return id_usuariocompra;
	}
	public void setId_usuariocompra(UUID id_usuariocompra) {
		this.id_usuariocompra = id_usuariocompra;
	}
	public UUID getId_usuariovende() {
		return id_usuariovende;
	}
	public void setId_usuariovende(UUID id_usuariovende) {
		this.id_usuariovende = id_usuariovende;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public String getNombre_usuariocreadopor() {
		return nombre_usuariocreadopor;
	}
	public void setNombre_usuariocreadopor(String nombre_usuariocreadopor) {
		this.nombre_usuariocreadopor = nombre_usuariocreadopor;
	}
	public UUID getId_usuariocreadopor() {
		return id_usuariocreadopor;
	}
	public void setId_usuariocreadopor(UUID id_usuariocreadopor) {
		this.id_usuariocreadopor = id_usuariocreadopor;
	}
	
	
}