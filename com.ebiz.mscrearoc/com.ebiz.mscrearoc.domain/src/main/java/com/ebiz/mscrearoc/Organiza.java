package com.ebiz.mscrearoc;

import java.math.BigDecimal;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Organiza {
	
	@JsonProperty("IdOrganizacion")
	private UUID in_idorganizacion;
	
	@JsonProperty("Habilitado")
	private Integer vc_habilitado;
	
	@JsonProperty("Montoventa")
	private BigDecimal vc_montoventa;

	public UUID getIn_idorganizacion() {
		return in_idorganizacion;
	}

	public void setIn_idorganizacion(UUID in_idorganizacion) {
		this.in_idorganizacion = in_idorganizacion;
	}

	public Integer getVc_habilitado() {
		return vc_habilitado;
	}

	public void setVc_habilitado(Integer vc_habilitado) {
		this.vc_habilitado = vc_habilitado;
	}

	public BigDecimal getVc_montoventa() {
		return vc_montoventa;
	}

	public void setVc_montoventa(BigDecimal vc_montoventa) {
		this.vc_montoventa = vc_montoventa;
	}
			  	
}
