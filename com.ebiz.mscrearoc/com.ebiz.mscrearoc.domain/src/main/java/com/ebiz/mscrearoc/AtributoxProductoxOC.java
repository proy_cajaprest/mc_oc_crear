package com.ebiz.mscrearoc;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.UUID;

public class AtributoxProductoxOC {
	
	@JsonProperty("in_idatributo")
	private UUID in_idatributo;
	
	@JsonProperty("ValorAtributoProducto")
	private String vc_valorenviado;//ValorAtributoProducto
	
	@JsonProperty("OfferingAttributeEquiv")
	private String vc_valorrealorg;//OfferingAttributeEquiv
	
	@JsonProperty("-Obligatorio")
	private String ch_mandatorio;//-Obligatorio
	
	@JsonProperty("-Modificable")
	private String vc_modificable;
	
	private Integer in_idunidad;//UnidadAtributoProducto
	
	@JsonProperty("OperadorAtributoProducto")
	private String vc_operador ;//OperadorAtributoProducto

	//@JsonProperty("vc_nombreatributo")
	@JsonProperty("-Nombre")
	private String vc_nombreatributo ;//-Nombre
	
	@JsonProperty("in_codigousuariocreacion")
	private Integer in_codigousuariocreacion ;
	
	@JsonProperty("in_habilitado")
	private Integer in_habilitado ;
	
	@JsonProperty("UnidadAtributoProducto")
	private String vc_nombreunidad;//UnidadAtributoProducto
	
	private String vc_idtablaoperador;
	
	private String vc_idregistrooperador;
	
	private String vc_idtablaunidad;
	
	private String vc_idregistrounidad;
	
//////////////////////////////////
	
	
	public UUID getIn_idatributo() {
		return in_idatributo;
	}

	public void setIn_idatributo(UUID in_idatributo) {
		this.in_idatributo = in_idatributo;
	}

	public String getVc_valorenviado() {
		return vc_valorenviado;
	}

	public void setVc_valorenviado(String vc_valorenviado) {
		this.vc_valorenviado = vc_valorenviado;
	}

	public String getVc_nombreatributo() {
		return vc_nombreatributo;
	}

	public void setVc_nombreatributo(String vc_nombreatributo) {
		this.vc_nombreatributo = vc_nombreatributo;
	}

	public Integer getIn_codigousuariocreacion() {
		return in_codigousuariocreacion;
	}

	public void setIn_codigousuariocreacion(Integer in_codigousuariocreacion) {
		this.in_codigousuariocreacion = in_codigousuariocreacion;
	}

	public Integer getIn_habilitado() {
		return in_habilitado;
	}

	public void setIn_habilitado(Integer in_habilitado) {
		this.in_habilitado = in_habilitado;
	}

	public String getVc_valorrealorg() {
		return vc_valorrealorg;
	}

	public void setVc_valorrealorg(String vc_valorrealorg) {
		this.vc_valorrealorg = vc_valorrealorg;
	}

	public String getCh_mandatorio() {
		return ch_mandatorio;
	}

	public void setCh_mandatorio(String ch_mandatorio) {
		this.ch_mandatorio = ch_mandatorio;
	}

	public String getVc_modificable() {
		return vc_modificable;
	}

	public void setVc_modificable(String vc_modificable) {
		this.vc_modificable = vc_modificable;
	}

	public Integer getIn_idunidad() {
		return in_idunidad;
	}

	public void setIn_idunidad(Integer in_idunidad) {
		this.in_idunidad = in_idunidad;
	}

	public String getVc_operador() {
		return vc_operador;
	}

	public void setVc_operador(String vc_operador) {
		this.vc_operador = vc_operador;
	}

	public String getVc_nombreunidad() {
		return vc_nombreunidad;
	}

	public void setVc_nombreunidad(String vc_nombreunidad) {
		this.vc_nombreunidad = vc_nombreunidad;
	}

	public String getVc_idtablaoperador() {
		return vc_idtablaoperador;
	}

	public void setVc_idtablaoperador(String vc_idtablaoperador) {
		this.vc_idtablaoperador = vc_idtablaoperador;
	}

	public String getVc_idregistrooperador() {
		return vc_idregistrooperador;
	}

	public void setVc_idregistrooperador(String vc_idregistrooperador) {
		this.vc_idregistrooperador = vc_idregistrooperador;
	}

	public String getVc_idtablaunidad() {
		return vc_idtablaunidad;
	}

	public void setVc_idtablaunidad(String vc_idtablaunidad) {
		this.vc_idtablaunidad = vc_idtablaunidad;
	}

	public String getVc_idregistrounidad() {
		return vc_idregistrounidad;
	}

	public void setVc_idregistrounidad(String vc_idregistrounidad) {
		this.vc_idregistrounidad = vc_idregistrounidad;
	}
	
	
	
	
	
	//////////////////////////////////////
}
