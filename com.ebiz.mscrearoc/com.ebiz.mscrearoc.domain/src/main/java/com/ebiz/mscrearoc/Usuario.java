package com.ebiz.mscrearoc;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Usuario {

    @JsonProperty("Usuario")
    private String usuario;

    @JsonProperty("Nombre")
    private String nombre;
    
    private String nombre_org;

    @JsonProperty("ApellidoPaterno")
    private String apellidoPaterno;
    
    private String apellidoMaterno;

    @JsonProperty("NumeroDocumento")
    private String numeroDocumento;

    @JsonProperty("Direccion")
    private String direccion;

    @JsonProperty("Telefono")
    private String telefono;

    @JsonProperty("Email")
    private String email;

    @JsonProperty("Ciudad")
    private String ciudad;
    
    private UUID id_usuario;
        
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public UUID getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(UUID id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getNombre_org() {
		return nombre_org;
	}

	public void setNombre_org(String nombre_org) {
		this.nombre_org = nombre_org;
	}
	
	
	
}
