package com.ebiz.mscrearoc;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Comentario {

	@JsonProperty("in_id")
    private UUID in_id;
	
	@JsonProperty("vc_num")
	private String vc_num;
	
	@JsonProperty("in_idorg")
	private UUID in_idorg;
	
	@JsonProperty("comentario")
    private String vc_comentario;
	
    @JsonProperty("id_orgproveedor")
    private UUID id_orgproveedor; 
    
    @JsonProperty("id_orgcomprador")
    private UUID id_orgcomprador;
    
    @JsonProperty("id_usuariocompra")
    private UUID id_usuariocompra;
    
    @JsonProperty("id_usuariovende")
    private UUID id_usuariovende;
    
    public UUID getIn_id() {
        return in_id;
    }

    public void setIn_id(UUID in_id) {
        this.in_id = in_id;
    }
    
    public String getVc_num() {
		return vc_num;
	}

	public void setVc_num(String vc_num) {
		this.vc_num = vc_num;
	}

    public String getVc_comentario() {
        return vc_comentario;
    }

    public void setVc_comentario(String vc_comentario) {
        this.vc_comentario = vc_comentario;
    }

	public UUID getIn_idorg() {
		return in_idorg;
	}

	public void setIn_idorg(UUID in_idorg) {
		this.in_idorg = in_idorg;
	}

	public UUID getId_orgproveedor() {
		return id_orgproveedor;
	}

	public void setId_orgproveedor(UUID id_orgproveedor) {
		this.id_orgproveedor = id_orgproveedor;
	}

	public UUID getId_orgcomprador() {
		return id_orgcomprador;
	}

	public void setId_orgcomprador(UUID id_orgcomprador) {
		this.id_orgcomprador = id_orgcomprador;
	}

	public UUID getId_usuariocompra() {
		return id_usuariocompra;
	}

	public void setId_usuariocompra(UUID id_usuariocompra) {
		this.id_usuariocompra = id_usuariocompra;
	}

	public UUID getId_usuariovende() {
		return id_usuariovende;
	}

	public void setId_usuariovende(UUID id_usuariovende) {
		this.id_usuariovende = id_usuariovende;
	}
	

}