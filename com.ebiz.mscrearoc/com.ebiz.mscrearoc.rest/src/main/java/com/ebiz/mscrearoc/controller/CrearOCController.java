package com.ebiz.mscrearoc.controller;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.ebiz.mscrearoc.CEOrdenCompras;
import com.ebiz.mscrearoc.CrearOCService;
import com.ebiz.mscrearoc.EquivalenciaOrg;
import com.ebiz.events.service.EventManagerService;
import com.ebiz.mscrearoc.OrdenCompra;
import com.ebiz.mscrearoc.OrdenListServiceResponse;
import com.ebiz.mscrearoc.Organiza;
import com.ebiz.events.service.EventSenderService;
import com.ebiz.mscrearoc.Putestado;
import com.ebiz.mscrearoc.PutestadoService;
import com.ebiz.mscrearoc.Rol;
import com.ebiz.mscrearoc.Usuario;
import com.ebiz.mscrearoc.UsuarioEquivalencia;
import com.ebiz.events.Evento;
import com.ebiz.mscrearoc.events.SuccessPayload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

//XC
import com.ebiz.mscrearoc.Comentario;
import com.ebiz.mscrearoc.ComentarioService;
//FIN XC
import com.ebiz.mscrearoc.CreaOrganizacion;


@Component
@EnableKafka
public class CrearOCController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CrearOCController.class);

	/*@Value("#{'${app.constantes.mensajes}'.split(',')}")
	private List<String> lista_mensajes;*/
	
	@Autowired
	private ObjectMapper mapper;
	
	@Value("${app.constantes.put}")
	private String put;
	
	@Value("${app.constantes.post}")
	private String post;
	
	@Value("${app.constantes.administrador}")
	private String administrador;
	
	@Value("${app.constantes.adm_proveedor}")
	private String adm_proveedor;

	@Autowired
	private CrearOCService crearOCService;

	@Autowired
	private EventManagerService eventManagerService;

	@Autowired
	private EventSenderService productor;
	
	@Autowired
	private PutestadoService putestadoService;

	//XC
	@Autowired
	private ComentarioService comentarioService;
	//FIN XC

	@Value("${events.topic.output.cuadroeval}")
	private String cuadroEvalTopic;
	
	//@Value("${events.topic.output.cambioestado}")
	//private String cambioestadoTopic;
	
	@Value("${events.topic.output.respuesta}")
	private String topicRespuesta;

	@KafkaListener(topics = "${events.topic.input.name}")
	public void listen(String payload) {
		LOGGER.info("El evento inicio");
		LOGGER.info("\n\n" + payload + "\n\n");
		//Recepcion de evento
		Evento<CEOrdenCompras> reqEvento = eventManagerService.deserializeEvent(payload, CEOrdenCompras.class);

		LOGGER.info("¿Evento válido?");
		if (reqEvento == null) {
			LOGGER.info("No. El evento es nulo");
			LOGGER.info("El evento no tiene el formato requerido por el microservicio");
			Evento<JsonNode> errorEvento = eventManagerService.deserializeEvent(payload, JsonNode.class);

			if (errorEvento != null) {
				LOGGER.info("event_id: " + errorEvento.getEvent_id());
				errorEvento = eventManagerService.initErrorEvent(errorEvento, "creacion","El formato enviado es incorrecto");

				if (!productor.enviar(errorEvento.getData().getMeta().getTarget(),
						eventManagerService.serializeEvent(errorEvento))) {
					LOGGER.info("El evento de error no se pudo enviar");
				}

			} else {
				LOGGER.info("El objeto recibido no es un evento");
			}
			return;
		}else {
			LOGGER.info("Sí.");
		}

		LOGGER.info("Evento recibido\nevent_id: " + reqEvento.getEvent_id());
		String origen_datos = reqEvento.getData().getMeta().getOrigen_datos();
		String http_verb = reqEvento.getData().getMeta().getHttp_verb();
		String token = reqEvento.getData().getMeta().getSession_id();
		UUID org_id = reqEvento.getData().getMeta().getOrg_id();//debe traer el ruc
		LOGGER.info(origen_datos+" origen datos listen ");
		//Desencapsulacion y procesamiento
		CEOrdenCompras cEOrdenCompras = reqEvento.getData().getPayload();//Desencapsualcion del evento
		List<OrdenCompra> lista_idoc = null;

		//System.out.println("SE CREO RESPUESTA" + lista_mensajes);
		//String msj = crearOCService.verificacionTotal(cEOrdenCompras,origen_datos);
		String msj  =  "";
		//System.out.println("MENSAJE : " + msj);
		//if (msj.equalsIgnoreCase(lista_mensajes.get(0))) {//para portal
			try {
				String pais = cEOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_rucproveedor().substring(0, 2);
				LOGGER.info("Buscando Proveedor...");
				Organiza proveedor=buscar_organizacion(cEOrdenCompras.getPouploadmq().getCompradorOrgID(), token,cEOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_rucproveedor(), pais );
				Usuario usuario_prov=null;
				UsuarioEquivalencia usu_compra = null;
				UsuarioEquivalencia usuario_creador = null;
				Rol rol=null;
				EquivalenciaOrg equiva_organizacion = null;
				
				if (proveedor==null) {
					LOGGER.info("Se creara proveedor");
					proveedor = listen_crearorganizacion(cEOrdenCompras, reqEvento);
					if (proveedor!=null) {
						equiva_organizacion=buscar_equivalencia( token, org_id, proveedor.getIn_idorganizacion(), cEOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_codinternoprov() );
						if (equiva_organizacion==null) {
							LOGGER.info("Se creara equivalencia");
							equiva_organizacion = listen_crearequivalenciaorganizacion(cEOrdenCompras, reqEvento, org_id , proveedor.getIn_idorganizacion());
						}
						usuario_prov=buscar_usuario_adm(proveedor.getIn_idorganizacion(), token, "2");
						if (usuario_prov==null) {
							LOGGER.info("Se creara usuario proveedor");
							rol = buscar_rol(token, adm_proveedor);
							LOGGER.info(rol.getRolId() +" rol");
							usuario_prov = listen_crearusuario(cEOrdenCompras,reqEvento,proveedor.getIn_idorganizacion(),rol);
						}
					}
				}
				
				Organiza org= new Organiza();
				/*org=crearOCService.buscar_organizacion(cEOrdenCompras);//eliminarlo viene en el campo orgid*/
				/*if (cEOrdenCompras.getPouploadmq().getCompradorOrgID()!=null) {
					org=crearOCService.buscar_organizacion(cEOrdenCompras.getPouploadmq().getCompradorOrgID(),token,cEOrdenCompras.getPouploadmq().getCompradorOrgID());
				}*/
				org.setIn_idorganizacion(org_id);
				LOGGER.info("Buscar usuario comprador...");
				
				LOGGER.info(cEOrdenCompras.getPouploadmq().getEquivalencia_comprador() + " equivalencia comprador");
				
				if (cEOrdenCompras.getPouploadmq().getEquivalencia_comprador()!=null) {
					LOGGER.info(cEOrdenCompras.getPouploadmq().getEquivalencia_comprador() + " usuario comprador");
					usu_compra = equivalenciaUsuario(org.getIn_idorganizacion(), token, cEOrdenCompras.getPouploadmq().getEquivalencia_comprador());
					if (usu_compra!=null) {
						LOGGER.info(usu_compra.getIdusuario() +" Usuario comprador ");
					}
				}
								
				if (usuario_prov==null) {
					LOGGER.info("Se creara usuario proveedor");
					rol = buscar_rol(token, adm_proveedor);
					LOGGER.info(rol.getRolId() +" rol");
					usuario_prov = listen_crearusuario(cEOrdenCompras,reqEvento,proveedor.getIn_idorganizacion(),rol);
				}
				
				if (cEOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_creadopor()!=null) {
					LOGGER.info(cEOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_creadopor() + " creadoPor");
					usuario_creador = equivalenciaUsuario(org.getIn_idorganizacion(), token, cEOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_creadopor());
					if (usuario_creador!=null) {
						LOGGER.info(usuario_creador.getIdusuario() +" Usuario creador ");
					}
				}
				
				if (org.getIn_idorganizacion()!=null && proveedor!=null && proveedor.getIn_idorganizacion()!=null && usuario_prov!=null && usuario_creador!=null ) {
					LOGGER.info(org.getIn_idorganizacion()+" idorg compradora");
					LOGGER.info(token + " token ");
					UUID idusuario_auditoria = getUserId(token);
					//LOGGER.info(org.getVc_ruc()+" ruc");
					if (http_verb.equalsIgnoreCase(post)) {
						lista_idoc = crearOCService.insertOC(cEOrdenCompras,org,token,proveedor,usuario_prov,usu_compra,usuario_creador,idusuario_auditoria); // retorna una lista de UUID
						LOGGER.info("SE INSERTO CORRECTAMENTE");
					}
					/*else if(http_verb.equalsIgnoreCase(put)) {
						//
						//probando rest template
						int prueba=queryStatus(null,null);
						LOGGER.info("funcionma RestTemplate"+prueba);
						lista_idoc = crearOCService.updateOC(cEOrdenCompras,org); // retorna una lista de UUID
						LOGGER.info("SE ACTUALIZO CORRECTAMENTE");
						
						/*Evento<CambioEstado> cambioestadoEvento=eventManagerService.createEmptyEvent(CambioEstado.class);
						cambioestadoEvento.getData().setMeta(reqEvento.getData().getMeta());
						cambioestadoEvento = eventManagerService.initCustomEvent(cambioestadoEvento, "cambioestado-oc-solicitado", "oc", cambioestadoTopic);
						CambioEstado cambioestado=new CambioEstado();
						//obtener del select los id
						cambioestado.setId(id);*/
						/*consumir servicio REST*/
						/*RestTemplate restTemplate = new RestTemplate();
						SuccessPayload successPayload = restTemplate.getForObject("http://localhost:8080/spring-rest/data/fetchxml/{id}", SuccessPayload.class, 200);*/
						/*fin servicio REST*/
						
						/*RestTemplate restTemplate = new RestTemplate();
						String uri = "http://localhost:8080/spring-rest/data/exchange/{id}";
					        HttpHeaders headers = new HttpHeaders();
					        headers.setContentType(MediaType.APPLICATION_JSON);
					        
					        HttpEntity<String> entity = new HttpEntity<String>("Hello World!", headers);
					        ResponseEntity<SuccessPayload> personEntity = restTemplate.exchange(uri, HttpMethod.PUT, entity, SuccessPayload.class, 200);*/
					        //personEntity.get
					        
	
					/*}*/
				}else {
					Evento<CEOrdenCompras> errorEvento = eventManagerService.initErrorEvent(reqEvento, "creacion", msj);
					if (!productor.enviar(errorEvento.getData().getMeta().getTarget(),
							eventManagerService.serializeEvent(errorEvento))) {
						LOGGER.info("La organizacion compradora o proveedora no existe");
					}
					return;
				}
			}catch (Exception e) {
				e.printStackTrace();
				LOGGER.error(e.getMessage());
				Evento<CEOrdenCompras> errorEvento = eventManagerService.initErrorEvent(reqEvento, "creacion", msj);
				if (!productor.enviar(errorEvento.getData().getMeta().getTarget(),
						eventManagerService.serializeEvent(errorEvento))) {
					LOGGER.info("La organizacion compradora o proveedora no existe");
				}
				return;
			}
		/*} else {
			Evento<CEOrdenCompras> errorEvento = eventManagerService.initErrorEvent(reqEvento, "creacion", msj);
			if (!productor.enviar(errorEvento.getData().getMeta().getTarget(),
					eventManagerService.serializeEvent(errorEvento))) {
				LOGGER.info("El evento de error no se pudo enviar");
			}
			return;
		}*/

		//Envio de evento de salida
		if (lista_idoc != null && lista_idoc.size()>0) {

			String mensaje = "Se ha insertado la(s) orden de compra con id ";
			for (OrdenCompra id : lista_idoc) {
				mensaje += id.getIn_idoc() + ", ";
			}

			LOGGER.info(mensaje);
			/*CuadroEvaluacion cuadroEvaluacion = cEOrdenCompras.getCuadroEvaluacion();
			/// Segun el correspondiente flag 
			if (cuadroEvaluacion != null) {
				cuadroEvaluacion.setLista_idoc(lista_idoc);
				System.out.println("PASO EL CUADRO DE EVAL");

				Evento<CuadroEvaluacion> cuadroevalEvento = eventManagerService
						.createEmptyEvent(CuadroEvaluacion.class);
				cuadroevalEvento.getData().setMeta(reqEvento.getData().getMeta());

				cuadroevalEvento = eventManagerService.initCustomEvent(cuadroevalEvento,
						"creacion-cuadroeval-solicitado", "cuadroeval", cuadroEvalTopic);

				if (!productor.enviar(cuadroevalEvento.getData().getMeta().getTarget(),
						eventManagerService.serializeEvent(cuadroevalEvento))) {
					System.out.println("El evento de creacion de cuadroeval no se pudo enviar");
				}
				System.out.println("\n\n" + "Se envio al micro servicio de cuadro de evaluacion" + "\n\n");

			} else {
				System.out.println("NO PASO EL CUADRO DE EVAL");
			}*/

			Evento<SuccessPayload> resEvento = eventManagerService.createEmptyEvent(SuccessPayload.class);
			resEvento.getData().setMeta(reqEvento.getData().getMeta());

			resEvento = eventManagerService.initSuccessEvent(resEvento, "creado");

			//llenado de campos de la respuesta
			SuccessPayload successPayload = new SuccessPayload();
			successPayload.setId_doc(lista_idoc.get(0).getIn_idoc());//Por cambiar
			successPayload.setNum_doc(cEOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_numeroseguimiento());//Por cambiar
			successPayload.setId_orgcomprador(lista_idoc.get(0).getIn_idorgcompra());
			successPayload.setId_orgproveedor(lista_idoc.get(0).getIn_idorganizacionproveedora());
			successPayload.setId_usuariocompra(lista_idoc.get(0).getIn_idusuariocomprador());
			successPayload.setId_usuariovende(lista_idoc.get(0).getIn_idusuarioproveedor());
			successPayload.setId_usuariocreadopor(lista_idoc.get(0).getId_usuariocreadopor());
			successPayload.setNombre_usuariocreadopor(lista_idoc.get(0).getNombre_usuariocreadopor());

			resEvento.getData().setPayload(successPayload);

			if (!productor.enviar(resEvento.getData().getMeta().getTarget(),
					eventManagerService.serializeEvent(resEvento))) {
				LOGGER.info("El evento de creacion no se pudo enviar");
			}
		} else {
			Evento<CEOrdenCompras> errorEvento = eventManagerService.initErrorEvent(reqEvento, "creacion",
					"Error en los datos proporcionados");

			if (!productor.enviar(errorEvento.getData().getMeta().getTarget(),
					eventManagerService.serializeEvent(errorEvento))) {
				LOGGER.info("El evento de error no se pudo enviar");
			}
		}
	}

	@KafkaListener(topics = "${events.topic.input.comentario}")
	public void listenComentario(String payload) {

		///////////////////////7
		Evento<Comentario> reqEvento = eventManagerService.deserializeEvent(payload, Comentario.class);
		
		if (reqEvento == null) {
			LOGGER.info("El evento no tiene el formato requerido por el microservicio");
			Evento<JsonNode> errorEvento = eventManagerService.deserializeEvent(payload, JsonNode.class);

			if (errorEvento != null) {
				LOGGER.info("event_id: " + errorEvento.getEvent_id());
				errorEvento = eventManagerService.initErrorEvent(errorEvento, "comentario", "El formato enviado es incorrecto");

				if (!productor.enviar(errorEvento.getData().getMeta().getTarget(),
						eventManagerService.serializeEvent(errorEvento))) {
					LOGGER.info("El evento de error no se pudo enviar");
				}

			} else {
				LOGGER.info("El objeto recibido no es un evento");
			}
			return;
		}
		////////////////////////
		LOGGER.info("Evento recibido\nevent_id: " + reqEvento.getEvent_id());
		Comentario est = reqEvento.getData().getPayload();        
		int res=0;
		if(reqEvento.getData().getMeta().getTipo_empresa().equalsIgnoreCase("C")){
			res = comentarioService.guardarcomentariocomprador(est);
		}
		if(reqEvento.getData().getMeta().getTipo_empresa().equalsIgnoreCase("P")){
			res = comentarioService.guardarcomentarioproveedor(est);
		}

		////////////////////////
		if (res == 1) {
			
			Evento<SuccessPayload> resEvento = eventManagerService.createEmptyEvent(SuccessPayload.class);
			resEvento.getData().setMeta(reqEvento.getData().getMeta());

			resEvento = eventManagerService.initSuccessEvent(resEvento, "actualizado");

			//llenado de campos de la respuesta
			SuccessPayload successPayload = new SuccessPayload();
			successPayload.setId_doc(est.getIn_id());
			//successPayload.setNum_doc(putestadoService.getNroSeguimiento(successPayload.getId_doc()));
			OrdenCompra oc=crearOCService.buscar_OC(est.getIn_id());
			successPayload.setId_orgcomprador(oc.getIn_idorgcompra());
			successPayload.setId_orgproveedor(oc.getIn_idorganizacionproveedora());
			successPayload.setId_usuariocompra(oc.getIn_idusuariocomprador());
			successPayload.setId_usuariovende(oc.getIn_idusuarioproveedor());
			successPayload.setId_usuariocreadopor(oc.getId_usuariocreadopor());
			successPayload.setNombre_usuariocreadopor(oc.getNombre_usuariocreadopor());

			resEvento.getData().setPayload(successPayload);

			if (!productor.enviar(resEvento.getData().getMeta().getTarget(),
					eventManagerService.serializeEvent(resEvento))) {
				LOGGER.info("El evento de actualizacion no se pudo enviar");
			}

		} else {
			Evento<Comentario> errorEvento = eventManagerService.initErrorEvent(reqEvento, "actualizacion", "Error en los datos proporcionados");

			LOGGER.info(errorEvento.getEvent_id() + " -- " + errorEvento.getData().getMeta().getTarget());

			if (!productor.enviar(errorEvento.getData().getMeta().getTarget(),
					eventManagerService.serializeEvent(errorEvento))) {
				LOGGER.info("El evento de error no se pudo enviar");
			}
		}
	}


	//metodo antiguo
	/*public void listen_insertoc(String payload) {
	
		System.out.println("\n\n" + "OK" + "\n\n");
	
		//Recepcion de evento
		Evento<CEOrdenCompras> reqEvento = new Evento<>();
	
		try {
			reqEvento = mapper.readValue(payload, new TypeReference<Evento<CEOrdenCompras>>() {
			});
		} catch (Exception e) {
	
			System.out.println("El evento no tiene el formato requerido por el microservicio");
	
			Evento<JsonNode> errorEvento = new Evento<>();
			try {
				errorEvento = mapper.readValue(payload, new TypeReference<Evento<JsonNode>>() {
				});
			} catch (Exception e2) {
				e2.printStackTrace();
			}
	
			eventManagerService.initErrorEvent(errorEvento);
	
			try {
				productor.enviar(errorEvento.getData().getMeta().getTarget(), mapper.writeValueAsString(errorEvento));
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			e.printStackTrace();
			return;
		}
		//Fin Recepcion de evento
	
		CEOrdenCompras cEOrdenCompras = reqEvento.getData().getPayload();//Desencapsualcion del evento
		List<UUID> lista_idoc = null;
	
		System.out.println("SE CREO RESPUESTA" + lista_mensajes);
		String msj = crearOCService.verificacionTotal(cEOrdenCompras);
		System.out.println("MENSAJE : " + msj);
		if (msj.equalsIgnoreCase(lista_mensajes.get(0))) {
			try {
				lista_idoc = crearOCService.insertOC(cEOrdenCompras); // retorna una lista de UUID
				System.out.println("SE INSERTO CORRECTAMENTE");
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e.getMessage().toString());
			}
		} else {
			//respuesta.setEstado(1);
			//respuesta.setMensaje(msj);
			Evento<CEOrdenCompras> errorEvento = reqEvento;
			eventManagerService.initErrorEvent(errorEvento);
			try {
				productor.enviar(errorEvento.getData().getMeta().getTarget(), mapper.writeValueAsString(errorEvento));
			} catch (Exception e) {
				e.printStackTrace();
			}
			return;
		}
	
		if (lista_idoc != null) {
	
			String mensaje = "Se ha insertado la(s) orden de compra con id ";
			for (UUID id : lista_idoc) {
				mensaje += id + ", ";
			}
	
			//respuesta.setEstado(0);
			//respuesta.setMensaje(mensaje);
			System.out.println(mensaje);
			CuadroEvaluacion cuadroEvaluacion = cEOrdenCompras.getCuadroEvaluacion();
			/// Segun el correspondiente flag 
			if (cuadroEvaluacion != null) {
				cuadroEvaluacion.setLista_idoc(lista_idoc);
				System.out.println("PASO EL CUADRO DE EVAL");
				try {
					//String jsonCuadroEvaluacion = mapper.writeValueAsString(cuadroEvaluacion);
	
					Evento<CuadroEvaluacion> cuadroevalEvento = new Evento<>();
					Data<CuadroEvaluacion> data = new Data<>();
					data.setMeta(reqEvento.getData().getMeta());
					cuadroevalEvento.setData(data);
	
					eventManagerService.initCustomEvent(cuadroevalEvento, "creacion-cuadroeval-solicitado",
							"cuadroeval", cuadroEvalTopic);
	
					String jsonCuadroEvaluacion = mapper.writeValueAsString(cuadroevalEvento);
					productor.enviar(cuadroEvalTopic, jsonCuadroEvaluacion);
					System.out.println("\n\n" + "Se envio al micro servicio de cuadro de evaluacion" + "\n\n");
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
			}
			else {
				System.out.println("NO PASO EL CUADRO DE EVAL");
			}
	
			Evento<SuccessPayload> resEvento = new Evento<>();
			Data<SuccessPayload> data = new Data<>();
			data.setMeta(reqEvento.getData().getMeta());
			data.getMeta().setResource("oc");
			resEvento.setData(data);
	
			eventManagerService.initSuccessEvent(resEvento);
	
			//llenado de campos de la respuesta
			SuccessPayload successPayload = new SuccessPayload();
			successPayload.setId_doc(lista_idoc.get(0));//Por cambiar
			successPayload.setNum_doc(cEOrdenCompras.getLista_oc().get(0).getVc_numeroseguimiento());//Por cambiar
	
			data.setPayload(successPayload);
	
			try {
				productor.enviar(resEvento.getData().getMeta().getTarget(), mapper.writeValueAsString(resEvento));
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			Evento<CEOrdenCompras> errorEvento = reqEvento;
			eventManagerService.initErrorEvent(errorEvento);
			try {
				productor.enviar(errorEvento.getData().getMeta().getTarget(), mapper.writeValueAsString(errorEvento));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	
	}*/
	
	//private static final String STATUS_CODE="0000";
	/*@Autowired
	private RestTemplate restTemplate;

	@Value("${app.constantes.url}")
	private String queryUri;
	
	private final static String TIPO_EMPRESA="C";
	private final static String ORIGEN_DATOS="PEB2M";
	
	public int queryStatus(String organizationId,String tokenBearer) {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		//headers.set("Authorization", String.format("Bearer %s",tokenBearer));}
		headers.set("Authorization", "Bearer comprador1");
		String NumeroOrden = "b4bfbdad-ab21-4561-bd3f-3bf72d2d123d";
		//headers.set("tipo_empresa", TIPO_EMPRESA);
		headers.set("org_id", organizationId);
		headers.set("origen_datos", ORIGEN_DATOS);
		
		HttpEntity<String>entity = new HttpEntity<>(headers);
		
		ResponseEntity<Detalle> response = null;
		
		try {
			response = restTemplate.exchange(queryUri, HttpMethod.GET, entity,Detalle.class , NumeroOrden);
		}catch(Exception ex) {
			//LOGGER.error("", ex);
		}
		
		if(response!=null && response.getStatusCode()==HttpStatus.OK && response.getBody().getVersion()!=null) {
		
			return response.getBody().getVersion();
			
		}else {
			return 0;
		}
	}*/
	
	
	@KafkaListener(topics = "${events.topic.input.cambioestado}")
	public void listen_updateestado(String payload) {

		///////////////////////7
		Evento<Putestado> reqEvento = eventManagerService.deserializeEvent(payload, Putestado.class);
		
		if (reqEvento == null) {
			LOGGER.info("El evento no tiene el formato requerido por el microservicio");
			Evento<JsonNode> errorEvento = eventManagerService.deserializeEvent(payload, JsonNode.class);

			if (errorEvento != null) {
				LOGGER.info("event_id: " + errorEvento.getEvent_id());
				errorEvento = eventManagerService.initErrorEvent(errorEvento, "creacion", "El formato enviado es incorrecto");

				if (!productor.enviar(errorEvento.getData().getMeta().getTarget(),
						eventManagerService.serializeEvent(errorEvento))) {
					LOGGER.info("El evento de error no se pudo enviar");
				}

			} else {
				LOGGER.info("El objeto recibido no es un evento");
			}
			return;
		}
		////////////////////////
		
		LOGGER.info("Evento recibido\nevent_id: " + reqEvento.getEvent_id());

		Putestado est = reqEvento.getData().getPayload();
		//Datos de cabecera
		est.setPerfil(reqEvento.getData().getMeta().getTipo_empresa().toUpperCase());
		est.setIdorg(reqEvento.getData().getMeta().getOrg_id());
		LOGGER.info("PERFIL : " + est.getPerfil());
		LOGGER.info("ORGANIZACION ID  : " + est.getIdorg());
		LOGGER.info("Comentario: "+est.getComentario());
		int res=0;
		/////////////////////////////////////////////////////////////////////////////////////////////////
		if(est.getEstadoactual()==null || est.getEstadoactual()=="") {
			putestadoService.obtenerEstadoActual(est);
		}
		//Aqui poner el validador.
		/////////////////////////********************VALIDADOR****************////////////////////////////
			try {
				RestTemplate restTemplate = new RestTemplate();
				LOGGER.info("TEMPLATE URI ---->"+queryValidador);
				String recurso = new String(reqEvento.getData().getMeta().getResource());
				String perfil = new String(est.getPerfil().toLowerCase());
				String id = est.getIddoc().toString();
				String est_actual = new String(est.getEstadoactual());
				String accion = new String(est.getAccion());
				ResponseEntity<String> response = restTemplate.exchange(queryValidador, HttpMethod.GET, null, String.class,recurso,perfil,id,est_actual,accion);
				int statusCode = response.getStatusCodeValue();LOGGER.info("STATUS CODE ---->"+statusCode);
				String validacion = response.getBody();LOGGER.info("RESULT ---->"+validacion);
				
				if (response != null && statusCode == 200 && validacion.contains("valido-")) {
					if(reqEvento.getData().getMeta().getResource().equals("oc")) {
						est.setEstadoactual(validacion.substring(7));
						
						if (est.getPerfil().equals("C")) {
							res = putestadoService.actualizaroccomprador(est);
							if(est.getComentario() != null && est.getComentario() != ""){
								Comentario com = new Comentario();
								com.setIn_id(est.getIddoc());
								com.setVc_comentario(est.getComentario());
								com.setVc_num(est.getNumeroseguimiento());
								com.setIn_idorg(est.getIdorg());
								comentarioService.guardarcomentariocomprador(com);
							}
						}else if (est.getPerfil().equals("P")) {
							res = putestadoService.actualizarocproveedor(est);
							if(est.getComentario() != null && est.getComentario() != ""){
								Comentario com = new Comentario();
								com.setIn_id(est.getIddoc());
								com.setVc_comentario(est.getComentario());
								com.setVc_num(est.getNumeroseguimiento());
								com.setIn_idorg(est.getIdorg());
								comentarioService.guardarcomentarioproveedor(com);
								
							}
						}
					}
				}
			}catch (Exception e) {
				e.printStackTrace();
				LOGGER.error(e.getMessage());
			}
			
			/////////////////////////********************FIN VALIDADOR****************////////////////////////////
		
			////////////////////////
			if (res == 1) {
				
				Evento<SuccessPayload> resEvento = eventManagerService.createEmptyEvent(SuccessPayload.class);
				resEvento.getData().setMeta(reqEvento.getData().getMeta());

				resEvento = eventManagerService.initSuccessEvent(resEvento, "estadocambiado");

				//llenado de campos de la respuesta
				SuccessPayload successPayload = new SuccessPayload();
				successPayload.setId_doc(est.getIddoc());
				successPayload.setAccion(est.getAccion());
				successPayload.setNum_doc(est.getNumeroseguimiento());
				//successPayload.setNum_doc(putestadoService.getNroSeguimiento(successPayload.getId_doc()));
				OrdenCompra oc = null;//nuevo xc
				if(est.getIddoc()!=null){oc=crearOCService.buscar_OC(est.getIddoc());}//nuevo xc
				else{oc=crearOCService.buscar_OCxnumseg(reqEvento.getData().getMeta().getOrg_id(),est.getNumeroseguimiento());}//nuevo xc
				if(oc != null){
					if(oc.getIn_idorgcompra()!=null){successPayload.setId_orgcomprador(oc.getIn_idorgcompra());}
					if(oc.getIn_idorganizacionproveedora()!=null){successPayload.setId_orgproveedor(oc.getIn_idorganizacionproveedora());}
					if(oc.getIn_idusuariocomprador()!=null){successPayload.setId_usuariocompra(oc.getIn_idusuariocomprador());}
					if(oc.getIn_idusuarioproveedor()!=null){successPayload.setId_usuariovende(oc.getIn_idusuarioproveedor());}
					successPayload.setId_usuariocreadopor(oc.getId_usuariocreadopor());
					successPayload.setNombre_usuariocreadopor(oc.getNombre_usuariocreadopor());
				}
				successPayload.setMotivo(est.getComentario());

				resEvento.getData().setPayload(successPayload);

				if (!productor.enviar(resEvento.getData().getMeta().getTarget(),
						eventManagerService.serializeEvent(resEvento))) {
					LOGGER.info("El evento de actualizacion no se pudo enviar");
				}else{
					LOGGER.info("El evento se envio correctamente");
				}

			} else {
				Evento<Putestado> errorEvento = eventManagerService.initErrorEvent(reqEvento, "cambioestado", "Error en los datos proporcionados");

				LOGGER.info(errorEvento.getEvent_id() + " -- " + errorEvento.getData().getMeta().getTarget());

				if (!productor.enviar(errorEvento.getData().getMeta().getTarget(),
						eventManagerService.serializeEvent(errorEvento))) {
					LOGGER.info("El evento de error no se pudo enviar");
				}else{
					LOGGER.info("El evento de error se envio correctamente");
				}
				
			}
		}
	

	public Organiza listen_crearorganizacion(CEOrdenCompras ceOrdenCompras, Evento<CEOrdenCompras> crearOC) {
	
		String token = crearOC.getData().getMeta().getSession_id();
		Evento<CreaOrganizacion> resEvento = eventManagerService.createEmptyEvent(CreaOrganizacion.class);
		resEvento.getData().setMeta(crearOC.getData().getMeta());

		//resEvento = eventManagerService.initSuccessEvent(resEvento, "crearorganizacion");
		resEvento = eventManagerService.initCustomEvent(resEvento, "creacion-organizacion-solicitado", "organizacion", "t_crear_org");

		//llenado de campos de la respuesta
		CreaOrganizacion orgPayload = new CreaOrganizacion();
		LOGGER.info("RUC PROVEEDOR "+ceOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_rucproveedor());
		LOGGER.info("PROVEEDOR RAZON SOCIAL "+ceOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_razonsocialproveedor());
		orgPayload.setVc_ruc(ceOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_rucproveedor().substring(2));
		orgPayload.setVc_nombre(ceOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_razonsocialproveedor());
		orgPayload.setVc_direccion("-");
		orgPayload.setVc_email("-");
		orgPayload.setIn_codigousuariocreacion(UUID.randomUUID().toString());

		Organiza proveedor = null;
		ceOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_rucproveedor();
		resEvento.getData().setPayload(orgPayload);

		if (!productor.enviar(resEvento.getData().getMeta().getTarget(),
				eventManagerService.serializeEvent(resEvento))) {
			LOGGER.info("El evento de actualizacion no se pudo enviar");
		}
		
		for (int i=0; i<5;i++) {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//rest a org
			if (proveedor==null)
				proveedor=buscar_organizacion(ceOrdenCompras.getPouploadmq().getCompradorOrgID(), token,ceOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_rucproveedor(), ceOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_rucproveedor().substring(0, 2));
			else break;
		}
		
		
		return proveedor;
	} 
	
	public Usuario listen_crearusuario(CEOrdenCompras ceOrdenCompras, Evento<CEOrdenCompras> crearOC, UUID idorganizacionproveedora,Rol rol) {
		
		String token = crearOC.getData().getMeta().getSession_id();
		Evento<UsuarioEquivalencia> resEvento = eventManagerService.createEmptyEvent(UsuarioEquivalencia.class);
		resEvento.getData().setMeta(crearOC.getData().getMeta());
		
		LOGGER.info(rol.getRolId() +" rol id antes de crear usuario ");
		
		List<UUID> lista_roles = new ArrayList<>(); 
		lista_roles.add(rol.getRolId());

		//resEvento = eventManagerService.initSuccessEvent(resEvento, "crearorganizacion");
		resEvento = eventManagerService.initCustomEvent(resEvento, "creacion-usuario-solicitado", "usuario", "t_crear_usuario");

		//llenado de campos de la respuesta
		UsuarioEquivalencia usuarioPayload = new UsuarioEquivalencia();
		Usuario usuario= new Usuario();
		LOGGER.info("RUC USUARIO PROVEEDOR "+ceOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_rucproveedor());
		usuarioPayload.setAp_paterno(administrador);
		usuarioPayload.setDireccion("-");
		usuarioPayload.setTelefono("-");
		usuarioPayload.setEmail("soluciones@ebizlatin.com");
		usuarioPayload.setNombre_usuario(ceOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_rucproveedor());
		usuarioPayload.setVc_password(ceOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_rucproveedor());
		usuarioPayload.setCodigoUsuarioCreacion(UUID.randomUUID());
		usuarioPayload.setFechaHoraCreacion("2017-11-21T23:59:59-05:00");
		usuarioPayload.setHabilitado(1);
		usuarioPayload.setTitulo("Sr.");
		usuarioPayload.setListaIdRoles(lista_roles);
		usuarioPayload.setIdOrganizacion(idorganizacionproveedora);
		
		ceOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_rucproveedor();
		resEvento.getData().setPayload(usuarioPayload);

		if (!productor.enviar(resEvento.getData().getMeta().getTarget(),
				eventManagerService.serializeEvent(resEvento))) {
			LOGGER.info("El evento de actualizacion no se pudo enviar");
		}
		
		for (int i=0; i<5;i++) {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//rest a org
			if (usuario==null) {
				usuario=buscar_usuario_adm(idorganizacionproveedora, token, "2");
			}else break;
			
		}
		
		
		return usuario;
	} 
	
				/*modificar Evento<Putestado> errorEvento = eventManagerService.initErrorEvent(reqEvento, "cambioestado", "Error en los datos proporcionados");

				LOGGER.info(errorEvento.getEvent_id() + " -- " + errorEvento.getData().getMeta().getTarget());

				if (!productor.enviar(errorEvento.getData().getMeta().getTarget(),
						eventManagerService.serializeEvent(errorEvento))) {
					LOGGER.info("El evento de error no se pudo enviar");
				}
				
		
		}
	/*public void buscar_organizacion(String payload) {

		///////////////////////7
		Evento<Putestado> reqEvento = eventManagerService.deserializeEvent(payload, Putestado.class);
		
		if (reqEvento == null) {
			LOGGER.info("El evento no tiene el formato requerido por el microservicio");
			Evento<JsonNode> errorEvento = eventManagerService.deserializeEvent(payload, JsonNode.class);

			if (errorEvento != null) {
				LOGGER.info("event_id: " + errorEvento.getEvent_id());
				errorEvento = eventManagerService.initErrorEvent(errorEvento, "creacion", "El formato enviado es incorrecto");

				if (!productor.enviar(errorEvento.getData().getMeta().getTarget(),
						eventManagerService.serializeEvent(errorEvento))) {
					LOGGER.info("El evento de error no se pudo enviar");
				}

			} else {
				LOGGER.info("El objeto recibido no es un evento");
			}
			return;
		}
		////////////////////////
		
		LOGGER.info("Evento recibido\nevent_id: " + reqEvento.getEvent_id());

		Putestado est = reqEvento.getData().getPayload();
		//Datos de cabecera
		est.setPerfil(reqEvento.getData().getMeta().getTipo_empresa().toUpperCase());
		est.setIdorg(reqEvento.getData().getMeta().getOrg_id());
		LOGGER.info("PERFIL : " + est.getPerfil());
		LOGGER.info("ORGANIZACION ID  : " + est.getIdorg());
		int res=0;
		/////////////////////////////////////////////////////////////////////////////////////////////////
		if(est.getEstadoactual()==null || est.getEstadoactual()=="") {
			putestadoService.obtenerEstadoActual(est);
		}
		//Aqui poner el validador.
		/////////////////////////********************VALIDADOR****************////////////////////////////
		/*	try {
				RestTemplate restTemplate = new RestTemplate();
				String uri = "http://b2miningdata.com/api/mssm/v1/validacion/?recurso="+reqEvento.getData().getMeta().getResource()+"&perfil="+est.getPerfil().toLowerCase()+"&id="+est.getIddoc()+"&estadoactual="+est.getEstadoactual()+"&accion="+est.getAccion();
				LOGGER.info("TEMPLATE URI ---->"+uri);
				ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.GET, null, String.class);
				int statusCode = response.getStatusCodeValue();LOGGER.info("STATUS CODE ---->"+statusCode);
				String validacion = response.getBody();LOGGER.info("RESULT ---->"+validacion);
				
				if (response != null && statusCode == 200 && validacion.contains("valido-")) {
					if(reqEvento.getData().getMeta().getResource().equals("oc")) {
						est.setEstadoactual(validacion.substring(7));
						
						if (est.getPerfil().equals("C")) {
							res = putestadoService.actualizaroccomprador(est);
							if(est.getComentario() != null && est.getComentario() != ""){
								Comentario com = new Comentario();
								com.setIn_id(est.getIddoc());
								com.setVc_comentario(est.getComentario());
								com.setVc_num(est.getNumeroseguimiento());
								com.setIn_idorg(est.getIdorg());
								comentarioService.guardarcomentariocomprador(com);
							}
						}else if (est.getPerfil().equals("P")) {
							res = putestadoService.actualizarocproveedor(est);
							if(est.getComentario() != null && est.getComentario() != ""){
								Comentario com = new Comentario();
								com.setIn_id(est.getIddoc());
								com.setVc_comentario(est.getComentario());
								com.setVc_num(est.getNumeroseguimiento());
								com.setIn_idorg(est.getIdorg());
								comentarioService.guardarcomentarioproveedor(com);
								
							}
						}
					}
				}
			}catch (Exception e) {
				e.printStackTrace();
				LOGGER.error(e.getMessage());
			}
			
			/////////////////////////********************FIN VALIDADOR****************////////////////////////////
		
			////////////////////////
			/*if (res == 1) {
				
				Evento<SuccessPayload> resEvento = eventManagerService.createEmptyEvent(SuccessPayload.class);
				resEvento.getData().setMeta(reqEvento.getData().getMeta());

				resEvento = eventManagerService.initSuccessEvent(resEvento, "estadocambiado");

				//llenado de campos de la respuesta
				SuccessPayload successPayload = new SuccessPayload();
				successPayload.setId_doc(est.getIddoc());
				successPayload.setAccion(est.getAccion());
				//successPayload.setNum_doc(putestadoService.getNroSeguimiento(successPayload.getId_doc()));

				resEvento.getData().setPayload(successPayload);

				if (!productor.enviar(resEvento.getData().getMeta().getTarget(),
						eventManagerService.serializeEvent(resEvento))) {
					LOGGER.info("El evento de actualizacion no se pudo enviar");
				}

			} else {
				Evento<Putestado> errorEvento = eventManagerService.initErrorEvent(reqEvento, "cambioestado", "Error en los datos proporcionados");

				LOGGER.info(errorEvento.getEvent_id() + " -- " + errorEvento.getData().getMeta().getTarget());

				if (!productor.enviar(errorEvento.getData().getMeta().getTarget(),
						eventManagerService.serializeEvent(errorEvento))) {
					LOGGER.info("El evento de error no se pudo enviar");
				}
				
			}
		}*/
	
	@Autowired
	private RestTemplate restTemplate1;

	@Value("${api.oc.client.query-uri}")
	private String queryUri1;
	
	@Value("${api.oc.client.query_validador}")
	private String queryValidador;
	
	@Value("${api.oc.client.query-sec}")
	private String querySec;
	
	private static final String STATUS_CODE="0000";
	private final static String TIPO_EMPRESA="C";
	private final static String ORIGEN_DATOS="PEB2M";

	public Organiza buscar_organizacion(String organizationId, String tokenBearer,String RUC, String pais) {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", String.format("Bearer %s",tokenBearer));
		
		LOGGER.info("organizationId "+organizationId);
		
		headers.set("tipo_empresa", TIPO_EMPRESA);
		headers.set("org_id", organizationId);
		headers.set("ORIGEN_DATOS", ORIGEN_DATOS);
		
		HttpEntity<String>entity = new HttpEntity<>(headers);
		
		ResponseEntity<OrdenListServiceResponse> response = null;
		
		String parametroRUC = RUC.substring(2);
		
		try {
			response = restTemplate1.exchange(queryUri1, HttpMethod.GET, entity,OrdenListServiceResponse.class , parametroRUC, pais);
		}catch(Exception ex) {
			LOGGER.error("", ex);
		}
		
		if (response!=null)  {
				
			LOGGER.info("response.getBody() buscar organizacion "+response.getBody());
			
			if (response.getBody()!=null) {
				LOGGER.info("response.getBody().getData() getdata"+response.getBody().getData());
			}
		}
		
		if(response!=null && response.getStatusCode()==HttpStatus.OK && STATUS_CODE.compareTo(response.getBody().getStatuscode())==0 && !response.getBody().getData().isEmpty()) {
		
			return response.getBody().getData().get(0);
			
		}else {
			return null;
		}
	}
	
	@Autowired
	private RestTemplate restTemplate2;

	@Value("${api.oc.client.query-adm}")
	private String queryUri2;
	
	/*private static final String STATUS_CODE="0000";
	private final static String TIPO_EMPRESA="C";
	private final static String ORIGEN_DATOS="PEB2M";*/
	
	//@Override
	public Usuario buscar_usuario_adm(UUID organizationId, String tokenBearer, String tipo_usuario) {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", String.format("Bearer %s",tokenBearer));
		
		//headers.set("tipo_empresa", TIPO_EMPRESA);
		headers.set("tipo_usuario", tipo_usuario);
		//headers.set("id", organizationId);
		//headers.set("ORIGEN_DATOS", ORIGEN_DATOS);
		
		HttpEntity<String>entity = new HttpEntity<>(headers);
		
		ResponseEntity<JsonNode> response = null;
		
		//String parametroRUC = RUC.substring(2);
		
		LOGGER.info("tipo_usuario "+tipo_usuario );
		LOGGER.info("organizacion id "+organizationId);
		
		try {
			response = restTemplate2.exchange(queryUri2, HttpMethod.GET, entity,JsonNode.class , organizationId);
		}catch(Exception ex) {
			LOGGER.error("", ex);
		}
		
		if(response!=null && response.getStatusCode()==HttpStatus.OK && response.getBody()!=null) {
			LOGGER.info("responseGetBody buscar usuario administrador "+response.getBody() );
			if (response.getBody()!=null && response.getBody().get(0)!=null) {
				try {
					return mapper.treeToValue(response.getBody().get(0), Usuario.class);
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				return null;
			}
			return null;
		}else {
			return null;
		}
	}
	
	@Autowired
	private RestTemplate restTemplate3;

	@Value("${api.oc.client.query-rol}")
	private String queryUri3;
	
	/*private static final String STATUS_CODE="0000";
	private final static String TIPO_EMPRESA="C";
	private final static String ORIGEN_DATOS="PEB2M";*/

	public Rol buscar_rol(String tokenBearer,String vc_rol) {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", String.format("Bearer %s",tokenBearer));
		
		/*headers.set("tipo_empresa", TIPO_EMPRESA);
		headers.set("org_id", organizationId);
		headers.set("ORIGEN_DATOS", ORIGEN_DATOS);*/
		
		HttpEntity<String>entity = new HttpEntity<>(headers);
		
		ResponseEntity<JsonNode> response = null;
		
		LOGGER.info("vc_rol "+vc_rol );
		
		try {
			response = restTemplate3.exchange(queryUri3, HttpMethod.GET, entity,JsonNode.class , vc_rol);
		}catch(Exception ex) {
			LOGGER.error("", ex);
		}
		
		if(response!=null && response.getStatusCode()==HttpStatus.OK && response.getBody()!=null) {
			LOGGER.info("responseGetBody rol "+response.getBody() );
			try {
				return mapper.treeToValue(response.getBody(), Rol.class);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}else {
			return null;
		}
	}
	
	public EquivalenciaOrg listen_crearequivalenciaorganizacion(CEOrdenCompras ceOrdenCompras, Evento<CEOrdenCompras> crearOC,UUID idorgcompradora, UUID idorgproveedora) {
		
		String token = crearOC.getData().getMeta().getSession_id();
		Evento<EquivalenciaOrg> resEvento = eventManagerService.createEmptyEvent(EquivalenciaOrg.class);
		resEvento.getData().setMeta(crearOC.getData().getMeta());

		//resEvento = eventManagerService.initSuccessEvent(resEvento, "crearorganizacion");
		resEvento = eventManagerService.initCustomEvent(resEvento, "creacion-equivalencia-solicitado", "organizacion", "t_crear_equivalencia");

		//llenado de campos de la respuesta
		EquivalenciaOrg equiorgPayload = new EquivalenciaOrg();
		LOGGER.info("RUC PROVEEDOR PARA EQUIVALENCIA "+ceOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_rucproveedor());
		LOGGER.info("PROVEEDOR RAZON SOCIAL PARA EQUIVALENCIA "+ceOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_razonsocialproveedor());
		equiorgPayload.setCodigoInterno(ceOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_codinternoprov());
		equiorgPayload.setIdOrganizacionOrigen(idorgcompradora.toString());
		equiorgPayload.setIdOrganizacionDestino(idorgproveedora.toString());

		EquivalenciaOrg equi = null;
		ceOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_rucproveedor();
		resEvento.getData().setPayload(equiorgPayload);

		if (!productor.enviar(resEvento.getData().getMeta().getTarget(),
				eventManagerService.serializeEvent(resEvento))) {
			LOGGER.info("El evento de actualizacion no se pudo enviar");
		}
		
		for (int i=0; i<5;i++) {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//rest a org
			if (equi==null)
				equi=buscar_equivalencia( token, idorgcompradora, idorgproveedora, ceOrdenCompras.getPouploadmq().getLista_oc().get(0).getVc_codinternoprov() );
			else break;
		}
		
		
		return equi;
	} 

	@Autowired
	private RestTemplate restTemplate5;

	@Value("${api.oc.client.query-equi}")
	private String queryUri5;
	
	/*private static final String STATUS_CODE="0000";
	private final static String TIPO_EMPRESA="C";
	private final static String ORIGEN_DATOS="PEB2M";*/

	public EquivalenciaOrg buscar_equivalencia(String tokenBearer,UUID idorgcompradora, UUID idorgproveedora, String codigo_interno) {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", String.format("Bearer %s",tokenBearer));
		
		/*headers.set("tipo_empresa", TIPO_EMPRESA);
		headers.set("org_id", organizationId);
		headers.set("ORIGEN_DATOS", ORIGEN_DATOS);*/
		
		HttpEntity<String>entity = new HttpEntity<>(headers);
		
		ResponseEntity<JsonNode> response = null;
		
		LOGGER.info("codigo interno o equivalencia "+codigo_interno );
		
		try {
			response = restTemplate5.exchange(queryUri3, HttpMethod.GET, entity,JsonNode.class , idorgcompradora, idorgproveedora, codigo_interno  );
		}catch(Exception ex) {
			LOGGER.error("", ex);
		}
		
		if(response!=null && response.getStatusCode()==HttpStatus.OK && response.getBody()!=null) {
			LOGGER.info("responseGetBody equivalencia "+response.getBody() );
			try {
				return mapper.treeToValue(response.getBody(), EquivalenciaOrg.class);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}else {
			return null;
		}
	}
	
	@Autowired
	private RestTemplate restTemplate4;

	@Value("${api.oc.client.query-usuario}")
	private String queryUri4;
	
	/*private static final String STATUS_CODE="0000";
	private final static String TIPO_EMPRESA="C";
	private final static String ORIGEN_DATOS="PEB2M";*/
	
	//@Override
	public UsuarioEquivalencia equivalenciaUsuario(UUID organizationId, String tokenBearer, String equivalencia) {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", String.format("Bearer %s",tokenBearer));
		
		HttpEntity<String>entity = new HttpEntity<>(headers);
		
		ResponseEntity<UsuarioEquivalencia> response = null;
		
		try {
			response = restTemplate4.exchange(queryUri4, HttpMethod.GET, entity,UsuarioEquivalencia.class , organizationId, equivalencia );
		}catch(Exception ex) {
			LOGGER.error("", ex);
		}
		
		if(response!=null && response.getStatusCode()==HttpStatus.OK) {
		
			return response.getBody();
			
		}else {
			return null;
		}
	}
	
	/*@Autowired
	private RestTemplate restTemplate6;

	@Value("${api.oc.client.query-sec}")
	private String queryUri6;

	public UUID getUserId(String oauthToken) {
		LOGGER.info("LLamando a mssecurity...");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", String.format(oauthToken));

		HttpEntity<String> entity = new HttpEntity<>(headers);

		ResponseEntity<JsonNode> response = null;

		try {
			response = restTemplate6.exchange(queryUri6, HttpMethod.GET, entity, JsonNode.class);
		} catch (Exception ex) {
			LOGGER.error("Fallo en la llamada a mssecurity", ex);
			return null;
		}

		LOGGER.info("Respuesta:\n{}", response.getBody());

		UUID user_id = null;

		if (response != null && response.getStatusCode() == HttpStatus.OK) {
			try {
				String id = response.getBody().get("userAuthentication").get("principal").get("user").get("id")
						.asText();
				user_id = UUID.fromString(id);
				LOGGER.info("Usuario autenticado con id: {}", user_id);
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
			}
			return user_id;
		} else {
			LOGGER.error("Error en la respuesta de ms-mssecurity");
		}

		return null;
	}*/
	
	@Autowired
	private RestTemplate restTemplate;
	
	public UUID getUserId(String oauthToken) {
		LOGGER.info("LLamando a mssecurity...");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", String.format("Bearer %s",oauthToken));

		HttpEntity<String> entity = new HttpEntity<>(headers);

		ResponseEntity<JsonNode> response = null;

		try {
			response = restTemplate.exchange(querySec, HttpMethod.GET, entity, JsonNode.class);
		} catch (Exception ex) {
			LOGGER.error("Fallo en la llamada a mssecurity", ex);
			return null;
		}

		LOGGER.info("Respuesta:\n{}", response.getBody());

		UUID user_id = null;

		if (response != null && response.getStatusCode() == HttpStatus.OK) {
			try {
				String id = response.getBody().get("userAuthentication").get("principal").get("user").get("id")
						.asText();
				user_id = UUID.fromString(id);
				LOGGER.info("Usuario autenticado con id: {}", user_id);
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
			}
			return user_id;
		} else {
			LOGGER.error("Error en la respuesta de ms-mssecurity");
		}

		return null;
	}


}
