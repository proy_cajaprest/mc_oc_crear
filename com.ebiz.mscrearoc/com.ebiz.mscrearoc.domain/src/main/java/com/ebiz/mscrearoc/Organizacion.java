package com.ebiz.mscrearoc;


import java.util.UUID;


public class Organizacion {

    private String vc_ruc;

	private UUID in_idorganizacion;

	public String getVc_ruc() {
		return vc_ruc;
	}

	public void setVc_ruc(String vc_ruc) {
		this.vc_ruc = vc_ruc;
	}

	public UUID getIn_idorganizacion() {
		return in_idorganizacion;
	}

	public void setIn_idorganizacion(UUID in_idorganizacion) {
		this.in_idorganizacion = in_idorganizacion;
	}
	
	
    
}
