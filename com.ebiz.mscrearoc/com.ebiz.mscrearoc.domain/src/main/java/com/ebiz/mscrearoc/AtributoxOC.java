package com.ebiz.mscrearoc;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;


public class AtributoxOC {
	
	@JsonProperty("vc_nombreatributo")
	private String vc_nombreatributo;
	
	@JsonProperty("in_idatributo")
	private UUID in_idatributo;
	
	@JsonProperty("vc_valorenviado")
	private String vc_valorenviado;

	@JsonProperty("vc_mandatorio")
	private String vc_mandatorio;
	
	@JsonProperty("vc_modificable")
	private String vc_modificable;//-Modificable
	
	@JsonProperty("in_idunidad ")
	private int in_idunidad ;
	
	@JsonProperty("in_codigousuariocreacion")
	private int in_codigousuariocreacion;
	
	@JsonProperty("in_habilitado")
	private int in_habilitado;

	//////////////////////////////////////////////////
	
	public String getVc_nombreatributo() {
		return vc_nombreatributo;
	}

	public void setVc_nombreatributo(String vc_nombreatributo) {
		this.vc_nombreatributo = vc_nombreatributo;
	}

	public UUID getIn_idatributo() {
		return in_idatributo;
	}

	public void setIn_idatributo(UUID in_idatributo) {
		this.in_idatributo = in_idatributo;
	}
	
	public String getVc_valorenviado() {
		return vc_valorenviado;
	}

	public void setVc_valorenviado(String vc_valorenviado) {
		this.vc_valorenviado = vc_valorenviado;
	}

	public String getVc_mandatorio() {
		return vc_mandatorio;
	}

	public void setVc_mandatorio(String vc_mandatorio) {
		this.vc_mandatorio = vc_mandatorio;
	}

	public String getVc_modificable() {
		return vc_modificable;
	}

	public void setVc_modificable(String vc_modificable) {
		this.vc_modificable = vc_modificable;
	}

	public int getIn_idunidad() {
		return in_idunidad;
	}

	public void setIn_idunidad(int in_idunidad) {
		this.in_idunidad = in_idunidad;
	}

	public int getIn_codigousuariocreacion() {
		return in_codigousuariocreacion;
	}

	public void setIn_codigousuariocreacion(int in_codigousuariocreacion) {
		this.in_codigousuariocreacion = in_codigousuariocreacion;
	}

	public int getIn_habilitado() {
		return in_habilitado;
	}

	public void setIn_habilitado(int in_habilitado) {
		this.in_habilitado = in_habilitado;
	}
	
	
	////////////////////////////////////////
	
}
