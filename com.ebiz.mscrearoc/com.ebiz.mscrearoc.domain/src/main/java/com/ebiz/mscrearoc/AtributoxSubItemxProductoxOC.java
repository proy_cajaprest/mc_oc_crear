package com.ebiz.mscrearoc;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.sql.Timestamp;
import java.util.UUID;

public class AtributoxSubItemxProductoxOC {
	
	@JsonProperty("in_idatributoxsubitemxprodxoc")
	private UUID in_idatributoxsubitemxprodxoc;
	
	@JsonProperty("ValorAtributoSubProducto")
	private String vc_valorenviado;//ValorAtributoSubProducto
	
	@JsonProperty("-Obligatorio")
	private String vc_mandatorio;//-Obligatorio
	
	@JsonProperty("-Modificable")
	private String vc_modificable;//-Modificable
	
	private Integer in_idunidad ;//UnidadAtributoSubProducto
	
	@JsonProperty("UnidadAtributoSubProducto")
	private String vc_nombreunidad;
	
	@JsonProperty("vc_unidaderror")
	private String vc_unidaderror;
	
	@JsonProperty("OperadorAtributoSubProducto")
	private String vc_operador;//OperadorAtributoSubProducto
	
	@JsonProperty("in_flagcodificado")
	private Integer in_flagcodificado ;
	
	@JsonProperty("vc_valorrealorg")
	private String vc_valorrealorg;
	
	@JsonProperty("-Nombre")
	private String vc_nombreatributo ;//-Nombre
	
	@JsonProperty("vc_campo1")
	private String vc_campo1 ;
	
	@JsonProperty("vc_campo2")
	private String vc_campo2 ;
	
	@JsonProperty("vc_campo3")
	private String vc_campo3 ;
	
	@JsonProperty("vc_campo4")
	private String vc_campo4 ;
	
	@JsonProperty("vc_campo5")
	private String vc_campo5 ;
	
	@JsonProperty("in_codigousuariocreacion")
	private Integer in_codigousuariocreacion ;
	
	@JsonProperty("in_codigousuariomodificacion")
	private Integer in_codigousuariomodificacion ;
	
	@JsonProperty("ts_fechahoracreacion")
	private Timestamp ts_fechahoracreacion;
	
	@JsonProperty("ts_fechahoramodificacion")
	private Timestamp ts_fechahoramodificacion;
	
	@JsonProperty("in_habilitado")
	private Integer in_habilitado ;
	
	private String vc_idtablaoperador;
	
	private String vc_idregistrooperador;
	
	private String vc_idtablaunidad;
	
	private String vc_idregistrounidad;
	
//////////////////////////////////

	public UUID getIn_idatributoxsubitemxprodxoc() {
		return in_idatributoxsubitemxprodxoc;
	}

	public void setIn_idatributoxsubitemxprodxoc(UUID in_idatributoxsubitemxprodxoc) {
		this.in_idatributoxsubitemxprodxoc = in_idatributoxsubitemxprodxoc;
	}

	public String getVc_valorenviado() {
		return vc_valorenviado;
	}

	public void setVc_valorenviado(String vc_valorenviado) {
		this.vc_valorenviado = vc_valorenviado;
	}

	public String getVc_mandatorio() {
		return vc_mandatorio;
	}

	public void setVc_mandatorio(String vc_mandatorio) {
		this.vc_mandatorio = vc_mandatorio;
	}

	public String getVc_modificable() {
		return vc_modificable;
	}

	public void setVc_modificable(String vc_modificable) {
		this.vc_modificable = vc_modificable;
	}

	public String getVc_nombreunidad() {
		return vc_nombreunidad;
	}

	public void setVc_nombreunidad(String vc_nombreunidad) {
		this.vc_nombreunidad = vc_nombreunidad;
	}

	public String getVc_unidaderror() {
		return vc_unidaderror;
	}

	public void setVc_unidaderror(String vc_unidaderror) {
		this.vc_unidaderror = vc_unidaderror;
	}

	public Integer getIn_flagcodificado() {
		return in_flagcodificado;
	}

	public void setIn_flagcodificado(Integer in_flagcodificado) {
		this.in_flagcodificado = in_flagcodificado;
	}

	public String getVc_valorrealorg() {
		return vc_valorrealorg;
	}

	public void setVc_valorrealorg(String vc_valorrealorg) {
		this.vc_valorrealorg = vc_valorrealorg;
	}

	public String getVc_nombreatributo() {
		return vc_nombreatributo;
	}

	public void setVc_nombreatributo(String vc_nombreatributo) {
		this.vc_nombreatributo = vc_nombreatributo;
	}

	public String getVc_campo1() {
		return vc_campo1;
	}

	public void setVc_campo1(String vc_campo1) {
		this.vc_campo1 = vc_campo1;
	}

	public String getVc_campo2() {
		return vc_campo2;
	}

	public void setVc_campo2(String vc_campo2) {
		this.vc_campo2 = vc_campo2;
	}

	public String getVc_campo3() {
		return vc_campo3;
	}

	public void setVc_campo3(String vc_campo3) {
		this.vc_campo3 = vc_campo3;
	}

	public String getVc_campo4() {
		return vc_campo4;
	}

	public void setVc_campo4(String vc_campo4) {
		this.vc_campo4 = vc_campo4;
	}

	public String getVc_campo5() {
		return vc_campo5;
	}

	public void setVc_campo5(String vc_campo5) {
		this.vc_campo5 = vc_campo5;
	}

	public Integer getIn_codigousuariocreacion() {
		return in_codigousuariocreacion;
	}

	public void setIn_codigousuariocreacion(Integer in_codigousuariocreacion) {
		this.in_codigousuariocreacion = in_codigousuariocreacion;
	}

	public Integer getIn_codigousuariomodificacion() {
		return in_codigousuariomodificacion;
	}

	public void setIn_codigousuariomodificacion(Integer in_codigousuariomodificacion) {
		this.in_codigousuariomodificacion = in_codigousuariomodificacion;
	}

	public Timestamp getTs_fechahoracreacion() {
		return ts_fechahoracreacion;
	}

	public void setTs_fechahoracreacion(Timestamp ts_fechahoracreacion) {
		this.ts_fechahoracreacion = ts_fechahoracreacion;
	}

	public Timestamp getTs_fechahoramodificacion() {
		return ts_fechahoramodificacion;
	}

	public void setTs_fechahoramodificacion(Timestamp ts_fechahoramodificacion) {
		this.ts_fechahoramodificacion = ts_fechahoramodificacion;
	}

	public Integer getIn_habilitado() {
		return in_habilitado;
	}

	public void setIn_habilitado(Integer in_habilitado) {
		this.in_habilitado = in_habilitado;
	}

	public Integer getIn_idunidad() {
		return in_idunidad;
	}

	public void setIn_idunidad(Integer in_idunidad) {
		this.in_idunidad = in_idunidad;
	}

	public String getVc_operador() {
		return vc_operador;
	}

	public void setVc_operador(String vc_operador) {
		this.vc_operador = vc_operador;
	}

	public String getVc_idtablaoperador() {
		return vc_idtablaoperador;
	}

	public void setVc_idtablaoperador(String vc_idtablaoperador) {
		this.vc_idtablaoperador = vc_idtablaoperador;
	}

	public String getVc_idregistrooperador() {
		return vc_idregistrooperador;
	}

	public void setVc_idregistrooperador(String vc_idregistrooperador) {
		this.vc_idregistrooperador = vc_idregistrooperador;
	}

	public String getVc_idtablaunidad() {
		return vc_idtablaunidad;
	}

	public void setVc_idtablaunidad(String vc_idtablaunidad) {
		this.vc_idtablaunidad = vc_idtablaunidad;
	}

	public String getVc_idregistrounidad() {
		return vc_idregistrounidad;
	}

	public void setVc_idregistrounidad(String vc_idregistrounidad) {
		this.vc_idregistrounidad = vc_idregistrounidad;
	}

	
	//////////////////////////////////////
}
