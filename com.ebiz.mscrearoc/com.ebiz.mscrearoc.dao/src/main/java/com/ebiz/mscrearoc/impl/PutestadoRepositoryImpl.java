package com.ebiz.mscrearoc.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ebiz.mscrearoc.PutestadoRepository;
import com.ebiz.mscrearoc.OrdenCompra;
import com.ebiz.mscrearoc.Putestado;

//import org.springframework.jdbc.support.KeyHolder;
//import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class PutestadoRepositoryImpl implements PutestadoRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Transactional(rollbackFor = Exception.class)
	
	public int actualizaroccomprador(Putestado putestado) {
		String sql = "";
		int i = 0;
		try {
			if (putestado.getNumeroseguimiento()!=null && putestado.getNumeroseguimiento()!="") {
			if (putestado.getAccion().toUpperCase().equals("ANULAR")) {
				sql = "UPDATE oc.t_oc T1 set vc_estadocomprador = ? ,vc_estadoproveedor = ? where in_version = (SELECT MAX(T2.IN_VERSION) FROM oc.t_oc T2 WHERE T1.ch_numeroseguimiento = T2.ch_numeroseguimiento) and T1.ch_numeroseguimiento = ? and T1.in_idorganizacioncompradora = ?";
				i = jdbcTemplate.update(sql, new Object[] { putestado.getEstadoactual(), putestado.getEstadoactual(), putestado.getNumeroseguimiento() ,putestado.getIdorg()});
				}else{
				sql = "UPDATE oc.t_oc T1 set vc_estadocomprador = ? where in_version = (SELECT MAX(T2.IN_VERSION) FROM oc.t_oc T2 WHERE T1.ch_numeroseguimiento = T2.ch_numeroseguimiento) and T1.ch_numeroseguimiento = ? and T1.in_idorganizacioncompradora = ?";
				i = jdbcTemplate.update(sql, new Object[] { putestado.getEstadoactual(), putestado.getNumeroseguimiento() ,putestado.getIdorg()});
				}
			}else {
			if (putestado.getAccion().toUpperCase().equals("ANULAR")) {
				sql = "update oc.t_oc set vc_estadocomprador = ? ,vc_estadoproveedor = ? where in_idoc = ? and in_idorganizacioncompradora = ?";
				i = jdbcTemplate.update(sql, new Object[] { putestado.getEstadoactual(), putestado.getEstadoactual(), putestado.getIddoc() ,putestado.getIdorg()});
				}else{
				sql = "update oc.t_oc set vc_estadocomprador = ? where in_idoc = ? and in_idorganizacioncompradora = ?";
				i = jdbcTemplate.update(sql, new Object[] { putestado.getEstadoactual(), putestado.getIddoc() ,putestado.getIdorg()});
				}
			}
			System.out.println("SQL ACTUALIZAR ESTADO COMPRADOR: " + sql);
			System.out.println("FILAS AFECTADAS POR EL UPDATE : " + i);
		}
		catch(EmptyResultDataAccessException e) {
			e.printStackTrace();
			throw e;
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("OCURRIO UN ERROR EN DAO : " + e.getMessage().toString());
			i = 0;
		}
		return i;
	}

	public int actualizarocproveedor(Putestado putestado) {
		String sql = "";
		int i = 0;
		try {
			if (putestado.getNumeroseguimiento()!=null && putestado.getNumeroseguimiento()!="") {
				if (putestado.getAccion().toUpperCase().equals("ACEPTAR")||putestado.getAccion().toUpperCase().equals("RECHAZAR")||putestado.getAccion().toUpperCase().equals("VISUALIZAR")||putestado.getAccion().toUpperCase().equals("ANULAR")) {
					sql = "UPDATE oc.t_oc T1 set vc_estadoproveedor = ?,vc_estadocomprador = ? where in_version = (SELECT MAX(T2.IN_VERSION) FROM oc.t_oc T2 WHERE T1.ch_numeroseguimiento = T2.ch_numeroseguimiento) and T1.ch_numeroseguimiento = ? and T1.in_idorganizacionproveedora = ?";
					i = jdbcTemplate.update(sql, new Object[] { putestado.getEstadoactual(),putestado.getEstadoactual(), putestado.getNumeroseguimiento() ,putestado.getIdorg()});
				}else {
					sql = "UPDATE oc.t_oc T1 set vc_estadoproveedor = ? where in_version = (SELECT MAX(T2.IN_VERSION) FROM oc.t_oc T2 WHERE T1.ch_numeroseguimiento = T2.ch_numeroseguimiento) and T1.ch_numeroseguimiento = ? and T1.in_idorganizacionproveedora = ?";
					i = jdbcTemplate.update(sql, new Object[] { putestado.getEstadoactual(), putestado.getNumeroseguimiento() ,putestado.getIdorg()});
				}
			}else {
				if (putestado.getAccion().toUpperCase().equals("ACEPTAR")||putestado.getAccion().toUpperCase().equals("RECHAZAR")||putestado.getAccion().toUpperCase().equals("VISUALIZAR")||putestado.getAccion().toUpperCase().equals("ANULAR")) {
					sql = "update oc.t_oc set vc_estadoproveedor = ?,vc_estadocomprador = ? where in_idoc = ? and in_idorganizacionproveedora = ?";
					i = jdbcTemplate.update(sql, new Object[] { putestado.getEstadoactual(),putestado.getEstadoactual(), putestado.getIddoc() ,putestado.getIdorg()});
				}else {
					sql = "update oc.t_oc set vc_estadoproveedor = ? where in_idoc = ? and in_idorganizacionproveedora = ?";
					i = jdbcTemplate.update(sql, new Object[] { putestado.getEstadoactual(), putestado.getIddoc() ,putestado.getIdorg()});
				}
			}
			System.out.println("SQL SQL ACTUALIZAR ESTADO PROVEEDOR: " + sql);
			System.out.println("FILAS AFECTADAS POR EL UPDATE : " + i);
		}
		catch(EmptyResultDataAccessException e) {
			e.printStackTrace();
			throw e;
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("OCURRIO UN ERROR EN DAO : " + e.getMessage().toString());
			i = 0;
		}
		return i;
	}
	
	public int obtenerEstadoActual(Putestado putestado) {
		int i = 0;
		String sql = "";
		List<OrdenCompra> l_oc = null;
		try {
			if (putestado.getNumeroseguimiento()!=null && putestado.getNumeroseguimiento()!="") {
				
				sql = "SELECT DISTINCT in_version,ch_numeroseguimiento,vc_estadoproveedor,vc_estadocomprador from oc.t_oc T1 where in_version = (SELECT MAX(T2.IN_VERSION) FROM oc.t_oc T2 WHERE T1.ch_numeroseguimiento = T2.ch_numeroseguimiento) AND T1.ch_numeroseguimiento = ? ";
				if (putestado.getPerfil().equals("P"))
					sql += "and T1.in_idorganizacionproveedora = ?";
				else
					sql += "and T1.in_idorganizacioncompradora= ?";
				l_oc = jdbcTemplate.query(sql,new Object[] { putestado.getNumeroseguimiento() ,putestado.getIdorg()}, this::mapParam_estados);
			}else {
				sql = "SELECT vc_estadoproveedor, vc_estadocomprador from oc.t_oc where in_idoc = ? ";
				if (putestado.getPerfil().equals("P"))
					sql += "and in_idorganizacionproveedora = ?";
				else
					sql += "and in_idorganizacioncompradora= ?";
				l_oc = jdbcTemplate.query(sql,new Object[] { putestado.getIddoc() ,putestado.getIdorg()}, this::mapParam_estados);
			}
			
			if (l_oc != null && l_oc.size()>0) {
				if (putestado.getPerfil().equals("P")) {
					putestado.setEstadoactual(l_oc.get(0).getVc_estadoproveedor());
				}else if (putestado.getPerfil().equals("C")){
					putestado.setEstadoactual(l_oc.get(0).getVc_estadocomprador());
				}
				i = l_oc.size();
			}
		}
		catch(EmptyResultDataAccessException e) {
			e.printStackTrace();
			throw e;
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("OCURRIO UN ERROR EN DAO : " + e.getMessage().toString());
			i = 0;
		}
		return i;
	}
	
	
	public OrdenCompra mapParam_estados(ResultSet rs, int i) throws SQLException {
		OrdenCompra oc = new OrdenCompra();
		
		oc.setVc_estadoproveedor(StringUtils.trimToEmpty(rs.getString("vc_estadoproveedor")));
		oc.setVc_estadocomprador(StringUtils.trimToEmpty(rs.getString("vc_estadocomprador")));
	
		return oc;
	}
	
}