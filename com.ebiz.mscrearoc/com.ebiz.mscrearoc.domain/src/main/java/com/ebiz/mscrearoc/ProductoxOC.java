package com.ebiz.mscrearoc;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProductoxOC {

	@JsonProperty("AtributoProducto")
	private List<AtributoxProductoxOC> lista_atributoxprod = new ArrayList<AtributoxProductoxOC>();

	@JsonProperty("SubProducto")
	private List<SubItemxProductoxOC> lista_subitemxprod = new ArrayList<SubItemxProductoxOC>();//Evelin Ribbeck
	
	@JsonProperty("in_idproducto")
	private UUID in_idproducto;
	
	//@JsonProperty("vc_codigoproductoorg")
	@JsonProperty("CodigoProducto")
	private String vc_codigoproductoorg;//CodigoProducto
	
	@JsonProperty("TipoProducto")
	private String vc_tipoproducto;
	
	//@JsonProperty("vc_descortapro")
	@JsonProperty("DescripcionProducto")
	private String vc_descortapro;//DescripcionProducto
	
	@JsonProperty("DescripcionDetalladaProducto")
	private String vc_deslargapro;//DescripcionDetalladaProducto
	
	@JsonProperty("in_codigousuariocreacion")
	private UUID in_codigousuariocreacion;
	
	@JsonProperty("Mercancia")
	private String ch_commodity;
	
	@JsonProperty("in_habilitado")
	private int in_habilitado;
	
	@JsonProperty("EstadoProducto")
	private String vc_estadoprod;
	
	@JsonProperty("idOrganizacionComprador")
	private UUID in_idorganizacion;
	
	//@JsonProperty("in_posicion")
	@JsonProperty("PosicionProducto")
	private int in_posicion;//PosicionProducto
	
	@JsonProperty("CodigoProductoMercado")
	private String vc_codigoproductomercado;//CodigoProductoMercado
	
	@JsonProperty("NumeroSolicitud")
	private String numeroSolicitud;//NumeroSolicitud
	
	@JsonProperty("NumeroProductoSolicitud")
	private String numeroProductoSolicitud;//NumeroProductoSolicitud
	
	@JsonProperty("NumeroInventario")
	private String numeroInventario;//NumeroInventario
	
	//@JsonProperty("ts_fechaentregaproducto")
	@JsonProperty("FechaEntregaProducto")
	private String ts_fechaentregaproducto;//FechaEntregaProducto
	
	//@JsonProperty("fl_precioproducto")
	@JsonProperty("PrecioUnitarioProducto")
	private BigDecimal fl_precioproducto;//PrecioUnitarioProducto
	
	//@JsonProperty("fl_cantidadproducto")
	@JsonProperty("CantidadProducto")
	private BigDecimal fl_cantidadproducto;//CantidadProducto
	
	//@JsonProperty("fl_preciototalproducto")
	@JsonProperty("PrecioProducto")
	private BigDecimal fl_preciototalproducto;//PrecioProducto
	
	private Integer in_idunidadproducto;//UnidadProducto
	
	//@JsonProperty("in_idunidadproducto")
	@JsonProperty("UnidadProducto")
	private String vc_unidadproducto;
	
	/*@JsonProperty("vc_posicion")
	private String vc_posicion;*/
	
	@JsonProperty("ProductoCat")
	private Integer in_idproductocat;

	@JsonProperty("IdCategoria")
	private Integer in_idcategoria;
	
	@JsonProperty("ItemRequisicion")
	private Integer in_itemrequisicion;
	
	@JsonProperty("CantidadProductoPend")
	private BigDecimal fl_cantidadproductopend;
	
	@JsonProperty("IdProdB2M")
	private Integer in_idprodb2m;
	
	@JsonProperty("IdRequerimiento")
	private Integer in_idrequerimiento;
	
	@JsonProperty("PosicionReq")
	private Integer in_posicionreq;
	
	@JsonProperty("CantidadRecepcionada")
	private Integer fl_cantidadrecepcionada;
	
	@JsonProperty("ImpuestoProducto")
	private BigDecimal fl_impuestos;
	
	private String vc_idtablaunidad;
	
	private String vc_idregistrounidad;
	
	//////////////////////////////////////////////
	
	public List<AtributoxProductoxOC> getLista_atributoxprod() {
		return lista_atributoxprod;
	}

	public void setLista_atributoxprod(List<AtributoxProductoxOC> lista_atributoxprod) {
		this.lista_atributoxprod = lista_atributoxprod;
	}
	
	public UUID getIn_idproducto() {
		return in_idproducto;
	}

	public void setIn_idproducto(UUID in_idproducto) {
		this.in_idproducto = in_idproducto;
	}

	public String getVc_codigoproductoorg() {
		return vc_codigoproductoorg;
	}

	public void setVc_codigoproductoorg(String vc_codigoproductoorg) {
		this.vc_codigoproductoorg = vc_codigoproductoorg;
	}

	public String getVc_tipoproducto() {
		return vc_tipoproducto;
	}

	public void setVc_tipoproducto(String vc_tipoproducto) {
		this.vc_tipoproducto = vc_tipoproducto;
	}

	public String getVc_descortapro() {
		return vc_descortapro;
	}

	public void setVc_descortapro(String vc_descortapro) {
		this.vc_descortapro = vc_descortapro;
	}

	public UUID getIn_codigousuariocreacion() {
		return in_codigousuariocreacion;
	}

	public void setIn_codigousuariocreacion(UUID in_codigousuariocreacion) {
		this.in_codigousuariocreacion = in_codigousuariocreacion;
	}

	public int getIn_habilitado() {
		return in_habilitado;
	}

	public void setIn_habilitado(int in_habilitado) {
		this.in_habilitado = in_habilitado;
	}

	public String getVc_estadoprod() {
		return vc_estadoprod;
	}

	public void setVc_estadoprod(String vc_estadoprod) {
		this.vc_estadoprod = vc_estadoprod;
	}

	public UUID getIn_idorganizacion() {
		return in_idorganizacion;
	}

	public void setIn_idorganizacion(UUID in_idorganizacion) {
		this.in_idorganizacion = in_idorganizacion;
	}

	public int getIn_posicion() {
		return in_posicion;
	}

	public void setIn_posicion(int in_posicion) {
		this.in_posicion = in_posicion;
	}

	public String getTs_fechaentregaproducto() {
		return ts_fechaentregaproducto;
	}

	public void setTs_fechaentregaproducto(String ts_fechaentregaproducto) {
		this.ts_fechaentregaproducto = ts_fechaentregaproducto;
	}

	public BigDecimal getFl_precioproducto() {
		return fl_precioproducto;
	}

	public void setFl_precioproducto(BigDecimal fl_precioproducto) {
		this.fl_precioproducto = fl_precioproducto;
	}

	public BigDecimal getFl_cantidadproducto() {
		return fl_cantidadproducto;
	}

	public void setFl_cantidadproducto(BigDecimal fl_cantidadproducto) {
		this.fl_cantidadproducto = fl_cantidadproducto;
	}

	public BigDecimal getFl_preciototalproducto() {
		return fl_preciototalproducto;
	}

	public void setFl_preciototalproducto(BigDecimal fl_preciototalproducto) {
		this.fl_preciototalproducto = fl_preciototalproducto;
	}

	/*public String getVc_posicion() {
		return vc_posicion;
	}

	public void setVc_posicion(String vc_posicion) {
		this.vc_posicion = vc_posicion;
	}*/

	public List<SubItemxProductoxOC> getLista_subitemxprod() {
		return lista_subitemxprod;
	}

	public void setLista_subitemxprod(List<SubItemxProductoxOC> lista_subitemxprod) {
		this.lista_subitemxprod = lista_subitemxprod;
	}

	public String getVc_deslargapro() {
		return vc_deslargapro;
	}

	public void setVc_deslargapro(String vc_deslargapro) {
		this.vc_deslargapro = vc_deslargapro;
	}

	public String getVc_codigoproductomercado() {
		return vc_codigoproductomercado;
	}

	public void setVc_codigoproductomercado(String vc_codigoproductomercado) {
		this.vc_codigoproductomercado = vc_codigoproductomercado;
	}

	public String getNumeroSolicitud() {
		return numeroSolicitud;
	}

	public void setNumeroSolicitud(String numeroSolicitud) {
		this.numeroSolicitud = numeroSolicitud;
	}

	public String getNumeroProductoSolicitud() {
		return numeroProductoSolicitud;
	}

	public void setNumeroProductoSolicitud(String numeroProductoSolicitud) {
		this.numeroProductoSolicitud = numeroProductoSolicitud;
	}

	public String getNumeroInventario() {
		return numeroInventario;
	}

	public void setNumeroInventario(String numeroInventario) {
		this.numeroInventario = numeroInventario;
	}

	public String getCh_commodity() {
		return ch_commodity;
	}

	public void setCh_commodity(String ch_commodity) {
		this.ch_commodity = ch_commodity;
	}

	/*public Integer getIn_idunidadproducto() {
		return in_idunidadproducto;
	}

	public void setIn_idunidadproducto(Integer in_idunidadproducto) {
		this.in_idunidadproducto = in_idunidadproducto;
	}*/

	public Integer getIn_idproductocat() {
		return in_idproductocat;
	}

	public void setIn_idproductocat(Integer in_idproductocat) {
		this.in_idproductocat = in_idproductocat;
	}

	public Integer getIn_idcategoria() {
		return in_idcategoria;
	}

	public void setIn_idcategoria(Integer in_idcategoria) {
		this.in_idcategoria = in_idcategoria;
	}

	public Integer getIn_itemrequisicion() {
		return in_itemrequisicion;
	}

	public void setIn_itemrequisicion(Integer in_itemrequisicion) {
		this.in_itemrequisicion = in_itemrequisicion;
	}

	public BigDecimal getFl_cantidadproductopend() {
		return fl_cantidadproductopend;
	}

	public void setFl_cantidadproductopend(BigDecimal fl_cantidadproductopend) {
		this.fl_cantidadproductopend = fl_cantidadproductopend;
	}

	public Integer getIn_idprodb2m() {
		return in_idprodb2m;
	}

	public void setIn_idprodb2m(Integer in_idprodb2m) {
		this.in_idprodb2m = in_idprodb2m;
	}

	public Integer getIn_idrequerimiento() {
		return in_idrequerimiento;
	}

	public void setIn_idrequerimiento(Integer in_idrequerimiento) {
		this.in_idrequerimiento = in_idrequerimiento;
	}

	public Integer getIn_posicionreq() {
		return in_posicionreq;
	}

	public void setIn_posicionreq(Integer in_posicionreq) {
		this.in_posicionreq = in_posicionreq;
	}

	public Integer getFl_cantidadrecepcionada() {
		return fl_cantidadrecepcionada;
	}

	public void setFl_cantidadrecepcionada(Integer fl_cantidadrecepcionada) {
		this.fl_cantidadrecepcionada = fl_cantidadrecepcionada;
	}

	public BigDecimal getFl_impuestos() {
		return fl_impuestos;
	}

	public void setFl_impuestos(BigDecimal fl_impuestos) {
		this.fl_impuestos = fl_impuestos;
	}

	public String getVc_unidadproducto() {
		return vc_unidadproducto;
	}

	public void setVc_unidadproducto(String vc_unidadproducto) {
		this.vc_unidadproducto = vc_unidadproducto;
	}

	public Integer getIn_idunidadproducto() {
		return in_idunidadproducto;
	}

	public void setIn_idunidadproducto(Integer in_idunidadproducto) {
		this.in_idunidadproducto = in_idunidadproducto;
	}

	public String getVc_idtablaunidad() {
		return vc_idtablaunidad;
	}

	public void setVc_idtablaunidad(String vc_idtablaunidad) {
		this.vc_idtablaunidad = vc_idtablaunidad;
	}

	public String getVc_idregistrounidad() {
		return vc_idregistrounidad;
	}

	public void setVc_idregistrounidad(String vc_idregistrounidad) {
		this.vc_idregistrounidad = vc_idregistrounidad;
	}
	
	
	
	///////////////////////////
}
