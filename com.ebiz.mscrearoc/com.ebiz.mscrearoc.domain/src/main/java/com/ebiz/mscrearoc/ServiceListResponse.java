package com.ebiz.mscrearoc;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ServiceListResponse<T> extends ServiceResponse {
		
		@JsonProperty("draw")
		protected int draw;
		
		@JsonProperty("recordsTotal")
		protected int recordsTotal;
		
		@JsonProperty("recordsFiltered")
		protected int recordsFiltered;
		
		@JsonProperty("data")
		private List<T> data;
		
		public int getDraw() {
			return draw;
		}
		public void setDraw(int draw) {
			this.draw = draw;
		}
		public int getRecordsTotal() {
			return recordsTotal;
		}
		public void setRecordsTotal(int recordsTotal) {
			this.recordsTotal = recordsTotal;
		}
		public int getRecordsFiltered() {
			return recordsFiltered;
		}
		public void setRecordsFiltered(int recordsFiltered) {
			this.recordsFiltered = recordsFiltered;
		}
		public List<T> getData() {
			return data;
		}
		public void setData(List<T> data) {
			this.data = data;
		}
}

