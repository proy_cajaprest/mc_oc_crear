package com.ebiz.mscrearoc;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UsuarioEquivalencia {
	
	@JsonProperty("NombreUsuario")
	private String nombre_usuario;
	
	@JsonProperty("Titulo")
	private String titulo;
	
	@JsonProperty("Nombre")
	private String nombre;
	
	@JsonProperty("ApellidoPaterno")
	private String ap_paterno;
	
	@JsonProperty("ApellidoMaterno")
	private String ap_materno;
	
	@JsonProperty("Contacto")
	private String contacto;	
	
	@JsonProperty("MontoMax")
	private Double monto_max;
	
	@JsonProperty("HabilitarEnvioMail") 
	private Integer envio_mails; // para habilitar el envio de emails
	
	@JsonProperty("Direccion")
	private String direccion;
	
	@JsonProperty("Telefono")
	private String telefono;
	
	@JsonProperty("Fax")
	private String fax;
	
	@JsonProperty("Contrasenha")
	private String vc_password;
	
	@JsonProperty("Codigousuariocreacion")
	private UUID codigoUsuarioCreacion;
	
	@JsonProperty("TipoDocumento")
	private String tipoDocumento;
	
	@JsonProperty("NumeroDocumento")
	private String numeroDocumento;
	
	@JsonProperty("Idtablamoneda")
	private String Idtablamoneda;
	
	@JsonProperty("Idregistromoneda")
	private String idregistromoneda;
	
	@JsonProperty("Idtablapais")
	private String Idtablapais;
	
	@JsonProperty("Idregistropais")
	private String idregistropais;
	
	@JsonProperty("Idtablatipodoc")
	private String idtablatipodoc;
	
	@JsonProperty("Idregistrotipodoc")
	private String idregistrotipodoc;
	
	@JsonProperty("IdOrganizacion")
	private UUID idOrganizacion;
	
	@JsonProperty("IdRoles")
	private List<UUID> listaIdRoles = new ArrayList<UUID>();
	
	/////////////// para el detalle de usuarios /////////////////
	@JsonProperty("IdUsuario")
	private UUID idusuario;
	
	@JsonProperty("Anexo")
	private String anexo;
	
	@JsonProperty("Email")
	private String email;
	
	@JsonProperty("campo1")
	private String campo1;
	
	@JsonProperty("campo2")
	private String campo2;
	
	@JsonProperty("campo3")
	private String campo3;
	
	@JsonProperty("campo4")
	private String campo4;
	
	@JsonProperty("campo5")
	private String campo5;
	
	@JsonProperty("FechaHoraCreacion")
	private String fechaHoraCreacion;
	
	@JsonProperty("CodigoUsuarioModificacion")
	private UUID codigoUsuarioModificacion;
	
	@JsonProperty("FechaHoraModificacion")
	private String fechaHoraModificacion;
	
	@JsonProperty("habilitado")
	private Integer habilitado;
	
	@JsonProperty("poraprobar")
	private Integer poraprobar;
	
	@JsonProperty("idioma")
	private String idioma;
	
	@JsonProperty("boletin")
	private Integer boletin;

	@JsonProperty("Codigousuariomodificacion")
	private UUID codigousuariomodificacion;
	
	@JsonProperty("id_doc") // para el delete que viene del evento
	private UUID id_doc;
	
	//////////////////////////////////////////////////////////////

	public String getNombre_usuario() {
		return nombre_usuario;
	}

	public void setNombre_usuario(String nombre_usuario) {
		this.nombre_usuario = nombre_usuario;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getAp_paterno() {
		return ap_paterno;
	}

	public void setAp_paterno(String ap_paterno) {
		this.ap_paterno = ap_paterno;
	}

	public String getAp_materno() {
		return ap_materno;
	}

	public void setAp_materno(String ap_materno) {
		this.ap_materno = ap_materno;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public Double getMonto_max() {
		return monto_max;
	}

	public void setMonto_max(Double monto_max) {
		this.monto_max = monto_max;
	}

	public Integer getEnvio_mails() {
		return envio_mails;
	}

	public void setEnvio_mails(Integer envio_mails) {
		this.envio_mails = envio_mails;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}
	
	public String getVc_password() {
		return vc_password;
	}

	public void setVc_password(String vc_password) {
		this.vc_password = vc_password;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getIdtablamoneda() {
		return Idtablamoneda;
	}

	public void setIdtablamoneda(String idtablamoneda) {
		Idtablamoneda = idtablamoneda;
	}

	public String getIdregistromoneda() {
		return idregistromoneda;
	}

	public void setIdregistromoneda(String idregistromoneda) {
		this.idregistromoneda = idregistromoneda;
	}

	public String getIdtablapais() {
		return Idtablapais;
	}

	public void setIdtablapais(String idtablapais) {
		Idtablapais = idtablapais;
	}

	public String getIdregistropais() {
		return idregistropais;
	}

	public void setIdregistropais(String idregistropais) {
		this.idregistropais = idregistropais;
	}

	public String getIdtablatipodoc() {
		return idtablatipodoc;
	}

	public void setIdtablatipodoc(String idtablatipodoc) {
		this.idtablatipodoc = idtablatipodoc;
	}

	public String getIdregistrotipodoc() {
		return idregistrotipodoc;
	}

	public void setIdregistrotipodoc(String idregistrotipodoc) {
		this.idregistrotipodoc = idregistrotipodoc;
	}

	public UUID getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(UUID codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	public UUID getIdusuario() {
		return idusuario;
	}

	public void setIdusuario(UUID idusuario) {
		this.idusuario = idusuario;
	}

	public String getAnexo() {
		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCampo1() {
		return campo1;
	}

	public void setCampo1(String campo1) {
		this.campo1 = campo1;
	}

	public String getCampo2() {
		return campo2;
	}

	public void setCampo2(String campo2) {
		this.campo2 = campo2;
	}

	public String getCampo3() {
		return campo3;
	}

	public void setCampo3(String campo3) {
		this.campo3 = campo3;
	}

	public String getCampo4() {
		return campo4;
	}

	public void setCampo4(String campo4) {
		this.campo4 = campo4;
	}

	public String getCampo5() {
		return campo5;
	}

	public void setCampo5(String campo5) {
		this.campo5 = campo5;
	}

	public String getFechaHoraCreacion() {
		return fechaHoraCreacion;
	}

	public void setFechaHoraCreacion(String fechaHoraCreacion) {
		this.fechaHoraCreacion = fechaHoraCreacion;
	}

	public UUID getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}

	public void setCodigoUsuarioModificacion(UUID codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}

	public String getFechaHoraModificacion() {
		return fechaHoraModificacion;
	}

	public void setFechaHoraModificacion(String fechaHoraModificacion) {
		this.fechaHoraModificacion = fechaHoraModificacion;
	}

	public Integer getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(Integer habilitado) {
		this.habilitado = habilitado;
	}
	
	public Integer getPoraprobar() {
		return poraprobar;
	}

	public void setPoraprobar(Integer poraprobar) {
		this.poraprobar = poraprobar;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public Integer getBoletin() {
		return boletin;
	}

	public void setBoletin(Integer boletin) {
		this.boletin = boletin;
	}

	public List<UUID> getListaIdRoles() {
		return listaIdRoles;
	}

	public void setListaIdRoles(List<UUID> listaIdRoles) {
		this.listaIdRoles = listaIdRoles;
	}

	public UUID getIdOrganizacion() {
		return idOrganizacion;
	}

	public void setIdOrganizacion(UUID idOrganizacion) {
		this.idOrganizacion = idOrganizacion;
	}

	public UUID getCodigousuariomodificacion() {
		return codigousuariomodificacion;
	}

	public void setCodigousuariomodificacion(UUID codigousuariomodificacion) {
		this.codigousuariomodificacion = codigousuariomodificacion;
	}

	public UUID getId_doc() {
		return id_doc;
	}

	public void setId_doc(UUID id_doc) {
		this.id_doc = id_doc;
	}
	///////////////////////////////////////
	
}
