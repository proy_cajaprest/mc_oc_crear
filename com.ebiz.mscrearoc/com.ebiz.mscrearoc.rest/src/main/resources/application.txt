
################### DataSource Configuration ##########################
spring.datasource.driver-class-name=org.postgresql.Driver
spring.datasource.url=jdbc:postgresql://b2md-dev-dbs-001.postgres.database.azure.com:5432/postgres
spring.datasource.username=u_b2m_escritura@b2md-dev-dbs-001
spring.datasource.password=server$1

#################### Constant values ##################################
app.constantes.cant_decimales=4
app.constantes.mensajes=Ok,Numero de decimales es mayor al permitido,Operacion no resulta lo esperado,Origen no especificado
app.constantes.origen1=PEESEACE,PEB2M
app.constantes.origen2=cliente,ERP
app.constantes.origen3=proveedor
app.constantes.habilitado=1
app.constantes.put=PUT
app.constantes.post=POST
app.constantes.estadoanuladoOC=OANUL
app.constantes.estadocompradorOC=OEMIT
app.constantes.estadovendedorOC=ONVIS
app.constantes.url=http://b2miningdata.com/api/msocd/v1/ocs/b4bfbdad-ab21-4561-bd3f-3bf72d2d123d
api.oc.client.query-uri=http://msorganizacion:8080/api/msorganizacion/v1/orgs/?Ruc={RUC}&pais={PAIS}&column_names=IdOrganizacion
api.oc.client.query-usuario=http://msusuario:8080/api/msusuario/v1/usuarios/{id}/usuario/?equivalencia={eq}
#app.constantes.url=http://b2miningdata.com/api/msoclistar/v1/ordenes/?column_names=NumeroOrden,Fecha,EstadoOrden,TipoOrden,Version,FechaRegistro&NumeroOrden={NumeroOrden}
#api.oc.client.query-usuario=http://msorganizacion:8080/api/msorganizacion/v1/orgs/adm/{id}
#api.oc.client.query-adm=http://msorganizacion:8080/api/msorganizacion/v1/orgs/adm/{id}
#api.oc.client.query-adm=https://ebiz-api-dev-001.azure-api.net/organizacion/msorganizacion/v1/orgs/adm/{id}
api.oc.client.query-adm=http://msorganizacion:8080/api/msorganizacion/v1/orgs/adm/{id}
########################################################################

spring.datasource.initialize=true
########comentado por XChang
##spring.batch.initializer.enabled=false

##########Kafka config Consumer
spring.kafka.consumer.group-id=crear_guia
spring.kafka.bootstrap-servers=52.179.85.136:9092
spring.kafka.consumer.key-deserializer=org.apache.kafka.common.serialization.StringDeserializer
spring.kafka.consumer.value-deserializer=org.apache.kafka.common.serialization.StringDeserializer
spring.kafka.properties.topic.input=t_crear_oc
#####XC
spring.kafka.properties.topic.input.comentario=t_comentario_oc
spring.kafka.properties.topic.input.cambioestado=t_actualizar_estado_oc
#####FIN XC
spring.kafka.properties.topic.output.cuadroeval=t_crear_cuadroeval
spring.kafka.properties.topic.output.respuesta=t_respuesta_oc
#spring.kafka.properties.topic.output.cambioestado=t_actualizar_estado_oc

events.source=ms-oc-crear

#server.port=

###
spring.datasource.tomcat.max-active=1
spring.datasource.tomcat.initialSize=0

####Opcional
spring.datasource.tomcat.minIdle=1
spring.datasource.tomcat.maxIdle=1

######configuracion kafka
spring.kafka.producer.compression-type=gzip
spring.kafka.producer.batch-size=0
spring.kafka.properties.max.block.ms=30000
spring.kafka.properties.max.in.flight.requests.per.connection=1
spring.kafka.properties.metadata.max.age.ms=120000