package com.ebiz.mscrearoc;

import com.ebiz.mscrearoc.Comentario;

public interface ComentarioRepository {
	
	int guardarcomentariocomprador(Comentario comentario);
	int guardarcomentarioproveedor(Comentario comentario);

}