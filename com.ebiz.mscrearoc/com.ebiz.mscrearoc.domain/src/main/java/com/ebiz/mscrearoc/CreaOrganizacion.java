package com.ebiz.mscrearoc;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreaOrganizacion {
	
    @JsonProperty("IdOrganizacion")
    private String in_idorganizacion;

    @JsonProperty("RazonSocial")
    private String vc_nombre;

    @JsonProperty("Ruc")
    private String vc_ruc;
    
    @JsonProperty("DireccionOrganizacion")
    private String vc_direccion;
        
    @JsonProperty("Telefono")
    private String vc_telefono;// ch en BD
    
    @JsonProperty("Fax")
    private String vc_fax; //ch en BD
    
    @JsonProperty("Email")
    private String vc_email;
    
    @JsonProperty("Url")
    private String vc_url;
	
    ///// agregados para la creacion 
    
	@JsonProperty("Moneda")
	private String vc_moneda;
	
	@JsonProperty("IdUsuarioCreacion")						
	private String in_codigousuariocreacion;
	
	@JsonProperty("Habilitado")
	private Integer in_habilitado = 1;
	
	@JsonProperty("Telefono2")
	private String ch_telefono2;
	
	@JsonProperty("Telefono3")
	private String ch_telefono3;
	
	@JsonProperty("AnxTel")
	private String ch_anx_tel;
	
	@JsonProperty("AnxTel2")
	private String ch_anx_tel2;
	
	@JsonProperty("AnxTel3")
	private String ch_anx_tel3;
	
	@JsonProperty("AnxFax")
	private String ch_anx_fax;
	
	@JsonProperty("CodigoPostal")
	private String vc_codigopostal;
	
	@JsonProperty("Representante1")
	private String vc_representante1;
	
	@JsonProperty("Representante2")
	private String vc_representante2;
	
	@JsonProperty("Representante3")
	private String vc_representante3;
	
	@JsonProperty("Representante4")
	private String vc_representante4;
	
	@JsonProperty("CargoRepre1")
	private String ch_cargorepre1;
	
	@JsonProperty("CargoRepre2")
	private String ch_cargorepre2;
	
	@JsonProperty("CargoRepre3")
	private String ch_cargorepre3;
	
	@JsonProperty("CargoRepre4")
	private String ch_cargorepre4;
	
	@JsonProperty("EmailRepre1")
	private String ch_emailrepre1;
	
	@JsonProperty("EmailRepre2")
	private String ch_emailrepre2;
	
	@JsonProperty("EmailRepre3")
	private String ch_emailrepre3;
	
	@JsonProperty("EmailRepre4")
	private String ch_emailrepre4;
	
	@JsonProperty("Provincia")
	private String vc_provincia;
	
	@JsonProperty("Ciudad")
	private String vc_ciudad;
	
	@JsonProperty("DireccionUnidadProduccion")
	private String vc_direccionunidadproduccion;
	
	@JsonProperty("UbicacionAlmacen")
	private String vc_ubicacionalmacen;
	
	@JsonProperty("AlmacenTransitorioAsoc")
	private String vc_almacentransitorioasoc;
	
	@JsonProperty("DireccionAlmacenPrincipal")
	private String vc_direccionalmacenprincipal;
	
	@JsonProperty("DireccionAlmacenTransitorio")
	private String vc_direccionalmacentransitorio;
	
	@JsonProperty("Pais")
	private String isoPais;
	
	@JsonProperty("IdGrupoEmpresarial")
	private UUID idGrupoEmpresarial;
	
	// resto de campos de la tabla organizacion
		
	@JsonProperty("Campo1")
	private String vc_campo1;
	
	@JsonProperty("Campo2")
	private String vc_campo2;
	
	@JsonProperty("Campo3")
	private String vc_campo3;
	
	@JsonProperty("Campo4")
	private String vc_campo4;
	
	@JsonProperty("Campo5")
	private String vc_campo5;
	
	@JsonProperty("Fabricante")
	private String vc_fabricante;
	
	@JsonProperty("Anhofundacion")
	private Integer in_anhofundacion;
	
	@JsonProperty("Descripcioncia")
	private String vc_descripcioncia;
	
	@JsonProperty("Nrocrfq")
	private Integer in_nrocrfq;
	
	@JsonProperty("IdAdmin")
	private Integer in_idadmin;
	
	@JsonProperty("Tipoempresa")
	private String vc_tipoempresa;
	
	@JsonProperty("Integrado")
	private Integer in_integrado;
	
	@JsonProperty("Monedamig")
	private String vc_monedamig;
	
	@JsonProperty("Paismig")
	private String vc_paismig;
	
	@JsonProperty("Poraprobar")
	private Integer in_poraprobar;
	
	@JsonProperty("Gironegocio")
	private String vc_gironegocio;
	
	@JsonProperty("Numempleados")
	private String vc_numempleados;
	
	@JsonProperty("Montoventa")
	private double fl_montoventa;
	
	@JsonProperty("Codigoempresaref")
	private String vc_codigoempresaref;
	
	@JsonProperty("Nombreempresaref")
	private String vc_nombreempresaref;
	
	@JsonProperty("RucEmpresaref")
	private String vc_rucempresaref;
	
	@JsonProperty("CuentaPrincipal")
	private String vc_cuentaprincipal;
	
	@JsonProperty("IdBanco")
	private Integer in_idbanco;
	
	@JsonProperty("NombreBanco")
	private String vc_nombrebanco;
	
	@JsonProperty("Codigoerp")
	private String vc_codigoerp;
	
	@JsonProperty("Estado")
	private String vc_estado;
	
	@JsonProperty("FlagCubso")
	private Integer in_flag_cubso;
	
	@JsonProperty("Storageaccounturl")
	private String vc_storageaccounturl;
	
	@JsonProperty("Storageaccountapikey")
	private String vc_storageaccountapikey;
	
	@JsonProperty("Fechahoracreacion")
	private String ts_fechahoracreacion;
	
	@JsonProperty("IdUsuarioModificacion")
	private String in_codigousuariomodificacion;
	
	@JsonProperty("Fechahoramodificacion")
	private String ts_fechahoramodificacion;	
	
	@JsonProperty("id_doc") // para el delete que viene del evento
	private UUID id_doc;
	
	@JsonProperty("Ubigeo")
	private String vc_ubigeo;
	
	@JsonProperty("CondicionContribuyente")
	private String vc_condicioncontribuyente;

	@JsonProperty("existe")
	private Boolean existe;

	public String getIn_idorganizacion() {
		return in_idorganizacion;
	}

	public void setIn_idorganizacion(String in_idorganizacion) {
		this.in_idorganizacion = in_idorganizacion;
	}

	public String getVc_nombre() {
		return vc_nombre;
	}

	public void setVc_nombre(String vc_nombre) {
		this.vc_nombre = vc_nombre;
	}

	public String getVc_ruc() {
		return vc_ruc;
	}

	public void setVc_ruc(String vc_ruc) {
		this.vc_ruc = vc_ruc;
	}

	public String getVc_direccion() {
		return vc_direccion;
	}

	public void setVc_direccion(String vc_direccion) {
		this.vc_direccion = vc_direccion;
	}

	public String getVc_telefono() {
		return vc_telefono;
	}

	public void setVc_telefono(String vc_telefono) {
		this.vc_telefono = vc_telefono;
	}

	public String getVc_fax() {
		return vc_fax;
	}

	public void setVc_fax(String vc_fax) {
		this.vc_fax = vc_fax;
	}

	public String getVc_email() {
		return vc_email;
	}

	public void setVc_email(String vc_email) {
		this.vc_email = vc_email;
	}

	public String getVc_url() {
		return vc_url;
	}

	public void setVc_url(String vc_url) {
		this.vc_url = vc_url;
	}

	
	
	public String toString() {
		return "Organizacion [in_idorganizacion=" + in_idorganizacion + ", vc_nombre=" + vc_nombre + ", vc_ruc="
				+ vc_ruc + ", vc_direccion=" + vc_direccion + "]";
	}

	//////////////////////////////////////
	public String getVc_moneda() {
		return vc_moneda;
	}

	public void setVc_moneda(String vc_moneda) {
		this.vc_moneda = vc_moneda;
	}

	public String getIn_codigousuariocreacion() {
		return in_codigousuariocreacion;
	}

	public void setIn_codigousuariocreacion(String in_codigousuariocreacion) {
		this.in_codigousuariocreacion = in_codigousuariocreacion;
	}

	public Integer getIn_habilitado() {
		return in_habilitado;
	}

	public void setIn_habilitado(Integer in_habilitado) {
		this.in_habilitado = in_habilitado;
	}

	public String getCh_telefono2() {
		return ch_telefono2;
	}

	public void setCh_telefono2(String ch_telefono2) {
		this.ch_telefono2 = ch_telefono2;
	}

	public String getCh_telefono3() {
		return ch_telefono3;
	}

	public void setCh_telefono3(String ch_telefono3) {
		this.ch_telefono3 = ch_telefono3;
	}

	public String getCh_anx_tel() {
		return ch_anx_tel;
	}

	public void setCh_anx_tel(String ch_anx_tel) {
		this.ch_anx_tel = ch_anx_tel;
	}

	public String getCh_anx_tel2() {
		return ch_anx_tel2;
	}

	public void setCh_anx_tel2(String ch_anx_tel2) {
		this.ch_anx_tel2 = ch_anx_tel2;
	}

	public String getCh_anx_tel3() {
		return ch_anx_tel3;
	}

	public void setCh_anx_tel3(String ch_anx_tel3) {
		this.ch_anx_tel3 = ch_anx_tel3;
	}

	public String getCh_anx_fax() {
		return ch_anx_fax;
	}

	public void setCh_anx_fax(String ch_anx_fax) {
		this.ch_anx_fax = ch_anx_fax;
	}

	public String getVc_codigopostal() {
		return vc_codigopostal;
	}

	public void setVc_codigopostal(String vc_codigopostal) {
		this.vc_codigopostal = vc_codigopostal;
	}

	public String getVc_representante1() {
		return vc_representante1;
	}

	public void setVc_representante1(String vc_representante1) {
		this.vc_representante1 = vc_representante1;
	}

	public String getVc_representante2() {
		return vc_representante2;
	}

	public void setVc_representante2(String vc_representante2) {
		this.vc_representante2 = vc_representante2;
	}

	public String getVc_representante3() {
		return vc_representante3;
	}

	public void setVc_representante3(String vc_representante3) {
		this.vc_representante3 = vc_representante3;
	}

	public String getVc_representante4() {
		return vc_representante4;
	}

	public void setVc_representante4(String vc_representante4) {
		this.vc_representante4 = vc_representante4;
	}

	public String getCh_cargorepre1() {
		return ch_cargorepre1;
	}

	public void setCh_cargorepre1(String ch_cargorepre1) {
		this.ch_cargorepre1 = ch_cargorepre1;
	}

	public String getCh_cargorepre2() {
		return ch_cargorepre2;
	}

	public void setCh_cargorepre2(String ch_cargorepre2) {
		this.ch_cargorepre2 = ch_cargorepre2;
	}

	public String getCh_cargorepre3() {
		return ch_cargorepre3;
	}

	public void setCh_cargorepre3(String ch_cargorepre3) {
		this.ch_cargorepre3 = ch_cargorepre3;
	}

	public String getCh_cargorepre4() {
		return ch_cargorepre4;
	}

	public void setCh_cargorepre4(String ch_cargorepre4) {
		this.ch_cargorepre4 = ch_cargorepre4;
	}

	public String getCh_emailrepre1() {
		return ch_emailrepre1;
	}

	public void setCh_emailrepre1(String ch_emailrepre1) {
		this.ch_emailrepre1 = ch_emailrepre1;
	}

	public String getCh_emailrepre2() {
		return ch_emailrepre2;
	}

	public void setCh_emailrepre2(String ch_emailrepre2) {
		this.ch_emailrepre2 = ch_emailrepre2;
	}

	public String getCh_emailrepre3() {
		return ch_emailrepre3;
	}

	public void setCh_emailrepre3(String ch_emailrepre3) {
		this.ch_emailrepre3 = ch_emailrepre3;
	}

	public String getCh_emailrepre4() {
		return ch_emailrepre4;
	}

	public void setCh_emailrepre4(String ch_emailrepre4) {
		this.ch_emailrepre4 = ch_emailrepre4;
	}

	public String getVc_provincia() {
		return vc_provincia;
	}

	public void setVc_provincia(String vc_provincia) {
		this.vc_provincia = vc_provincia;
	}

	public String getVc_ciudad() {
		return vc_ciudad;
	}

	public void setVc_ciudad(String vc_ciudad) {
		this.vc_ciudad = vc_ciudad;
	}

	public String getVc_direccionunidadproduccion() {
		return vc_direccionunidadproduccion;
	}

	public void setVc_direccionunidadproduccion(String vc_direccionunidadproduccion) {
		this.vc_direccionunidadproduccion = vc_direccionunidadproduccion;
	}

	public String getVc_ubicacionalmacen() {
		return vc_ubicacionalmacen;
	}

	public void setVc_ubicacionalmacen(String vc_ubicacionalmacen) {
		this.vc_ubicacionalmacen = vc_ubicacionalmacen;
	}

	public String getVc_almacentransitorioasoc() {
		return vc_almacentransitorioasoc;
	}

	public void setVc_almacentransitorioasoc(String vc_almacentransitorioasoc) {
		this.vc_almacentransitorioasoc = vc_almacentransitorioasoc;
	}

	public String getVc_direccionalmacenprincipal() {
		return vc_direccionalmacenprincipal;
	}

	public void setVc_direccionalmacenprincipal(String vc_direccionalmacenprincipal) {
		this.vc_direccionalmacenprincipal = vc_direccionalmacenprincipal;
	}

	public String getVc_direccionalmacentransitorio() {
		return vc_direccionalmacentransitorio;
	}

	public void setVc_direccionalmacentransitorio(String vc_direccionalmacentransitorio) {
		this.vc_direccionalmacentransitorio = vc_direccionalmacentransitorio;
	}

	public String getIsoPais() {
		return isoPais;
	}

	public void setIsoPais(String isoPais) {
		this.isoPais = isoPais;
	}

	public UUID getIdGrupoEmpresarial() {
		return idGrupoEmpresarial;
	}

	public void setIdGrupoEmpresarial(UUID idGrupoEmpresarial) {
		this.idGrupoEmpresarial = idGrupoEmpresarial;
	}

	public UUID getId_doc() {
		return id_doc;
	}

	public void setId_doc(UUID id_doc) {
		this.id_doc = id_doc;
	}

	public String getVc_campo1() {
		return vc_campo1;
	}

	public void setVc_campo1(String vc_campo1) {
		this.vc_campo1 = vc_campo1;
	}

	public String getVc_campo2() {
		return vc_campo2;
	}

	public void setVc_campo2(String vc_campo2) {
		this.vc_campo2 = vc_campo2;
	}

	public String getVc_campo3() {
		return vc_campo3;
	}

	public void setVc_campo3(String vc_campo3) {
		this.vc_campo3 = vc_campo3;
	}

	public String getVc_campo4() {
		return vc_campo4;
	}

	public void setVc_campo4(String vc_campo4) {
		this.vc_campo4 = vc_campo4;
	}

	public String getVc_campo5() {
		return vc_campo5;
	}

	public void setVc_campo5(String vc_campo5) {
		this.vc_campo5 = vc_campo5;
	}

	public String getVc_fabricante() {
		return vc_fabricante;
	}

	public void setVc_fabricante(String vc_fabricante) {
		this.vc_fabricante = vc_fabricante;
	}

	public Integer getIn_anhofundacion() {
		return in_anhofundacion;
	}

	public void setIn_anhofundacion(Integer in_anhofundacion) {
		this.in_anhofundacion = in_anhofundacion;
	}

	public String getVc_descripcioncia() {
		return vc_descripcioncia;
	}

	public void setVc_descripcioncia(String vc_descripcioncia) {
		this.vc_descripcioncia = vc_descripcioncia;
	}

	public Integer getIn_nrocrfq() {
		return in_nrocrfq;
	}

	public void setIn_nrocrfq(Integer in_nrocrfq) {
		this.in_nrocrfq = in_nrocrfq;
	}

	public Integer getIn_idadmin() {
		return in_idadmin;
	}

	public void setIn_idadmin(Integer in_idadmin) {
		this.in_idadmin = in_idadmin;
	}

	public String getVc_tipoempresa() {
		return vc_tipoempresa;
	}

	public void setVc_tipoempresa(String vc_tipoempresa) {
		this.vc_tipoempresa = vc_tipoempresa;
	}

	public Integer getIn_integrado() {
		return in_integrado;
	}

	public void setIn_integrado(Integer in_integrado) {
		this.in_integrado = in_integrado;
	}

	public String getVc_monedamig() {
		return vc_monedamig;
	}

	public void setVc_monedamig(String vc_monedamig) {
		this.vc_monedamig = vc_monedamig;
	}

	public String getVc_paismig() {
		return vc_paismig;
	}

	public void setVc_paismig(String vc_paismig) {
		this.vc_paismig = vc_paismig;
	}

	public Integer getIn_poraprobar() {
		return in_poraprobar;
	}

	public void setIn_poraprobar(Integer in_poraprobar) {
		this.in_poraprobar = in_poraprobar;
	}

	public String getVc_gironegocio() {
		return vc_gironegocio;
	}

	public void setVc_gironegocio(String vc_gironegocio) {
		this.vc_gironegocio = vc_gironegocio;
	}

	public String getVc_numempleados() {
		return vc_numempleados;
	}

	public void setVc_numempleados(String vc_numempleados) {
		this.vc_numempleados = vc_numempleados;
	}

	public double getFl_montoventa() {
		return fl_montoventa;
	}

	public void setFl_montoventa(double fl_montoventa) {
		this.fl_montoventa = fl_montoventa;
	}

	public String getVc_codigoempresaref() {
		return vc_codigoempresaref;
	}

	public void setVc_codigoempresaref(String vc_codigoempresaref) {
		this.vc_codigoempresaref = vc_codigoempresaref;
	}

	public String getVc_nombreempresaref() {
		return vc_nombreempresaref;
	}

	public void setVc_nombreempresaref(String vc_nombreempresaref) {
		this.vc_nombreempresaref = vc_nombreempresaref;
	}

	public String getVc_rucempresaref() {
		return vc_rucempresaref;
	}

	public void setVc_rucempresaref(String vc_rucempresaref) {
		this.vc_rucempresaref = vc_rucempresaref;
	}

	public String getVc_cuentaprincipal() {
		return vc_cuentaprincipal;
	}

	public void setVc_cuentaprincipal(String vc_cuentaprincipal) {
		this.vc_cuentaprincipal = vc_cuentaprincipal;
	}

	public Integer getIn_idbanco() {
		return in_idbanco;
	}

	public void setIn_idbanco(Integer in_idbanco) {
		this.in_idbanco = in_idbanco;
	}

	public String getVc_nombrebanco() {
		return vc_nombrebanco;
	}

	public void setVc_nombrebanco(String vc_nombrebanco) {
		this.vc_nombrebanco = vc_nombrebanco;
	}

	public String getVc_codigoerp() {
		return vc_codigoerp;
	}

	public void setVc_codigoerp(String vc_codigoerp) {
		this.vc_codigoerp = vc_codigoerp;
	}

	public String getVc_estado() {
		return vc_estado;
	}

	public void setVc_estado(String vc_estado) {
		this.vc_estado = vc_estado;
	}

	public Integer getIn_flag_cubso() {
		return in_flag_cubso;
	}

	public void setIn_flag_cubso(Integer in_flag_cubso) {
		this.in_flag_cubso = in_flag_cubso;
	}

	public String getVc_storageaccounturl() {
		return vc_storageaccounturl;
	}

	public void setVc_storageaccounturl(String vc_storageaccounturl) {
		this.vc_storageaccounturl = vc_storageaccounturl;
	}	

	public String getVc_storageaccountapikey() {
		return vc_storageaccountapikey;
	}

	public void setVc_storageaccountapikey(String vc_storageaccountapikey) {
		this.vc_storageaccountapikey = vc_storageaccountapikey;
	}

	public String getTs_fechahoracreacion() {
		return ts_fechahoracreacion;
	}

	public void setTs_fechahoracreacion(String ts_fechahoracreacion) {
		this.ts_fechahoracreacion = ts_fechahoracreacion;
	}

	public String getIn_codigousuariomodificacion() {
		return in_codigousuariomodificacion;
	}

	public void setIn_codigousuariomodificacion(String in_codigousuariomodificacion) {
		this.in_codigousuariomodificacion = in_codigousuariomodificacion;
	}

	public String getTs_fechahoramodificacion() {
		return ts_fechahoramodificacion;
	}

	public void setTs_fechahoramodificacion(String ts_fechahoramodificacion) {
		this.ts_fechahoramodificacion = ts_fechahoramodificacion;
	}

	public String getVc_ubigeo() {
		return vc_ubigeo;
	}

	public void setVc_ubigeo(String vc_ubigeo) {
		this.vc_ubigeo = vc_ubigeo;
	}

	public String getVc_condicioncontribuyente() {
		return vc_condicioncontribuyente;
	}

	public void setVc_condicioncontribuyente(String vc_condicioncontribuyente) {
		this.vc_condicioncontribuyente = vc_condicioncontribuyente;
	}

	public Boolean getExiste() {
		return existe;
	}
	
	public void setExiste(Boolean existe) {
		this.existe = existe;
	}
}
