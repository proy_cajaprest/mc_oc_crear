package com.ebiz.mscrearoc;

import java.util.UUID;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Rol {
	
	@JsonProperty("RolId")
	private UUID RolId;
	
	@JsonProperty("Descripcion")
	private String Descripcion;
	
	@JsonProperty("Comentario")
	private String Comentario;
	
	@JsonProperty("Estado")
	private String Estado;
	
	@JsonProperty("Campo1")
	private String Campo1;
	
	@JsonProperty("Campo2")
	private String Campo2;
	
	@JsonProperty("Campo3")
	private String Campo3;
	
	@JsonProperty("UsuarioCreacion")
	private UUID UsuarioCreacion;
	
	@JsonProperty("UsuarioModificacion")
	private UUID UsuarioModificacion;
	
	@JsonProperty("FechaHoraCreacion")
	private String FechaHoraCreacion;
	
	@JsonProperty("FechaHoraModificacion")
	private String FechaHoraModificacion;
	
	@JsonProperty("Habilitado")
	private int Habilitado;
	
	///////////////////////////////////
	
	public UUID getRolId() {
		return RolId;
	}

	public void setRolId(UUID rolId) {
		RolId = rolId;
	}

	public String getDescripcion() {
		return Descripcion;
	}

	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}

	public String getComentario() {
		return Comentario;
	}

	public void setComentario(String comentario) {
		Comentario = comentario;
	}

	public String getEstado() {
		return Estado;
	}

	public void setEstado(String estado) {
		Estado = estado;
	}

	public String getCampo1() {
		return Campo1;
	}

	public void setCampo1(String campo1) {
		Campo1 = campo1;
	}

	public String getCampo2() {
		return Campo2;
	}

	public void setCampo2(String campo2) {
		Campo2 = campo2;
	}

	public String getCampo3() {
		return Campo3;
	}

	public void setCampo3(String campo3) {
		Campo3 = campo3;
	}

	public UUID getUsuarioCreacion() {
		return UsuarioCreacion;
	}

	public void setUsuarioCreacion(UUID usuarioCreacion) {
		UsuarioCreacion = usuarioCreacion;
	}

	public UUID getUsuarioModificacion() {
		return UsuarioModificacion;
	}

	public void setUsuarioModificacion(UUID usuarioModificacion) {
		UsuarioModificacion = usuarioModificacion;
	}

	public String getFechaHoraCreacion() {
		return FechaHoraCreacion;
	}

	public void setFechaHoraCreacion(String fechaHoraCreacion) {
		FechaHoraCreacion = fechaHoraCreacion;
	}

	public String getFechaHoraModificacion() {
		return FechaHoraModificacion;
	}

	public void setFechaHoraModificacion(String fechaHoraModificacion) {
		FechaHoraModificacion = fechaHoraModificacion;
	}

	public int getHabilitado() {
		return Habilitado;
	}

	public void setHabilitado(int habilitado) {
		Habilitado = habilitado;
	}
	
}