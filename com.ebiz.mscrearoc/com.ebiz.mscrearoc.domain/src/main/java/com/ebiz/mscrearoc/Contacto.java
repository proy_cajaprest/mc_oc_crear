package com.ebiz.mscrearoc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Contacto {

    @JsonProperty("NombreContactoVendedor")
    private String nombreContactoVendedor;
    
    @JsonProperty("EmailContactoVendedor")
    private String emailContactoVendedor;
    
    @JsonProperty("OtrosDatosVendedor")
    private String otrosDatosVendedor;
    
    @JsonProperty("NombreContactoComprador")
    private String nombreContactoComprador;
    
    @JsonProperty("EmailContactoComprador")
    private String emailContactoComprador;
    
    @JsonProperty("OtrosDatosComprador")
    private String otrosDatosComprador;

    @JsonProperty("NombreContactoVendedor")
    public String getNombreContactoVendedor() {
        return nombreContactoVendedor;
    }

    @JsonProperty("NombreContactoVendedor")
    public void setNombreContactoVendedor(String nombreContactoVendedor) {
        this.nombreContactoVendedor = nombreContactoVendedor;
    }

    @JsonProperty("EmailContactoVendedor")
    public String getEmailContactoVendedor() {
        return emailContactoVendedor;
    }

    @JsonProperty("EmailContactoVendedor")
    public void setEmailContactoVendedor(String emailContactoVendedor) {
        this.emailContactoVendedor = emailContactoVendedor;
    }

    @JsonProperty("OtrosDatosVendedor")
    public String getOtrosDatosVendedor() {
        return otrosDatosVendedor;
    }

    @JsonProperty("OtrosDatosVendedor")
    public void setOtrosDatosVendedor(String otrosDatosVendedor) {
        this.otrosDatosVendedor = otrosDatosVendedor;
    }

    @JsonProperty("NombreContactoComprador")
    public String getNombreContactoComprador() {
        return nombreContactoComprador;
    }

    @JsonProperty("NombreContactoComprador")
    public void setNombreContactoComprador(String nombreContactoComprador) {
        this.nombreContactoComprador = nombreContactoComprador;
    }

    @JsonProperty("EmailContactoComprador")
    public String getEmailContactoComprador() {
        return emailContactoComprador;
    }

    @JsonProperty("EmailContactoComprador")
    public void setEmailContactoComprador(String emailContactoComprador) {
        this.emailContactoComprador = emailContactoComprador;
    }

    @JsonProperty("OtrosDatosComprador")
    public String getOtrosDatosComprador() {
        return otrosDatosComprador;
    }

    @JsonProperty("OtrosDatosComprador")
    public void setOtrosDatosComprador(String otrosDatosComprador) {
        this.otrosDatosComprador = otrosDatosComprador;
    }

}
