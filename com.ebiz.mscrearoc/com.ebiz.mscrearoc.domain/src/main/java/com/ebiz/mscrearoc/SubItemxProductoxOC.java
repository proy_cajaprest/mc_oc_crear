package com.ebiz.mscrearoc;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SubItemxProductoxOC {
	
	@JsonProperty("AtributoSubProducto")
	private List<AtributoxSubItemxProductoxOC> lista_atributoxsubitemxprod = new ArrayList<AtributoxSubItemxProductoxOC>();
	
	//@JsonProperty("CodigoSubProducto")
	private UUID in_idsubitemxprodxoc;//CodigoSubProducto
	
	@JsonProperty("CodigoSubProducto")
	private String vc_codigosubitemorg;
	
	@JsonProperty("in_tiposubitem")
	private Integer in_tiposubitem ;
	
	@JsonProperty("DescripcionSubProducto")
	private String vc_descortasubitem;//DescripcionSubProducto

	@JsonProperty("DescripcionDetalladaSubProducto")
	private String vc_deslargasubitem ;//DescripcionDetalladaSubProducto
	
	@JsonProperty("vc_campo1")
	private String vc_campo1 ;
	
	@JsonProperty("vc_campo2")
	private String vc_campo2 ;
	
	@JsonProperty("vc_campo3")
	private String vc_campo3 ;
	
	@JsonProperty("vc_campo4")
	private String vc_campo4 ;
	
	@JsonProperty("vc_campo5")
	private String vc_campo5 ;
	
	@JsonProperty("in_idusuariocreacion")
	private UUID in_idusuariocreacion ;
	
	@JsonProperty("in_idusuariomodificacion")
	private UUID in_idusuariomodificacion ;
	
	@JsonProperty("ts_fechahoracreacion")
	private Timestamp ts_fechahoracreacion;
	
	@JsonProperty("ts_fechahoramodificacion")
	private Timestamp ts_fechahoramodificacion;
	
	@JsonProperty("in_habilitado")
	private Integer in_habilitado ;
	
	@JsonProperty("vc_estadosubitem")
	private String vc_estadosubitem ;
	
	@JsonProperty("in_idorganizacion")
	private Integer in_idorganizacion ;
	

	private Integer in_subitemposicion ;//PosicionSubProducto
	
	@JsonProperty("FechaEntregaSubProducto")
	private String ts_fechaentregasubitem;//FechaEntregaSubProducto
	
	@JsonProperty("PrecioSubProducto")
	private BigDecimal fl_preciosubitem;//PrecioSubProducto
	
	@JsonProperty("PrecioUnitarioSubProducto")
	private BigDecimal fl_preciounitariosubitem;//PrecioUnitarioSubProducto
	
	@JsonProperty("ImpuestoSubProducto")
	private BigDecimal impuestoSubProducto;//ImpuestoSubProducto
	
	@JsonProperty("CantidadSubProducto")
	private BigDecimal fl_cantidadsubitem;//CantidadSubProducto
	
	/*@JsonProperty("fl_preciototalsubitem")
	private Double fl_preciototalsubitem;*/
	
	private Integer in_idunidadsubitem ;//UnidadSubProducto
	
	@JsonProperty("UnidadSubProducto")
	private String vc_unidadsubitem ;
	
	@JsonProperty("fl_cantidadsubitempend")
	private BigDecimal fl_cantidadsubitempend;
		
	@JsonProperty("vc_numeroparte")
	private String vc_numeroparte ;
	
	@JsonProperty("PosicionSubProducto")
	private String vc_subitemposicion ;
	
	@JsonProperty("in_idrequerimiento")
	private Integer in_idrequerimiento ;
	
	@JsonProperty("in_posicionreq")
	private Integer in_posicionreq ;
	
	@JsonProperty("FactorDecimalSubProducto")
	private BigDecimal factorDecimalSubProducto;//FactorDecimalSubProducto
	
	private String vc_idtablaunidad;
	
	private String vc_idregistrounidad;
	
//////////////////////////////////
	
	public UUID getIn_idsubitemxprodxoc() {
		return in_idsubitemxprodxoc;
	}

	public void setIn_idsubitemxprodxoc(UUID in_idsubitemxprodxoc) {
		this.in_idsubitemxprodxoc = in_idsubitemxprodxoc;
	}

	public String getVc_codigosubitemorg() {
		return vc_codigosubitemorg;
	}

	public void setVc_codigosubitemorg(String vc_codigosubitemorg) {
		this.vc_codigosubitemorg = vc_codigosubitemorg;
	}

	public Integer getIn_tiposubitem() {
		return in_tiposubitem;
	}

	public void setIn_tiposubitem(Integer in_tiposubitem) {
		this.in_tiposubitem = in_tiposubitem;
	}

	public String getVc_descortasubitem() {
		return vc_descortasubitem;
	}

	public void setVc_descortasubitem(String vc_descortasubitem) {
		this.vc_descortasubitem = vc_descortasubitem;
	}

	public String getVc_deslargasubitem() {
		return vc_deslargasubitem;
	}

	public void setVc_deslargasubitem(String vc_deslargasubitem) {
		this.vc_deslargasubitem = vc_deslargasubitem;
	}

	public String getVc_campo1() {
		return vc_campo1;
	}

	public void setVc_campo1(String vc_campo1) {
		this.vc_campo1 = vc_campo1;
	}

	public String getVc_campo2() {
		return vc_campo2;
	}

	public void setVc_campo2(String vc_campo2) {
		this.vc_campo2 = vc_campo2;
	}

	public String getVc_campo3() {
		return vc_campo3;
	}

	public void setVc_campo3(String vc_campo3) {
		this.vc_campo3 = vc_campo3;
	}

	public String getVc_campo4() {
		return vc_campo4;
	}

	public void setVc_campo4(String vc_campo4) {
		this.vc_campo4 = vc_campo4;
	}

	public String getVc_campo5() {
		return vc_campo5;
	}

	public void setVc_campo5(String vc_campo5) {
		this.vc_campo5 = vc_campo5;
	}

	public UUID getIn_idusuariocreacion() {
		return in_idusuariocreacion;
	}

	public void setIn_idusuariocreacion(UUID in_idusuariocreacion) {
		this.in_idusuariocreacion = in_idusuariocreacion;
	}

	public UUID getIn_idusuariomodificacion() {
		return in_idusuariomodificacion;
	}

	public void setIn_idusuariomodificacion(UUID in_idusuariomodificacion) {
		this.in_idusuariomodificacion = in_idusuariomodificacion;
	}

	public Timestamp getTs_fechahoracreacion() {
		return ts_fechahoracreacion;
	}

	public void setTs_fechahoracreacion(Timestamp ts_fechahoracreacion) {
		this.ts_fechahoracreacion = ts_fechahoracreacion;
	}

	public Timestamp getTs_fechahoramodificacion() {
		return ts_fechahoramodificacion;
	}

	public void setTs_fechahoramodificacion(Timestamp ts_fechahoramodificacion) {
		this.ts_fechahoramodificacion = ts_fechahoramodificacion;
	}

	public Integer getIn_habilitado() {
		return in_habilitado;
	}

	public void setIn_habilitado(Integer in_habilitado) {
		this.in_habilitado = in_habilitado;
	}

	public String getVc_estadosubitem() {
		return vc_estadosubitem;
	}

	public void setVc_estadosubitem(String vc_estadosubitem) {
		this.vc_estadosubitem = vc_estadosubitem;
	}

	public Integer getIn_idorganizacion() {
		return in_idorganizacion;
	}

	public void setIn_idorganizacion(Integer in_idorganizacion) {
		this.in_idorganizacion = in_idorganizacion;
	}

	public Integer getIn_subitemposicion() {
		return in_subitemposicion;
	}

	public void setIn_subitemposicion(Integer in_subitemposicion) {
		this.in_subitemposicion = in_subitemposicion;
	}

	public String getTs_fechaentregasubitem() {
		return ts_fechaentregasubitem;
	}

	public void setTs_fechaentregasubitem(String ts_fechaentregasubitem) {
		this.ts_fechaentregasubitem = ts_fechaentregasubitem;
	}

	public BigDecimal getFl_preciosubitem() {
		return fl_preciosubitem;
	}

	public void setFl_preciosubitem(BigDecimal fl_preciosubitem) {
		this.fl_preciosubitem = fl_preciosubitem;
	}

	public BigDecimal getFl_cantidadsubitem() {
		return fl_cantidadsubitem;
	}

	public void setFl_cantidadsubitem(BigDecimal fl_cantidadsubitem) {
		this.fl_cantidadsubitem = fl_cantidadsubitem;
	}

	/*public Double getFl_preciototalsubitem() {
		return fl_preciototalsubitem;
	}

	public void setFl_preciototalsubitem(Double fl_preciototalsubitem) {
		this.fl_preciototalsubitem = fl_preciototalsubitem;
	}*/

	public BigDecimal getFl_cantidadsubitempend() {
		return fl_cantidadsubitempend;
	}

	public void setFl_cantidadsubitempend(BigDecimal fl_cantidadsubitempend) {
		this.fl_cantidadsubitempend = fl_cantidadsubitempend;
	}

	public String getVc_numeroparte() {
		return vc_numeroparte;
	}

	public void setVc_numeroparte(String vc_numeroparte) {
		this.vc_numeroparte = vc_numeroparte;
	}

	public String getVc_subitemposicion() {
		return vc_subitemposicion;
	}

	public void setVc_subitemposicion(String vc_subitemposicion) {
		this.vc_subitemposicion = vc_subitemposicion;
	}

	public Integer getIn_idrequerimiento() {
		return in_idrequerimiento;
	}

	public void setIn_idrequerimiento(Integer in_idrequerimiento) {
		this.in_idrequerimiento = in_idrequerimiento;
	}

	public Integer getIn_posicionreq() {
		return in_posicionreq;
	}

	public void setIn_posicionreq(Integer in_posicionreq) {
		this.in_posicionreq = in_posicionreq;
	}

	public List<AtributoxSubItemxProductoxOC> getLista_atributoxsubitemxprod() {
		return lista_atributoxsubitemxprod;
	}

	public void setLista_atributoxsubitemxprod(List<AtributoxSubItemxProductoxOC> lista_atributoxsubitemxprod) {
		this.lista_atributoxsubitemxprod = lista_atributoxsubitemxprod;
	}

	public BigDecimal getImpuestoSubProducto() {
		return impuestoSubProducto;
	}

	public void setImpuestoSubProducto(BigDecimal impuestoSubProducto) {
		this.impuestoSubProducto = impuestoSubProducto;
	}

	public BigDecimal getFactorDecimalSubProducto() {
		return factorDecimalSubProducto;
	}

	public void setFactorDecimalSubProducto(BigDecimal factorDecimalSubProducto) {
		this.factorDecimalSubProducto = factorDecimalSubProducto;
	}

	public Integer getIn_idunidadsubitem() {
		return in_idunidadsubitem;
	}

	public void setIn_idunidadsubitem(Integer in_idunidadsubitem) {
		this.in_idunidadsubitem = in_idunidadsubitem;
	}

	public BigDecimal getFl_preciounitariosubitem() {
		return fl_preciounitariosubitem;
	}

	public void setFl_preciounitariosubitem(BigDecimal fl_preciounitariosubitem) {
		this.fl_preciounitariosubitem = fl_preciounitariosubitem;
	}

	public String getVc_unidadsubitem() {
		return vc_unidadsubitem;
	}

	public void setVc_unidadsubitem(String vc_unidadsubitem) {
		this.vc_unidadsubitem = vc_unidadsubitem;
	}

	public String getVc_idtablaunidad() {
		return vc_idtablaunidad;
	}

	public void setVc_idtablaunidad(String vc_idtablaunidad) {
		this.vc_idtablaunidad = vc_idtablaunidad;
	}

	public String getVc_idregistrounidad() {
		return vc_idregistrounidad;
	}

	public void setVc_idregistrounidad(String vc_idregistrounidad) {
		this.vc_idregistrounidad = vc_idregistrounidad;
	}
		
	//////////////////////////////////////
}
