package com.ebiz.mscrearoc;


public class Maestra {
	
	private String vc_idtabla;
	
	private String vc_desc_corta;	
	
	private String vc_idregistro;
	
	private String vc_iso; 

	public String getVc_idtabla() {
		return vc_idtabla;
	}

	public void setVc_idtabla(String vc_idtabla) {
		this.vc_idtabla = vc_idtabla;
	}

	public String getVc_desc_corta() {
		return vc_desc_corta;
	}

	public void setVc_desc_corta(String vc_desc_corta) {
		this.vc_desc_corta = vc_desc_corta;
	}

	public String getVc_idregistro() {
		return vc_idregistro;
	}

	public void setVc_idregistro(String vc_idregistro) {
		this.vc_idregistro = vc_idregistro;
	}

	public String getVc_iso() {
		return vc_iso;
	}

	public void setVc_iso(String vc_iso) {
		this.vc_iso = vc_iso;
	}
	
	
}
