package com.ebiz.mscrearoc.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ebiz.mscrearoc.PutestadoRepository;
import com.ebiz.mscrearoc.PutestadoService;
import com.ebiz.mscrearoc.Putestado;

@Service
public class PutestadoServiceImpl implements PutestadoService {
	
	@Autowired
	private PutestadoRepository putestadoRepository;

	public int actualizaroccomprador(Putestado putestado) {
		return putestadoRepository.actualizaroccomprador(putestado);
	}

	public int actualizarocproveedor(Putestado putestado) {
		return putestadoRepository.actualizarocproveedor(putestado);
	}
	
	public int obtenerEstadoActual(Putestado putestado) {
		return putestadoRepository.obtenerEstadoActual(putestado);
	}
}